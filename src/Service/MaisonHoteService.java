/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;
import static Core.Controller.getUserId;
import Entities.Maisons_hotes;
import Entities.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import utils.DataSource;

/**
 *
 * @author Bhs Nada
 */
public class MaisonHoteService {
        Connection c= DataSource.getInstance().getConnection();
        
         /***********************************  Ajouter Maison D'hôte   **************************************/
 
    public void ajouterHote(Maisons_hotes m){
         
        
        try {
            Statement st= c.createStatement();
            String req = "INSERT INTO maisons_hotes (nom,description,pays,gouvernorat,image,adresse,site_web,mail,capacites,tel,prix,user_id) VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?)";
            
            PreparedStatement ps = c.prepareStatement(req);
            
             ps.setString(1, m.getNom());
             ps.setString(2, m.getDescription());
             ps.setString(3, m.getPays());
             ps.setString(4, m.getGouvernorat());
             ps.setString(5, m.getImage());
             ps.setString(6, m.getAdresse());
             ps.setString(7, m.getSite_web());
             ps.setString(8, m.getMail());
             ps.setInt(9,m.getCapacites());
             ps.setInt(10,m.getTel());
             ps.setDouble(11,m.getPrix());
             ps.setInt(12,getUserId());
             System.out.println(ps);
              ps.executeUpdate();
              System.out.println("Maison d'hote ajouté avec succès"); 
                      
        } catch (SQLException ex) {
            Logger.getLogger(MaisonHoteService.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }
    ///////////////////////////////////////////////////////////////////////////////
    
    /****************** Afficher Maison d'hôte  ***************************/
    
    public ArrayList<Maisons_hotes> AfficherMaisonsHotes() {
        ArrayList<Maisons_hotes> myList = new ArrayList<Maisons_hotes>();
        try {

            String req = "SELECT * FROM maisons_hotes";
            Statement st = c.createStatement();
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {

                Maisons_hotes maison = new Maisons_hotes();
                
                maison.setId(rs.getInt("id"));
                maison.setNom(rs.getString("nom"));
                maison.setDescription(rs.getString("description"));
                maison.setPays(rs.getString("pays"));
                maison.setGouvernorat(rs.getString("gouvernorat"));
                maison.setCapacites(rs.getInt("capacites"));
                maison.setTel(rs.getInt("tel"));
                maison.setMail(rs.getString("mail"));
                maison.setImage(rs.getString("image"));
                maison.setAdresse(rs.getString("adresse"));
                maison.setSite_web(rs.getString("site_web"));
                maison.setPrix(rs.getDouble("prix"));
            
                myList.add(maison);

            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return myList;

    }
    /////////////////////////////////////////////////////////////////////////////////
    
    public List<Maisons_hotes> list()
    {
    
    ObservableList<Maisons_hotes> Maisons=FXCollections.observableArrayList();
    
     String req = "SELECT * FROM maisons_hotes";
            Statement st;
            try {
                st = c.createStatement();
                ResultSet rs = st.executeQuery(req);
                 while (rs.next()) {

                Maisons_hotes maison = new Maisons_hotes();
                
             
                maison.setId(rs.getInt("id"));
                maison.setNom(rs.getString("nom"));
                maison.setDescription(rs.getString("description"));
                maison.setPays(rs.getString("pays"));
                maison.setGouvernorat(rs.getString("gouvernorat"));
                maison.setCapacites(rs.getInt("capacites"));
                maison.setTel(rs.getInt("tel"));
                maison.setMail(rs.getString("mail"));
                maison.setImage(rs.getString("image"));
                maison.setAdresse(rs.getString("adresse"));
                maison.setSite_web(rs.getString("site_web"));
                maison.setPrix(rs.getDouble("prix"));
            
                Maisons.add(maison);

            }
                
            } catch (SQLException ex) {
                Logger.getLogger(MaisonHoteService.class.getName()).log(Level.SEVERE, null, ex);
            }
            
    return Maisons;

    }
    
    //////////////////////////////////////////////////////////////////
     public void SupprimerHote(Maisons_hotes m) {
        try {
           
            String req = "Delete from maisons_hotes Where id=?";
            PreparedStatement pst2 = c.prepareStatement(req);
            pst2.setInt(1, m.getId());
            System.out.println("suppression !!! ");
            pst2.executeUpdate();
        } 
        catch (SQLException ex) {
           System.err.println(ex.getMessage());
        }

    }

    ///////////////////////////////////////////////////////////////////////////
     /************
      *             Modifier Maisons d'hôtes
      */
     
     
         
      public void ModifierMaison(Maisons_hotes m,int id){

                  try {
            String requete = "UPDATE  maisons_hotes SET nom=?,description=?,gouvernorat=?,capacites=?,adresse=?,site_web=?,mail=?,tel=?,prix=?"
                    + " WHERE id=?";
         Statement st= c.createStatement();
           
            PreparedStatement ps = c.prepareStatement(requete);
            
            ps.setString(1, m.getNom());
            ps.setString(2, m.getDescription());
           // ps.setString(3, m.getPays());
            ps.setString(3, m.getGouvernorat());
            ps.setInt(4, m.getCapacites());
            ps.setString(5, m.getAdresse());
            ps.setString(6, m.getSite_web());
            ps.setString(7, m.getMail());
            ps.setInt(8, m.getTel());
            ps.setDouble(9, m.getPrix());
            ps.setInt(10, m.getId());
            //ps.setString(11, m.getImage());
            ps.executeUpdate();
            
                      System.out.println(m.getId());
           
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }


 }
      
      
      //**************************************    Rechercher par nom hôte    ***********************************    
     

      public List<Maisons_hotes> FindByName(String key){
          
        ObservableList<Maisons_hotes> maisonsObserv=FXCollections.observableArrayList();
    
        try {
            
            
            String req="SELECT * FROM maisons_hotes where nom LIKE '%"+key+"%' ";
            Statement st= c.createStatement();
           
            PreparedStatement ps = c.prepareStatement(req);
            ResultSet rs= st.executeQuery(req);
                        
            
            while(rs.next()){
                
                
                   
                
              Maisons_hotes maison = new Maisons_hotes();
                
             
                maison.setId(rs.getInt("id"));
                maison.setNom(rs.getString("nom"));
                maison.setDescription(rs.getString("description"));
                maison.setPays(rs.getString("pays"));
                maison.setGouvernorat(rs.getString("gouvernorat"));
                maison.setCapacites(rs.getInt("capacites"));
                maison.setTel(rs.getInt("tel"));
                maison.setMail(rs.getString("mail"));
                maison.setImage(rs.getString("image"));
                maison.setAdresse(rs.getString("adresse"));
                maison.setSite_web(rs.getString("site_web"));
                maison.setPrix(rs.getDouble("prix"));
            
                maisonsObserv.add(maison);

            
        } }
        catch (SQLException ex) {
            System.err.println(ex.getMessage());       
        }
    
        
                System.out.println(maisonsObserv);
 
    return maisonsObserv;
      }
      /***********************************************************************************************/
    
    
    
    
     //**************************************    Rechercher par pays hôte    ***********************************    
     

      public List<Maisons_hotes> FindByPays(String key){
          
        ObservableList<Maisons_hotes> maisonsObserv=FXCollections.observableArrayList();
    
        try {
            
            
            String req="SELECT * FROM maisons_hotes where pays LIKE '"+key+"' ";
            Statement st= c.createStatement();
           
            PreparedStatement ps = c.prepareStatement(req);
            ResultSet rs= st.executeQuery(req);
                        
            
            while(rs.next()){
                
                
                   
                
              Maisons_hotes maison = new Maisons_hotes();
                
             
                maison.setId(rs.getInt("id"));
                maison.setNom(rs.getString("nom"));
                maison.setDescription(rs.getString("description"));
                maison.setPays(rs.getString("pays"));
                maison.setGouvernorat(rs.getString("gouvernorat"));
                maison.setCapacites(rs.getInt("capacites"));
                maison.setTel(rs.getInt("tel"));
                maison.setMail(rs.getString("mail"));
                maison.setImage(rs.getString("image"));
                maison.setAdresse(rs.getString("adresse"));
                maison.setSite_web(rs.getString("site_web"));
                maison.setPrix(rs.getDouble("prix"));
            
                maisonsObserv.add(maison);

            
        } }
        catch (SQLException ex) {
            System.err.println(ex.getMessage());       
        }
    
        
                System.out.println(maisonsObserv);
 
    return maisonsObserv;
      }
      /***********************************************************************************************/
    
    
    public int UserResponsable() throws SQLException{

          
         String req="SELECT COUNT(*) as nb FROM fos_user  u WHERE u.id=?  and roles  LIKE '%ROLE_RESPONSABLE_HOTE%' ";
            Statement st= c.createStatement();  
            PreparedStatement ps = c.prepareStatement(req);
              ps.setInt(1,getUserId());
            ResultSet rs= ps.executeQuery();
            int nb=0;
            if (rs != null)
            {
                while(rs.next()){ 
                    nb=rs.getInt("nb");
                }
            }
            System.out.println("********** Role :::   "+ nb);
            return nb;  
    }
    
    
    
    
    

}
