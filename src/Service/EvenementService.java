/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import static Core.Controller.getUserId;
import Entities.Evenement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import utils.DataSource;

/**
 *
 * @author soumaya
 */
public class EvenementService {

    private Connection con = DataSource.getInstance().getConnection();

    public Evenement getEvenementById(int id) {
        try {
            String query = "select * from evenement where id = ?";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            Evenement evenement;
            while (rs.next()) {
                evenement = new Evenement(rs.getInt("id"), rs.getString("image"), rs.getInt("nbreplace"), rs.getDate("dateDebut"), rs.getString("nomEvenement"), rs.getString("description"), rs.getString("type"), rs.getDate("dateFin"), rs.getDouble("prix"), rs.getString("adr"));
                return evenement;
            }
        } catch (SQLException ex) {
            Logger.getLogger(EvenementService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Evenement getEvenementByAventure(String s) {
        try {
            String query = "select * from evenement where type = 'Aventure'";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, s);
            ResultSet rs = ps.executeQuery();
            Evenement evenement;
            while (rs.next()) {
                evenement = new Evenement(rs.getInt("id"), rs.getString("image"), rs.getInt("nbreplace"), rs.getDate("dateDebut"), rs.getString("nomEvenement"), rs.getString("description"), rs.getString("type"), rs.getDate("dateFin"), rs.getDouble("prix"), rs.getString("adr"));
                return evenement;
            }
        } catch (SQLException ex) {
            Logger.getLogger(EvenementService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Evenement insertEvenement(Evenement e) {
        try {

            String query = "INSERT INTO `evenement`(`image`, `nbreplace`, `dateDebut`, `nomEvenement`, `description`, `type`, `dateFin`, `prix`, `adr`,`responsable`) VALUES(?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement prs = con.prepareStatement(query);

            prs.setString(1, e.getImageEve());

            prs.setInt(2, e.getNbplaces());
            prs.setDate(3, e.getDateEvenement());
            prs.setString(4, e.getTitre());
            prs.setString(5, e.getDescription());
            prs.setString(6, e.getTitreCordination());
            prs.setDate(7, e.getDatefin());
            prs.setDouble(8, e.getPrix());
            prs.setString(9, e.getAdr());
                        prs.setInt(10,getUserId());

            int id = prs.executeUpdate();
            e.setId(id);
            return e;
        } catch (SQLException ex) {
            Logger.getLogger(EvenementService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ObservableList<Evenement> getAll() {

        try {
            String query = "select * from evenement";
            Statement ste = con.createStatement();
            ResultSet rs = ste.executeQuery(query);
            ObservableList<Evenement> evenements = FXCollections.observableArrayList();
            while (rs.next()) {
                evenements.add(new Evenement(rs.getInt("id"), rs.getInt("nbreplace"), rs.getDate("dateDebut"),
                        rs.getString("nomEvenement"), rs.getString("description"), rs.getString("type"),
                        rs.getDate("dateFin"), rs.getDouble("prix"), rs.getString("adr")));

            }
            return evenements;
        } catch (SQLException ex) {
            Logger.getLogger(EvenementService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ObservableList<Evenement> getEv() {
        try {
            String query = "select * from evenement WHERE dateDebut > NOW()";

            Statement ste = con.createStatement();
            ResultSet rs = ste.executeQuery(query);
            ObservableList<Evenement> evenements = FXCollections.observableArrayList();
            while (rs.next()) {
                evenements.add(new Evenement(rs.getInt("id"), rs.getString("image"), rs.getInt("nbreplace"), rs.getDate("dateDebut"), rs.getString("nomEvenement"), rs.getString("description"), rs.getString("type"), rs.getDate("dateFin"), rs.getDouble("prix"), rs.getString("adr")));
            }
            System.out.println("+++" + evenements.size());
            return evenements;
        } catch (SQLException ex) {
            Logger.getLogger(EvenementService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ObservableList<Evenement> getEv1() {
        try {
            String query = "select * from evenement WHERE responsable=?";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, getUserId());
            ResultSet rs = ps.executeQuery();
            ObservableList<Evenement> evenements = FXCollections.observableArrayList();
            while (rs.next()) {
                evenements.add(new Evenement(rs.getInt("id"), rs.getString("image"), rs.getInt("nbreplace"), rs.getDate("dateDebut"), rs.getString("nomEvenement"), rs.getString("description"), rs.getString("type"), rs.getDate("dateFin"), rs.getDouble("prix"), rs.getString("adr")));
            }
            System.out.println("+++" + evenements.size());
            return evenements;
        } catch (SQLException ex) {
            Logger.getLogger(EvenementService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public boolean deleteEvenement(int ide) {
        try {
            Connection con = DataSource.getInstance().getConnection();
            String query = "DELETE from evenement where id=?";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, ide);
            ps.executeUpdate();
            System.out.println("Suppression avec SuccÃ©s !");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(EvenementService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public void updateEvenement(Evenement e, int id) {

        try {
            String query = "Update evenement set nomEvenement = ?,type=?,description=?,dateDebut=?,dateFin=?,prix=?,adr=?,nbreplace=? where id = " + id;
            PreparedStatement prs = con.prepareStatement(query);

            prs.setString(1, e.getTitre());
            prs.setString(2, e.getTitreCordination());
            prs.setString(3, e.getDescription());
            prs.setDate(4, (Date) e.getDateEvenement());
            prs.setDate(5, (Date) e.getDatefin());
            prs.setDouble(6, e.getPrix());
            prs.setString(7, e.getAdr());
            prs.setInt(8, e.getNbplaces());
            prs.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(EvenementService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
