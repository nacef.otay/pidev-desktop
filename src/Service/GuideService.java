/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;


import Entities.Evenement;
import Entities.Guide;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import utils.DataSource;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import utils.DataSource;


/**
 *
 * @author soumaya
 */
public class GuideService {
    
    private Connection con = DataSource.getInstance().getConnection();
     
      public Guide getGuideById(int id) {
        try {
            String query = "select * from guide where id = ?";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            Guide guide;
            while (rs.next()) {
               guide = new Guide(rs.getInt("id"), rs.getString("nomevent"),rs.getString("eventType") , rs.getString("nom"), rs.getString("prenom"), rs.getString("mail"),rs.getInt("tel"));
                return guide;
            }
        } catch (SQLException ex) {
            Logger.getLogger(GuideService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
      
      public ObservableList<Guide> getGuidesByEventId(int idEvent) {
        try {
            String query = "select * from guide where evenement = ?";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, idEvent);
            ResultSet rs = ps.executeQuery();
            ObservableList<Guide> guides = FXCollections.observableArrayList();
            while (rs.next()) {
 guides.add(new Guide(rs.getInt("id"), rs.getString("nomevent"),rs.getString("eventType") , rs.getString("nom"), rs.getString("prenom"), rs.getString("mail"),rs.getInt("tel")));

            }
            return guides;
        } catch (SQLException ex) {
            Logger.getLogger(GuideService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
      
       public Guide insertGuide(Guide e) {
        try {
           
            String query = "INSERT INTO `guide`(`nomevent`, `nom`, `prenom`, `mail`, `tel`, `eventType`,`evenement`) VALUES(?,?,?,?,?,?,?)";
            PreparedStatement prs = con.prepareStatement(query);
            
            prs.setString(1, e.getNomEvent());
            prs.setInt(7,Evenement.getEvent_courant());
            prs.setString(2, e.getNom());
            prs.setString(3, e.getPrenom());
            prs.setString(4, e.getMail());
            prs.setInt(5, e.getTel());
            prs.setString(6, e.getEventType());
            
        int id = prs.executeUpdate();
            e.setId(id);
            return e;
        } catch (SQLException ex) {
            Logger.getLogger(GuideService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

        public ObservableList<Guide> getAll() {

        try {
            String query = "select * from guide";
            Statement ste = con.createStatement();
            ResultSet rs = ste.executeQuery(query);
            ObservableList<Guide> guides = FXCollections.observableArrayList();
            while (rs.next()) {
       guides.add( new Guide(rs.getInt("id"), rs.getString("nomevent"),rs.getString("eventType") , rs.getString("nom"), rs.getString("prenom"), rs.getString("mail"),rs.getInt("tel")));

               
            }
            return guides;
        } catch (SQLException ex) {
            Logger.getLogger(GuideService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
   
    public ObservableList<Guide> getGu() {
        try {
            String query = "select * from guide";

            Statement ste = con.createStatement();
            ResultSet rs = ste.executeQuery(query);
            ObservableList<Guide> guides = FXCollections.observableArrayList();
            while (rs.next()) {
 guides.add(new Guide(rs.getInt("id"), rs.getString("nomevent"),rs.getString("eventType") , rs.getString("nom"), rs.getString("prenom"), rs.getString("mail"),rs.getInt("tel")));

            }
            System.out.println("+++"+guides.size());
            return guides;
        } catch (SQLException ex) {
            Logger.getLogger(GuideService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
     public boolean deleteGuide(int ide) {
        try {
            Connection con = DataSource.getInstance().getConnection();
            String query = "DELETE from guide where id=?";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, ide);
            ps.executeUpdate();
            System.out.println("Suppression avec Succées !");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(GuideService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
     
     public void updateGuide(Guide e, int id) {

        try {
            String query ="Update guide set nomevent = ?,nom=?,prenom=?,mail=?,tel=?,eventType=? where id = "+id;
            
            PreparedStatement prs = con.prepareStatement(query);
            
            prs.setString(1,e.getNomEvent());
            prs.setString(2,e.getNom());
            prs.setString(3,e.getPrenom());
            prs.setString(4,e.getMail());
            prs.setInt(5, e.getTel());
            prs.setString(6, e.getEventType());
            prs.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(GuideService.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }

    public void deleteGuide(Guide guideCourant) {
       
        String query = "delete from guide where id=?";
        PreparedStatement ps;
        try {
            ps = con.prepareStatement(query);
            ps.setInt(1, guideCourant.getId());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(GuideService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
    


}
