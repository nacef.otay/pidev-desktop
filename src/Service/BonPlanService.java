/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import static Core.Controller.getUserId;
import GUI.ProfilController;
import Entities.Bonplan;
import Entities.User;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableArray;
import javafx.collections.ObservableList;
import utils.DataSource;

/**
 *
 * @author marie
 */
public class BonPlanService {
     Connection c= DataSource.getInstance().getConnection();

  
         /***********************************  Ajouterreclamation   **************************************/
 
    public void ajouterBonPlan(Bonplan b ){
         
        
        try {
            Statement st= c.createStatement();
            String req = "INSERT INTO bonplan (name,description,phone,adresse,image,"
                    + "etoile,note,prix,categorie_name,user_id) VALUES ( ?,?,?,?,?,?,?,?,?,?)";
            
            PreparedStatement ps = c.prepareStatement(req);
            
             ps.setString(1, b.getName());
             ps.setString(2, b.getDescription());
             ps.setString(3, b.getPhone());
             ps.setString(4,b.getAdresse());
             ps.setString(5, b.getImage());
             ps.setInt(6, b.getEtoile());
             ps.setInt(7,b.getNote());
             ps.setDouble(8,b.getPrix());
             ps.setObject(9, b.getCategorie());
             ps.setInt(10,getUserId());
             System.out.println("******************");

           //  ps.setDate(9,b.getDatePublication());
            System.out.println(b.getCategorie());
             System.out.println(ps);
              ps.executeUpdate();
              System.out.println("Bon plan  ajouté avec succès"); 
                      
        } catch (SQLException ex) {
            Logger.getLogger(BonPlanService.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }
    ///////////////////////////////////////////////////////////////////////////////
    
    /****************** Afficher Maison d'hôte  ***************************/
    
    public ArrayList<Bonplan> AfficherBonPlan() {
        ArrayList<Bonplan> myList = new ArrayList<Bonplan>();
        try {

            String req = "SELECT * FROM bonplan";
            Statement st = c.createStatement();
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {

               Bonplan bonplan= new Bonplan();
                
          
                bonplan.setName(rs.getString("name"));
                bonplan.setDescription(rs.getString("description"));
                bonplan.setPhone(rs.getString("phone"));
                bonplan.setImage(rs.getString("image"));
                bonplan.setAdresse(rs.getString("adresse"));
                bonplan.setPrix(rs.getDouble("prix"));
                bonplan.setEtoile(rs.getInt("etoile"));
                bonplan.setNote(rs.getInt("note"));
                bonplan.setDatePublication(new java.util.Date());
                bonplan.setId(rs.getInt("id"));
                bonplan.setCategorie(rs.getString("categorie_name"));
                myList.add(bonplan);

            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return myList;

    }
    /////////////////////////////////////////////////////////////////////////////////
    
    public List<Bonplan> list()
    { ObservableList<Bonplan> Bonplans=FXCollections.observableArrayList();
    
     String req = "SELECT * FROM bonplan";
            Statement st;
            try {
                st = c.createStatement();
                ResultSet rs = st.executeQuery(req);
                 while (rs.next()) {

                Bonplan bonplan = new Bonplan();
                
          
                bonplan.setName(rs.getString("name"));
                bonplan.setDescription(rs.getString("description"));
                bonplan.setPhone(rs.getString("phone"));
                bonplan.setImage(rs.getString("image"));
                bonplan.setAdresse(rs.getString("adresse"));
                bonplan.setPrix(rs.getDouble("prix"));
                bonplan.setEtoile(rs.getInt("etoile"));
                bonplan.setNote(rs.getInt("note"));
                bonplan.setDatePublication(new java.util.Date());
                bonplan.setId(rs.getInt("id"));
                bonplan.setCategorie(rs.getString("categorie_name"));

                Bonplans.add(bonplan);

            }
                
            } catch (SQLException ex) {
                Logger.getLogger(BonPlanService.class.getName()).log(Level.SEVERE, null, ex);
            }
            
    return Bonplans;

    }
    
    //////////////////////////////////////////////////////////////////
     public void SupprimerBonPlan(Bonplan b) {
        try {
           
            String req = "Delete from bonplan Where id=?";
            PreparedStatement pst2 = c.prepareStatement(req);
            pst2.setInt(1, b.getId());
            System.out.println("suppression !!! ");
            pst2.executeUpdate();
        } 
        catch (SQLException ex) {
           System.err.println(ex.getMessage());
        }

     }
     
     /////////////////MODIF //////////////
     public void ModifierBonPlan(Bonplan b,int id){

                  try {
            String requete = "UPDATE  bonplan SET name=?,description=?,adresse=?,prix=?,etoile=?,image=?,note=?,phone=?"
                    + " WHERE id=?";
         Statement st= c.createStatement();
           
            PreparedStatement ps = c.prepareStatement(requete);
            
            ps.setString(1, b.getName());
            ps.setString(2, b.getDescription());
           // ps.setString(3, m.getPays());
            ps.setString(3, b.getAdresse());
            ps.setDouble(4, b.getPrix());
            ps.setInt(5, b.getEtoile());
            ps.setString(6, b.getImage());
            ps.setInt(7, b.getNote());
            ps.setString(8, b.getPhone());
            ps.setInt(9, b.getId());
          
            ps.executeUpdate();
            
                      System.out.println(b.getId());
           
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
 }
     
     ///////////propre bon plan de user
     
      public ArrayList<Bonplan> listBonPlanPropreUser()
    {
    
        ArrayList<Bonplan>Bonplans = new ArrayList<Bonplan>();
    
     String req = "SELECT * FROM bonplan where user_id=?";
            Statement st;
            try {
                st = c.createStatement();
                PreparedStatement ps = c.prepareStatement(req);

               ps.setInt(1, getUserId());
          
                ResultSet rs =ps.executeQuery();
                 while (rs.next()) {

                Bonplan bonplan = new Bonplan();
                
          
                bonplan.setName(rs.getString("name"));
                bonplan.setDescription(rs.getString("description"));
                bonplan.setPhone(rs.getString("phone"));
                bonplan.setImage(rs.getString("image"));
                bonplan.setAdresse(rs.getString("adresse"));
                bonplan.setPrix(rs.getDouble("prix"));
                bonplan.setEtoile(rs.getInt("etoile"));
                bonplan.setNote(rs.getInt("note"));
                bonplan.setDatePublication(new java.util.Date());
                bonplan.setId(rs.getInt("id"));
                bonplan.setCategorie(rs.getString("categorie_name"));

                Bonplans.add(bonplan);

            }
                
            } catch (SQLException ex) {
                Logger.getLogger(BonPlanService.class.getName()).log(Level.SEVERE, null, ex);
            }
            
    return Bonplans;

    }
      
      //////recherche nom
      
      public List<Bonplan> SearchBonPlanName (String key){
          
        ObservableList<Bonplan> bonplansobserv =FXCollections.observableArrayList();
    
        try {
            
            
            String req="SELECT * FROM bonplan where name LIKE '%"+key+"%' ";
            Statement st= c.createStatement();
           
            PreparedStatement ps = c.prepareStatement(req);
            ResultSet rs= st.executeQuery(req);
                        
            
            while(rs.next()){
               Bonplan bon = new Bonplan();
                bon.setId(rs.getInt("id"));
                bon.setName(rs.getString("name"));
                bon.setAdresse(rs.getString("adresse"));
                bon.setPhone(rs.getString("phone"));
                bon.setImage(rs.getString("image"));
                bon.setPrix(rs.getDouble("prix"));
                bon.setCategorie(rs.getString("categorie_name"));
                bon.setNote(rs.getInt("note"));
                bon.setEtoile(rs.getInt("etoile"));
                bon.setDatePublication(rs.getDate("datePublication"));
                
            
                bonplansobserv.add(bon);

            
        } }
        catch (SQLException ex) {
            System.err.println(ex.getMessage());       
        }
    
    return bonplansobserv;
      }
      ////////
          //////recherche nom
      
      public ArrayList<Bonplan> SearchBonPlanNameList (String key){
          
        // ObservableArray<Bonplan> bonplansobserv =FXCollections.observableArrayList();
    ArrayList<Bonplan>  bonplansobserv= new ArrayList<Bonplan>();
        try {
            
            
            String req="SELECT * FROM bonplan where name LIKE '%"+key+"%' ";
            Statement st= c.createStatement();
           
            PreparedStatement ps = c.prepareStatement(req);
            ResultSet rs= st.executeQuery(req);
                        
            
            while(rs.next()){
               Bonplan bon = new Bonplan();
                bon.setId(rs.getInt("id"));
                bon.setName(rs.getString("name"));
                bon.setAdresse(rs.getString("adresse"));
                bon.setPhone(rs.getString("phone"));
                bon.setImage(rs.getString("image"));
                bon.setPrix(rs.getDouble("prix"));
                bon.setCategorie(rs.getString("categorie_name"));
                bon.setNote(rs.getInt("note"));
                bon.setEtoile(rs.getInt("etoile"));
                bon.setDatePublication(rs.getDate("datePublication"));
                
            
                bonplansobserv.add(bon);

            
        } }
        catch (SQLException ex) {
            System.err.println(ex.getMessage());       
        }
    
    return bonplansobserv;
      }
      ////////
       public int calculerNbreBonPlanParCategorie(String id) throws SQLException
    {
       
        int a=0;
      String req = "SELECT count(*) as nb FROM bonplan where categorie_name=?";
        PreparedStatement ste = c.prepareStatement(req);
        ste.setString(1, id);
        ResultSet rs = ste.executeQuery(); 
        while(rs.next()){
         a=rs.getInt("nb");
        }
    
    return a;
}
       //////////////
       
    public List<Bonplan> BonsPlansPlusNotes()
    { ObservableList<Bonplan> Bonplans=FXCollections.observableArrayList();
    
     String req = "SELECT * FROM bonplan order By note desc limit 4";
            Statement st;
            try {
                st = c.createStatement();
                ResultSet rs = st.executeQuery(req);
                 while (rs.next()) {

                Bonplan bonplan = new Bonplan();
                
          
                bonplan.setName(rs.getString("name"));
                bonplan.setDescription(rs.getString("description"));
                bonplan.setPhone(rs.getString("phone"));
                bonplan.setImage(rs.getString("image"));
                bonplan.setAdresse(rs.getString("adresse"));
                bonplan.setPrix(rs.getDouble("prix"));
                bonplan.setEtoile(rs.getInt("etoile"));
                bonplan.setNote(rs.getInt("note"));
                bonplan.setDatePublication(new java.util.Date());
                bonplan.setId(rs.getInt("id"));
                bonplan.setCategorie(rs.getString("categorie_name"));

                Bonplans.add(bonplan);

            }
                
            } catch (SQLException ex) {
                Logger.getLogger(BonPlanService.class.getName()).log(Level.SEVERE, null, ex);
            }
            return Bonplans;
    }
       
      /////////////////////
           //////////////
       
    public List<Bonplan> BonsPlansByRating()
    { ObservableList<Bonplan> Bonplans=FXCollections.observableArrayList();
    
     String req = "SELECT * FROM bonplan order By etoile desc limit 4";
            Statement st;
            try {
                st = c.createStatement();
                ResultSet rs = st.executeQuery(req);
                 while (rs.next()) {

                Bonplan bonplan = new Bonplan();
                
          
                bonplan.setName(rs.getString("name"));
                bonplan.setDescription(rs.getString("description"));
                bonplan.setPhone(rs.getString("phone"));
                bonplan.setImage(rs.getString("image"));
                bonplan.setAdresse(rs.getString("adresse"));
                bonplan.setPrix(rs.getDouble("prix"));
                bonplan.setEtoile(rs.getInt("etoile"));
                bonplan.setNote(rs.getInt("note"));
                bonplan.setDatePublication(new java.util.Date());
                bonplan.setId(rs.getInt("id"));
                bonplan.setCategorie(rs.getString("categorie_name"));

                Bonplans.add(bonplan);

            }
                
            } catch (SQLException ex) {
                Logger.getLogger(BonPlanService.class.getName()).log(Level.SEVERE, null, ex);
            }
            return Bonplans;
    }
    ///////////////////
    
 
      public List<Bonplan> FindByCategorie(String key){
          
        ObservableList<Bonplan> Bonplans=FXCollections.observableArrayList();
    
        try {
            
            
            String req="SELECT * FROM bonplan where categorie_name LIKE '"+key+"' ";
            Statement st= c.createStatement();
           
            PreparedStatement ps = c.prepareStatement(req);
            ResultSet rs= st.executeQuery(req);
                        
            
            while(rs.next()){
                
                
                   
                
              Bonplan bonplan = new Bonplan();
                
          
                bonplan.setName(rs.getString("name"));
                bonplan.setDescription(rs.getString("description"));
                bonplan.setPhone(rs.getString("phone"));
                bonplan.setImage(rs.getString("image"));
                bonplan.setAdresse(rs.getString("adresse"));
                bonplan.setPrix(rs.getDouble("prix"));
                bonplan.setEtoile(rs.getInt("etoile"));
                bonplan.setNote(rs.getInt("note"));
                bonplan.setDatePublication(new java.util.Date());
                bonplan.setId(rs.getInt("id"));
                bonplan.setCategorie(rs.getString("categorie_name"));

                Bonplans.add(bonplan);

            
        } }
        catch (SQLException ex) {
            System.err.println(ex.getMessage());       
        }
    
    return Bonplans;
      }
   
    
    /////////////
      //////
     /* public Bonplan getBonPlanById(int id) {
                      Bonplan b = null;

        try {
            String req = "select * from bonplan where id=?";
            Statement st= c.createStatement();
           
            PreparedStatement ps = c.prepareStatement(req);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
              User user = new User();
              b=new Bonplan();
           b.setAdresse( rs.getString("adresse"));
           b.setCategorie( rs.getString("categorie"));
           b.setDescription(rs.getString("description"));
           b.setEtoile( rs.getInt("etoile"));
           b.setImage( rs.getString("image"));
           b.setId(id);
           b.setName(rs.getString("name"));
           b.setPhone( rs.getString("phone"));
           b.setPrix(rs.getDouble("prix"));
           b.setNote(rs.getInt("note"));
           b.setUser(user);
                        
                        }
        } catch (SQLException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
            return b;
    }*/
    
    }
