/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import Entities.category;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import utils.DataSource;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author ZerOo
 */
public class ServiceCategory {
     Connection c = DataSource.getInstance().getConnection();
     
     public List<category> ListCategory() {
        List ALLproducts = new ArrayList();
        try {
            String query = "select * from category";
            Statement st = DataSource.getInstance().getConnection().createStatement();
            ResultSet rest = st.executeQuery(query);
            while (rest.next()) {
                category ct = new category();

                ct.setId(rest.getInt("id"));
                ct.setCategory(rest.getString("category"));

                ALLproducts.add(ct);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ServiceProduit.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ALLproducts;
    }
     
     public category getCategoryByName(String rname) {

        category rg = new category();
        try {
            String query = "select * from category where category = ?";
            PreparedStatement ps;

            ps = c.prepareStatement(query);
            ps.setString(1, rname);
            ResultSet rest = ps.executeQuery();

            while (rest.next()) {
                rg.setId(rest.getInt("id"));
                rg.setCategory(rest.getString("category"));
            }

        } catch (SQLException ex) {
            Logger.getLogger(ServiceProduit.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rg;

    }

   public void ajouterCategory(category cat) {
        try {
            String req = "INSERT INTO category ( category) VALUES (?)";
            PreparedStatement pre = c.prepareStatement(req);
           
            pre.setString(1,cat.getCategory());
            pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ServiceCategory.class.getName()).log(Level.SEVERE, null, ex);
        }
    }  
    public boolean findCategoryByName(String rname) {

        category rg = new category();
        try {
            String query = "select * from category where category = ?";
            PreparedStatement ps;

            ps = c.prepareStatement(query);
            ps.setString(1, rname);
            ResultSet rest = ps.executeQuery();

            if (rest.next()) {
                return true;
            }

        } catch (SQLException ex) {
            Logger.getLogger(ServiceProduit.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
}
     public void supprimerCategorie(int id) {

        try {
            PreparedStatement pt = c.prepareStatement("delete from category where id=?");

            pt.setInt(1, id);

            pt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(ServiceCategory.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
