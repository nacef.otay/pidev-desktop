/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import static Core.Controller.getUserId;
import Entities.Demande_responsable_hote;
import Entities.Maisons_hotes;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DataSource;
import static Core.Controller.getUserId;
import Entities.User;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Bhs Nada
 */
public class DemandeRespoHoteService {
      Connection c= DataSource.getInstance().getConnection();
      
         /***********************************  Ajouter Demande Responsable   **************************************/
 
    public void ajouterDemande(Demande_responsable_hote d){
         
        
        try {
            Statement st= c.createStatement();
            String req = "INSERT INTO demande_responsable_hote (user_id,description,date) VALUES ( ?,?,?)";
            
            PreparedStatement ps = c.prepareStatement(req);
            
             ps.setInt(1,getUserId());
             ps.setString(2, d.getDescription());
             ps.setDate(3, (Date) d.getDate());
             
             System.out.println(ps);
              ps.executeUpdate();
              System.out.println("Demande Responsable Envoyer "); 
                      
        } catch (SQLException ex) {
            Logger.getLogger(MaisonHoteService.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }
    ///////////////////////////////////////////////////////////////////////////////
     /****************** Afficher Demande Respo  ***************************/
    
    public ArrayList<Demande_responsable_hote> AfficherDemande() {
        ArrayList<Demande_responsable_hote> myList = new ArrayList<Demande_responsable_hote>();
        try {

            String req = "SELECT * FROM demande_responsable_hote";
            Statement st = c.createStatement();
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {

                Demande_responsable_hote demande = new Demande_responsable_hote();
                
                demande.setId(rs.getInt("id"));
               // demande.setId_user((User) rs.getObject("user_id"));
                demande.setDescription(rs.getString("description"));
                demande.setDate(rs.getDate("date"));
                myList.add(demande);

            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return myList;

    }
    /////////////////////////////////////////////////////////////////////////////////
    public void AjouterRole(User id) throws SQLException
    {
           String requete = "UPDATE  fos_user AS U SET U.roles='a:1:{i:0;s:21:\"ROLE_RESPONSABLE_HOTE\";}' WHERE U.id=(SELECT user_id from demande_responsable_hote AS d WHERE d.user_id=U.id)";
         Statement st= c.createStatement();
           
            PreparedStatement ps = c.prepareStatement(requete);
             ps.executeUpdate();
            
    }
        //////////////////////////////////////////////////////////////////
     public void SupprimerDemande(Demande_responsable_hote m) {
        try {
           
            String req = "Delete from demande_responsable_hote Where id=?";
            PreparedStatement pst2 = c.prepareStatement(req);
            pst2.setInt(1, m.getId());
            System.out.println("suppression Demande !!! ");
            pst2.executeUpdate();
        } 
        catch (SQLException ex) {
           System.err.println(ex.getMessage());
        }

    }

    ///////////////////////////////////////////////////////////////////////////
}
