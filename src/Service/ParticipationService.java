/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import static Core.Controller.getUserId;
import Entities.Evenement;
import Entities.User;
import com.sun.jna.platform.win32.Sspi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DataSource;

/**
 *
 * @author hero
 */
public class ParticipationService {

    private Connection con = DataSource.getInstance().getConnection();
//     {
//         try {
//             String req="UPDATE `event` SET `participants`=? WHERE `id`=?";
//             PreparedStatement prs = con.prepareStatement(req);
//             prs.setInt(1,(ev.getNbreParticipants())+1);
//             prs.setInt(2,ev.getId());
//             prs.executeUpdate();
//             System.out.println("services.ServiceEvents.increment()");  
//  
//         } catch (SQLException e) {
//             System.out.println("404");
//         }
//         
//     }

    public void increment(Evenement ev, User user) {
        Calendar c = Calendar.getInstance();
        Timestamp ts = new Timestamp(c.getTimeInMillis());
        try {
            String req = "UPDATE `evenement` SET `nbreplace`=? WHERE `id`=?";
            PreparedStatement prs = con.prepareStatement(req);
            prs.setInt(1, (ev.getNbplaces()) + 1);
            prs.setInt(2, ev.getId());
            prs.executeUpdate();
            req = "INSERT INTO participants(date_inscrit,userId,event_id,nomEvent) VALUES(?,?,?,?)";
            prs = con.prepareStatement(req);
            prs.setTimestamp(1, ts);
            prs.setInt(3, ev.getId());
            prs.setInt(2, getUserId());
            prs.setString(4, ev.getTitre());

            prs.executeUpdate();
        } catch (SQLException e) {
            System.out.println(ev.getId());
            System.out.println(user.getId());
            System.out.println("404404");
        }

    }

    public void decrement(Evenement ev, User user) {
        try {
            String req = "UPDATE `evenement` SET `nbreplace`=? WHERE `id`=?";
            PreparedStatement prs = con.prepareStatement(req);
            prs.setInt(1, (ev.getNbplaces()) + 1);
            prs.setInt(2, ev.getId());
            prs.executeUpdate();
            req = "DELETE FROM participants WHERE event_id = ? and userId=?";
            prs = con.prepareStatement(req);
            prs.setInt(1, ev.getId());
            prs.setInt(2, getUserId());
            prs.executeUpdate();

        } catch (SQLException e) {
            System.out.println("404");
        }

    }

    public boolean checkParticipation(Evenement evenement, User user) {
        String query = "select * from participants where userId = ? and event_id = ?";
        try {
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(2, evenement.getId());
            ps.setInt(1, getUserId());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ParticipationService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
}
