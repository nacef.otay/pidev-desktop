/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;


import static Core.Controller.getUserId;
import Entities.AvisEvenement;
import Entities.Evenement;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import utils.DataSource;

/**
 *
 * @author hero
 */
public class AvisEvenementService {
    private Connection con = DataSource.getInstance().getConnection();
    
    
    public int insertAvis(String contenu)  {
       
           
        try {
            String query = "INSERT INTO `avisevenement`(`contenu`,`idEvenement`,`idUser`) VALUES(?,?,?)";
            
            PreparedStatement prs = con.prepareStatement(query);
            
            prs.setString(1,contenu);
            prs.setInt(2, Evenement.getEvent_courant());
            prs.setInt(3, getUserId());
            
            int executeUpdate = prs.executeUpdate();
            
            return executeUpdate;
        } catch (SQLException ex) {
            Logger.getLogger(AvisEvenementService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
     
       
        

    }
    public ObservableList<AvisEvenement> getAvis() {
        try {
            String query = "select * from avisevenement";

            Statement ste = con.createStatement();
            ResultSet rs = ste.executeQuery(query);
            ObservableList<AvisEvenement> avis_evenements = FXCollections.observableArrayList();
            while (rs.next()) {
                avis_evenements.add(new AvisEvenement(rs.getString("contenu"), rs.getInt("IdEvenement")));
            }
            return avis_evenements;
        } catch (SQLException ex) {
            Logger.getLogger(EvenementService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public ObservableList<AvisEvenement> getAvis(int idEvenement) {
        try {
            String query = "select * from avisevenement where IdEvenement ="+idEvenement;

            Statement ste = con.createStatement();
            ResultSet rs = ste.executeQuery(query);
            ObservableList<AvisEvenement> avis_evenements = FXCollections.observableArrayList();
            while (rs.next()) {
                avis_evenements.add(new AvisEvenement(rs.getString("contenu"), rs.getInt("IdEvenement")));
            }
            return avis_evenements;
        } catch (SQLException ex) {
            Logger.getLogger(EvenementService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}

