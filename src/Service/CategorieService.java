/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import Entities.Categorie;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import utils.DataSource;

/**
 *
 * @author marie
 */
public class CategorieService {
     Connection c= DataSource.getInstance().getConnection();
        
         /***********************************  Ajouterreclamation   **************************************/
 
    public void ajouterCatgeorie(Categorie categorie ){
         
        
        try {
            Statement st= c.createStatement();
            String req = "INSERT INTO categorie (type) VALUES (?)";
            
            PreparedStatement ps = c.prepareStatement(req);
            
             ps.setString(1, categorie.getType());
             
              ps.executeUpdate();
            //  System.out.println("Catégorie ajouté avec succès"); 
                      
        } catch (SQLException ex) {
            Logger.getLogger(CategorieService.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }
    ///////////////////////////////////////////////////////////////////////////////
    
    
    public ArrayList<Categorie> AfficherCategorie() {
        ArrayList<Categorie> myList = new ArrayList<Categorie>();
        try {

            String req = "SELECT * FROM categorie";
            Statement st = c.createStatement();
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {

               Categorie categorie= new Categorie();
                
                categorie.setId(rs.getInt("id"));
                categorie.setType(rs.getString("type"));
                
            
                myList.add(categorie);

            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return myList;

    }
    /////////////////////////////////////////////////////////////////////////////////
    
    public List<String> list()
    {
    
    ObservableList<String> Categories=FXCollections.observableArrayList();
    
     String req = "SELECT type FROM categorie";
            Statement st;
            try {
                st = c.createStatement();
                ResultSet rs = st.executeQuery(req);
                 while (rs.next()) {

                Categorie categorie = new Categorie();
                
          
                categorie.setType(rs.getString("type"));
                
            
                Categories.add(rs.getString("type"));

                
            }
                
            } catch (SQLException ex) {
                Logger.getLogger(CategorieService.class.getName()).log(Level.SEVERE, null, ex);
            }
            
    return Categories;

    }
    
    //////////////////////////////////////////////////////////////////
     public void SupprimerCategorie(Categorie categorie) {
        try {
           
            String req = "Delete from categorie Where id=?";
            PreparedStatement pst2 = c.prepareStatement(req);
            pst2.setInt(1, categorie.getId());
            System.out.println("suppression !!! ");
            pst2.executeUpdate();
        } 
        catch (SQLException ex) {
           System.err.println(ex.getMessage());
        }

     }
     
     /////////////////MODIF //////////////
     public void ModifierCategorie(Categorie categorie,int id){

                  try {
            String requete = "UPDATE  categorie SET type=?"
                    + " WHERE id=?";
         Statement st= c.createStatement();
           
            PreparedStatement ps = c.prepareStatement(requete);
            
            ps.setString(1, categorie.getType());
           
            ps.setInt(2, categorie.getId());
          
            ps.executeUpdate();
            
                      System.out.println(categorie.getId());
           
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }


 }
     ////////
     public String getCategorie(String type){
         String test = null ;
          try {
            String requete = "select type from categorie"
                    + " WHERE type like '%?%'";
         Statement st= c.createStatement();
           
            PreparedStatement ps = c.prepareStatement(requete);
            
            ps.setString(1, type);
          
          test= Integer.toString(ps.executeUpdate());
              //System.out.println(test);
           
                       
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
         return test;
     }
     //////////////
     public int getCategorieName(String type){
         try {
            String requete = "select id from categorie"
                    + " WHERE type=?";
         Statement st= c.createStatement();
           
            PreparedStatement ps = c.prepareStatement(requete);
            
            ps.setString(1, type);
          
           return  ps.executeUpdate();
                       
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return 0;
     }
}
