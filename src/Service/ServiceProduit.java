/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import static Core.Controller.getUserId;
import Entities.Produit;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import utils.DataSource;
import utils.SendMail;

/**
 *
 * @author hp
 */
public class ServiceProduit {

    Connection c = DataSource.getInstance().getConnection();

    public ServiceProduit() {
    }
    
    public void sendMailToUser(Produit r) {
        ServiceProduit service = new ServiceProduit();
        UserService service1 = new UserService();
        Runnable runnable
                = new Runnable() {
            public void run() {
                String emailAdress = service1.getUserById(r.getOwner()).getEmail();
                String emailSubject = "Paiement";
                String emailBody = "Votre Produit nommé " + r.getNom() + " a ete acheté "
                        ;
                SendMail.sendMail1(emailAdress, emailSubject, emailBody);
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    public void ajouterPrdouit(Produit p1) {
        String req = "insert into produit (id,nom,description,prix,quantity,date,owner,image_id,category,region) values (?,?,?,?,?,?,?,?,?,?)";
        java.util.Date d1 = new java.util.Date();
        Date d = new Date(d1.getTime());
        try {
            PreparedStatement ps = c.prepareStatement(req);
            ps.setInt(1, p1.getId());
            ps.setString(2, p1.getNom());
            ps.setString(3, p1.getDescription());
            ps.setFloat(4, p1.getPrix());
            ps.setInt(5, p1.getQuantity());
            ps.setDate(6, d);
            ps.setInt(7, getUserId());
            ps.setString(8, p1.getImage_id());
            ps.setInt(9, 3);
            ps.setInt(10, 3);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ServiceCommande.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Produit> ListProduits() {
        List ALLproducts = new ArrayList();
        try {
            String query = "select * from produit";
            Statement st = DataSource.getInstance().getConnection().createStatement();
            ResultSet rest = st.executeQuery(query);
            while (rest.next()) {
                Produit pr = new Produit();

                pr.setId(rest.getInt("id"));
                pr.setNom(rest.getString("nom"));
                pr.setDescription(rest.getString("description"));
                pr.setPrix(rest.getInt("prix"));
                pr.setQuantity(rest.getInt("quantity"));
                pr.setCategory(rest.getInt("category"));
                pr.setOwner(rest.getInt("owner"));
                pr.setImage_id(rest.getString("image_id"));
                ALLproducts.add(pr);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ServiceProduit.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ALLproducts;

    }

    public void afficherProduit() {

        try {
            Statement st = c.createStatement();

            String query = "select * from produit";
            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {
                System.out.println("Commande id: " + rs.getInt("id") + ": nom du produit: " + rs.getString("nom") + ":Catégorie: " + rs.getString("category") + ":Quantité du produit: " + rs.getInt("quantity") + ":Prix du produit: " + rs.getFloat("prix") + ":Description: " + rs.getString("description"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServiceProduit.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public Produit getAcualiteById(int idCourant) {

        Produit pr = new Produit();
        try {
            String query = "select * from produit where id = ?";
            PreparedStatement ps;

            ps = c.prepareStatement(query);
            ps.setInt(1, idCourant);
            ResultSet rest = ps.executeQuery();

            while (rest.next()) {
                pr.setId(rest.getInt("id"));
                pr.setNom(rest.getString("nom"));
                pr.setDescription(rest.getString("description"));
                pr.setPrix(rest.getInt("prix"));
                pr.setQuantity(rest.getInt("quantity"));
                pr.setCategory(rest.getInt("category"));
                pr.setOwner(rest.getInt("owner"));
                pr.setImage_id(rest.getString("image_id"));

            }

        } catch (SQLException ex) {
            Logger.getLogger(ServiceProduit.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pr;

    }

    public void modifierProduit(Produit p1) {
        try {
            //requete pre compiler
            PreparedStatement pt = c.prepareStatement("update produit set nom = ? ,description =?,prix=?,quantity=?,image_id =? where id =?");
            pt.setString(1, p1.getNom());
            pt.setString(2, p1.getDescription());
            pt.setInt(3, p1.getPrix());
            pt.setInt(4, p1.getQuantity());
            pt.setString(5, p1.getImage_id());
            pt.setInt(6, p1.getId());

            pt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ServiceProduit.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void supprimerProduit(int id) {

        try {
            PreparedStatement pt = c.prepareStatement("delete from produit where id=?");

            pt.setInt(1, id);

            pt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(ServiceCommande.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public List<Produit> RechercheActualiteParNom(String recherche) {

        List ALLproducts = new ArrayList();
        try {
            String query = "select * from produit WHERE nom  LIKE '%" + recherche + "%' or prix LIKE '%" + recherche + "%' ;";
            Statement st = c.createStatement();
            ResultSet rest = st.executeQuery(query);
            while (rest.next()) {
                Produit pr = new Produit();

                pr.setId(rest.getInt("id"));
                pr.setNom(rest.getString("nom"));
                pr.setDescription(rest.getString("description"));
                pr.setPrix(rest.getInt("prix"));
                pr.setQuantity(rest.getInt("quantity"));
                pr.setCategory(rest.getInt("category"));
                pr.setOwner(rest.getInt("owner"));
                ALLproducts.add(pr);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Service.ServiceProduit.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ALLproducts;

    }

    public List<Produit> My(int id) {

        List ALLproducts = new ArrayList();
        try {
            String query = "select * from produit WHERE owner  = " + id + " ;";
            Statement st = c.createStatement();
            ResultSet rest = st.executeQuery(query);
            while (rest.next()) {
                Produit pr = new Produit();

                pr.setId(rest.getInt("id"));
                pr.setNom(rest.getString("nom"));
                pr.setDescription(rest.getString("description"));
                pr.setPrix(rest.getInt("prix"));
                pr.setQuantity(rest.getInt("quantity"));
                pr.setCategory(rest.getInt("category"));
                pr.setOwner(rest.getInt("owner"));
                ALLproducts.add(pr);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Service.ServiceProduit.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ALLproducts;

    }
    
        public List<Produit> outre(int id) {

        List ALLproducts = new ArrayList();
        try {
            String query = "select * from produit WHERE owner  != " + id + " ;";
            Statement st = c.createStatement();
            ResultSet rest = st.executeQuery(query);
            while (rest.next()) {
                Produit pr = new Produit();

                pr.setId(rest.getInt("id"));
                pr.setNom(rest.getString("nom"));
                pr.setDescription(rest.getString("description"));
                pr.setPrix(rest.getInt("prix"));
                pr.setQuantity(rest.getInt("quantity"));
                pr.setCategory(rest.getInt("category"));
                pr.setOwner(rest.getInt("owner"));
                ALLproducts.add(pr);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Service.ServiceProduit.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ALLproducts;

    }

}
