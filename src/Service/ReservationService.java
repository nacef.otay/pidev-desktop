/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import static Core.Controller.getUserId;
import Entities.Maisons_hotes;
import Entities.Reservation_hotes;
import Entities.User;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import utils.DataSource;

/**
 *
 * @author Bhs Nada
 */
public class ReservationService {

    Connection c = DataSource.getInstance().getConnection();

    /**
     * ********************************* Ajouter Réservation   *************************************
     */
    public void ajouterReservation(Reservation_hotes r) {

        try {
            Statement st = c.createStatement();
            String req = "INSERT INTO reservation_hotes (date_debut,nb_personne,prix,maisons_hotes_id,nb_jours,user_id) VALUES ( ?,?,?,?,?,?)";

            PreparedStatement ps = c.prepareStatement(req);
        
            ps.setDate(1, (Date) r.getDate_debut());
          //  ps.setDate(2, (Date) r.getDate_fin());
            ps.setInt(2, r.getNb_personne());
            ps.setDouble(3, r.getPrix());
            ps.setInt(4, r.getMaisons_hotes_id().getId());
            ps.setInt(5, r.getNb_jours());
           ps.setInt(6,getUserId());
           
            ps.executeUpdate();
            System.out.println("Rservation ajouté avec succès");

        } catch (SQLException ex) {
            Logger.getLogger(ReservationService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    ///////////////////////////////////////////////////////////////////////////////

    public double PrixReservation(int id) {
        double prixReser = 0;
        double prix_hote;
        int nbP,nbJours;

        String req = "SELECT m.prix AS prixNuit, nb_jours AS nbJ,nb_personne AS nb FROM maisons_hotes m JOIN reservation_hotes r ON m.id=r.maisons_hotes_id ";

        Statement st;
        try {
            st = c.createStatement();
             
            ResultSet rs = st.executeQuery(req);
            
            while (rs.next()) {
                
                prix_hote = rs.getDouble("prixNuit");
                System.out.println("le prix d'une nuit est  " + prix_hote);
               // nbJ = rs.getInt("dateF") - rs.getInt("dateD");
               // System.out.println(" le nbr de jour est :   " + nbJ);
                nbP = rs.getInt("nb");
                System.out.println("nb personnes :   " + nbP);
                nbJours=rs.getInt("nbJ");
                prixReser = prix_hote * nbJours * nbP;
                System.out.println("*** *** Prix Reservation *** *** : " + prixReser);

            }
        } catch (SQLException ex) {
            Logger.getLogger(ReservationService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return prixReser;
    }

    /////////////////////////////////////////////////////////////////
    public Reservation_hotes AfficherReservation(int num) throws SQLException {
        Reservation_hotes r = new Reservation_hotes();
        Statement st = c.createStatement();
        String req = "SELECT * from reservation_hotes where numero_reservation=?";
        PreparedStatement ps = c.prepareStatement(req);
        ps.setInt(1, r.getNumero_reservation());
        ResultSet rs = st.executeQuery(req);
        while (rs.next()) {
            r.setNumero_reservation(rs.getInt("numero_reservation"));
            r.setDate_debut(rs.getDate("date_debut"));
            r.setDate_fin(rs.getDate("date_fin"));
            r.setMaisons_hotes_id((Maisons_hotes) rs.getObject("maisons_hotes_id"));
            r.setNb_personne(rs.getInt("nb_personne"));
            r.setPrix(rs.getDouble("pix"));
        }
        return r;

    }
    ////////////////////////////////////////////////////////////////

    /**
     * **************** Afficher Maison d'hôte  **************************
     */
    public ArrayList<Reservation_hotes> AfficherReservationHotes() {
        ArrayList<Reservation_hotes> myList = new ArrayList<Reservation_hotes>();
        try {

            String req = "SELECT * FROM reservation_hotes";
            Statement st = c.createStatement();
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {

                Reservation_hotes r = new Reservation_hotes();

                r.setNumero_reservation(rs.getInt("numero_reservation"));
                r.setDate_debut(rs.getDate("date_debut"));
                r.setDate_fin(rs.getDate("date_fin"));
                //r.setMaisons_hotes_id((Maisons_hotes) rs.getObject("maisons_hotes_id"));
                
                r.setNb_personne(rs.getInt("nb_personne"));
                r.setPrix(rs.getDouble("prix"));

                myList.add(r);

            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return myList;

    }
    /////////////////////////////////////////////////////////////////////////////////
/**
     * **************** Afficher Reservation Maison d'hôte  user cncte **************************
     */
    public ArrayList<Reservation_hotes> AfficherReservation() {
        ArrayList<Reservation_hotes> myList = new ArrayList<Reservation_hotes>();
         String req = "SELECT r.*,m.nom FROM reservation_hotes r JOIN maisons_hotes m on r.maisons_hotes_id=m.id where r.user_id=?";
         Statement st;
        try {
               // numero_reservation,date_debut,nb_personne,prix,user_id,nb_jours 
           st=c.createStatement();
            PreparedStatement  ps = c.prepareStatement(req);
           ps.setInt(1,getUserId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Reservation_hotes r = new Reservation_hotes();
               
                
                r.setNumero_reservation(rs.getInt("r.numero_reservation"));
                r.setDate_debut(rs.getDate("r.date_debut"));
                r.setNb_jours(rs.getInt("r.nb_jours"));
              // r.setMaisons_hotes_id((Maisons_hotes) rs.getObject("m.nom"));

             // r.setMaisons_hotes_id((rs.getInt("m.nom")));
                r.setNb_personne(rs.getInt("r.nb_personne"));
                r.setPrix(rs.getDouble("r.prix"));
                
                myList.add(r);

            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return myList;

    }
    /////////////////////////////////////////////////////////////////////////////////
}
