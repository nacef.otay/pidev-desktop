/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import Entities.region;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import utils.DataSource;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ZerOo
 */
public class ServiceRegion {

    Connection c = DataSource.getInstance().getConnection();

    public List<region> ListRegion() {
        List ALLproducts = new ArrayList();
        try {
            String query = "select * from region";
            Statement st = DataSource.getInstance().getConnection().createStatement();
            ResultSet rest = st.executeQuery(query);
            while (rest.next()) {
                region rg = new region();

                rg.setId(rest.getInt("id"));
                rg.setRegion(rest.getString("region"));

                ALLproducts.add(rg);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ServiceProduit.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ALLproducts;
    }

    public region getRegionByName(String rname) {

        region rg = new region();
        try {
            String query = "select * from region where region = ?";
            PreparedStatement ps;

            ps = c.prepareStatement(query);
            ps.setString(1, rname);
            ResultSet rest = ps.executeQuery();

            while (rest.next()) {
                rg.setId(rest.getInt("id"));
                rg.setRegion(rest.getString("region"));
            }

        } catch (SQLException ex) {
            Logger.getLogger(ServiceProduit.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rg;

    }

}
