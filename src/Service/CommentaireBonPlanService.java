/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import static Core.Controller.getUserId;
import Entities.Bonplan;
import Entities.CommentaireBonPlan;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import utils.DataSource;

/**
 *
 * @author marie
 */
public class CommentaireBonPlanService {
         Connection c= DataSource.getInstance().getConnection();
         
   public void ajouterCommentaire(CommentaireBonPlan comment) {
         
        try {
            Statement st= c.createStatement();
            String req = "INSERT INTO bon_plan_commentaire(user_id,bon_plan_id,contenu) "
                    + "VALUES ( ?,?,?)";
            
            PreparedStatement ps = c.prepareStatement(req);
            
             ps.setInt(1, getUserId());
            // System.out.println("id bon plan ******"+comment.getBonplan().getId());
             ps.setInt(2,comment.getBonplan().getId());
             ps.setString(3,comment.getContenu());
             //System.out.println("**** contenu ******"+comment.getContenu());
             //ps.setDate(4,new java.sql.Date(0));
             ps.executeUpdate();
             //System.out.println("commentaire succès"); 
                      
        } catch (SQLException ex) {
            Logger.getLogger(CommentaireBonPlanService.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }
       /////////////////////////////////////////////////////////////////////////////////
    
    public List<CommentaireBonPlan> listbpcomment(int idBp)
   { 
       ObservableList<CommentaireBonPlan> Commentaires=FXCollections.observableArrayList();
    
     String req = "SELECT contenu,datePublication FROM bon_plan_commentaire"
             + " where bon_plan_id=?";
            Statement st;
            try {
                st = c.createStatement();
                PreparedStatement ps = c.prepareStatement(req);

               ps.setInt(1,idBp);
          
                ResultSet rs =ps.executeQuery();
                 while (rs.next()) {


                CommentaireBonPlan comment = new CommentaireBonPlan();
                
                comment.setContenu(rs.getString("contenu"));
          //comment.setId(rs.getInt("id"));
                comment.setDateCommentaire(rs.getDate("datePublication"));
                BonPlanService bs = new BonPlanService();
                //
                //comment.setBonplan(bs.getBonPlanById(idBp));
                
                 Commentaires.add(comment);
                 
  }
            } catch (SQLException ex) {
                Logger.getLogger(BonPlanService.class.getName()).log(Level.SEVERE, null, ex);
            }
            
    return Commentaires;

    }
    
  
}
