/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import static Core.Controller.getUserId;
import Entities.panier;
import Entities.panier;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DataSource;

/**
 *
 * @author ZerOo
 */
public class ServicePanier {

    public ServicePanier() {
    }

    Connection c = DataSource.getInstance().getConnection();

    public void ajouterPanier(panier p1) {
        String req = "insert into panier (id,user_id,produit_id,quantite,date_p,prix) values (?,?,?,?,?,?)";
        java.util.Date d1 = new java.util.Date();
        Date d = new Date(d1.getTime());
        try {
            PreparedStatement ps = c.prepareStatement(req);
            ps.setInt(1, p1.getId());
            ps.setInt(2, p1.getUserid());
            ps.setInt(3, p1.getProduitid());
            ps.setFloat(4, p1.getQuantite());
            ps.setDate(5, d);
            ps.setInt(6, p1.getPrix());

            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ServiceCommande.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void modifierpanier(panier p1) {
        try {
            //requete pre compiler
            PreparedStatement pt = c.prepareStatement("update panier set quantite = ? ,prix = ? where user_id = ? and produit_id = ?");
            pt.setInt(1, p1.getQuantite());
            //pt.setString(2,p1.getDescription());
            pt.setInt(2, p1.getPrix());
            pt.setInt(3, p1.getUserid());
            pt.setInt(4, p1.getProduitid());
            pt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ServiceProduit.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void supprimerPanier(int id) {

        try {
            PreparedStatement pt = c.prepareStatement("delete from panier where id=?");
            pt.setInt(1, id);
            pt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(ServiceCommande.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public List<panier> ListPanier() {
        List ALLproducts = new ArrayList();
        try {
            String query = "select * from panier";
            Statement st = DataSource.getInstance().getConnection().createStatement();
            ResultSet rest = st.executeQuery(query);
            while (rest.next()) {
                panier pr = new panier();

                pr.setId(rest.getInt("id"));
                pr.setUserid(rest.getInt("user_id"));
                pr.setProduitid(rest.getInt("produit_id"));
                pr.setQuantite(rest.getInt("quantite"));
                pr.setDate(rest.getDate("date_p"));
                pr.setPrix(rest.getInt("prix"));

                ALLproducts.add(pr);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ServiceProduit.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ALLproducts;

    }

    public List<panier> ListPanierUser(int userid) {
        List ALLproducts = new ArrayList();
        try {
            PreparedStatement st = c.prepareStatement("select * from panier where user_id = ?");
            st.setInt(1, userid);
            ResultSet rest = st.executeQuery();
            while (rest.next()) {
                panier pr = new panier();

                pr.setId(rest.getInt("id"));
                pr.setUserid(rest.getInt("user_id"));
                pr.setProduitid(rest.getInt("produit_id"));
                pr.setQuantite(rest.getInt("quantite"));
                pr.setDate(rest.getDate("date_p"));
                pr.setPrix(rest.getInt("prix"));

                ALLproducts.add(pr);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ServiceProduit.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ALLproducts;

    }

}
