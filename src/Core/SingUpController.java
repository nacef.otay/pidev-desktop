/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Core;

import com.jfoenix.controls.JFXCheckBox;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author OTAY Nacef
 */
public class SingUpController implements Initializable {

    @FXML
    private ImageView back;
    @FXML
    private JFXCheckBox terms;
    @FXML
    private DatePicker date_naissance;
    @FXML
    private TextField username;
    @FXML
    private TextField nom;
    @FXML
    private TextField prenom;
    @FXML
    private TextField email;
    @FXML
    private PasswordField password;
    @FXML
    private PasswordField repeat_password;
    @FXML
    private Button reset;
    @FXML
    private Button ajouter;
    @FXML
    private Label exit;
    @FXML
    private Label info;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void back(MouseEvent event) {
    }

    @FXML
    private void ResetAll(ActionEvent event) {
    }

    @FXML
    private void ajouter_utilisateur(ActionEvent event) {
    }

    @FXML
    private void exit(MouseEvent event) {
    }
    
}
