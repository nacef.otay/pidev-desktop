/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Core;

import IService.*;
import Service.*;

/**
 *
 * @author Nacef
 */
public class Service {
    private static Service service ;
    private IUserService userService;
    private IAlbumService albumService;
    private ICentreInteretService centreInteretService;
    private IPublicationService publicationService;
    private ISignalerService signalerService;
    private IRelationService relationService;
    private IDemandeService demandeService;
    private IBloquerService bloquerService;

    
    private Service()
    {
        
    }
    public static Service getInstance()
    {
        if(service == null)
            service = new Service();
        return service;
    }
       public IDemandeService getDemandeService()
    {
        if(demandeService == null)
            demandeService = new DemandeService();
        return demandeService;
    }
    public IRelationService getRelationService()
    {
        if(relationService == null)
            relationService = new RelationService();
        return relationService;
    }
    public IUserService getUserService()
    {
        if(userService == null)
            userService = new UserService();
        return userService;
    }
    
    public IAlbumService getAlbumService()
    {
        if(albumService == null)
            albumService = new AlbumService();
        return albumService;
    }
       
  public ICentreInteretService getCentreInteretService()
    {
        if(centreInteretService == null)
            centreInteretService = new CentreInteretService();
        return centreInteretService;
    }
      public IPublicationService getPublicationService()
    {
        if(publicationService == null)
            publicationService = new PublicationService();
        return publicationService;
    }
        public ISignalerService getSignalerService()
    {
        if(signalerService == null)
            signalerService = new SignalerService();
        return signalerService;
    }
        
            public IBloquerService getBloquerService()
    {
        if(bloquerService == null)
            bloquerService = new BloquerService();
        return bloquerService;
    }
}
