/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IService;

import Entities.User;

/**
 *
 * @author Nacef Otay
 */
public interface IBloquerService {
    
    public void bloquerUser(User u);
    
    public void debloquerUser(User u);
}
