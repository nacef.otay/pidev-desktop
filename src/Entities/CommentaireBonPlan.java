/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author marie
 */
public class CommentaireBonPlan {
    private int id ;
    private String contenu ;
    private User user ;
    private Bonplan bonplan;
    private Date dateCommentaire;

    public CommentaireBonPlan() {
    }
public CommentaireBonPlan(String contenu) {
    this.contenu = contenu;
    }

    public CommentaireBonPlan(Bonplan bonplan) {
        this.bonplan = bonplan;
    }

    public CommentaireBonPlan(int id, String contenu, User user, Bonplan bonplan, Date dateCommentaire) {
        this.id = id;
        this.contenu = contenu;
        this.user = user;
        this.bonplan = bonplan;
        this.dateCommentaire = dateCommentaire;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Bonplan getBonplan() {
        return bonplan;
    }

    public void setBonplan(Bonplan bonplan) {
        this.bonplan = bonplan;
    }

    public Date getDateCommentaire() {
        return dateCommentaire;
    }

    public void setDateCommentaire(Date dateCommentaire) {
        this.dateCommentaire = dateCommentaire;
    }

    @Override
    public String toString() {
        return "Commentaire{" + "id=" + id + ", contenu=" + contenu + ", user=" + user + ", bonplan=" + bonplan + ", dateCommentaire=" + dateCommentaire + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.id;
        hash = 97 * hash + Objects.hashCode(this.contenu);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CommentaireBonPlan other = (CommentaireBonPlan) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.contenu, other.contenu)) {
            return false;
        }
        if (!Objects.equals(this.user, other.user)) {
            return false;
        }
        if (!Objects.equals(this.bonplan, other.bonplan)) {
            return false;
        }
        return true;
    }
    
    
    
    
}
