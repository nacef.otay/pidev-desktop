/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import java.sql.Date;

/**
 *
 * @author soumaya
 */
public class Evenement extends RecursiveTreeObject<Evenement>{
    private static int event_courant;

    
  private Integer id;
    private String imageEve;
    public Integer nbplaces;
    private Date dateEvenement;
    private String titre;
    private String description;
    private String titreCordination;
    private Date datefin ; 
    private double prix ;
    private String adr ;

    public Evenement()  {
    }

    public Evenement(String imageEve, Integer nbplaces, Date dateEvenement, String titre, String description, String titreCordination, Date datefin, double prix, String adr) {
        this.imageEve = imageEve;
        this.nbplaces = nbplaces;
        this.dateEvenement = dateEvenement;
        this.titre = titre;
        this.description = description;
        this.titreCordination = titreCordination;
        this.datefin = datefin;
        this.prix = prix;
        this.adr = adr;
    }
    

    public Evenement(Date dateEvenement, String titre, String description, String titreCordination) {
        this.dateEvenement = dateEvenement;
        this.titre = titre;
        this.description = description;
        this.titreCordination = titreCordination;
    }

    public Evenement(Integer id, String imageEve, Integer nbplaces, Date dateEvenement, String titre, String description, String titreCordination) {
        this.id = id;
        this.imageEve = imageEve;
        this.nbplaces = nbplaces;
        this.dateEvenement = dateEvenement;
        this.titre = titre;
        this.description = description;
        this.titreCordination = titreCordination;
    }

    public Evenement(String imageEve, Integer nbplaces, Date dateEvenement, String titre, String description, String titreCordination) {
        this.imageEve = imageEve;
        this.nbplaces = nbplaces;
        this.dateEvenement = dateEvenement;
        this.titre = titre;
        this.description = description;
        this.titreCordination = titreCordination;
    }

    public Evenement(String imageEve, Date dateEvenement, String titre) {
        this.imageEve = imageEve;
        this.dateEvenement = dateEvenement;
        this.titre = titre;
    }

    

    public Evenement(Integer id, Integer nbplaces, Date dateEvenement, String titre, String description, String titreCordination) {
        this.id = id;
        this.nbplaces = nbplaces;
        this.dateEvenement = dateEvenement;
        this.titre = titre;
        this.description = description;
        this.titreCordination = titreCordination;
    }

    public Evenement(Integer nbplaces, Date dateEvenement, String titre, String description, String titreCordination) {
        this.nbplaces = nbplaces;
        this.dateEvenement = dateEvenement;
        this.titre = titre;
        this.description = description;
        this.titreCordination = titreCordination;
    }

    public Evenement(Integer id, String imageEve, Integer nbplaces, Date dateEvenement, String titre, String description, String titreCordination, Date datefin, double prix) {
        this.id = id;
        this.imageEve = imageEve;
        this.nbplaces = nbplaces;
        this.dateEvenement = dateEvenement;
        this.titre = titre;
        this.description = description;
        this.titreCordination = titreCordination;
        this.datefin = datefin;
        this.prix = prix;
    }

    public Evenement(Integer id, String imageEve, Integer nbplaces, Date dateEvenement, String titre, String description, String titreCordination, Date datefin, double prix, String adr) {
        this.id = id;
        this.imageEve = imageEve;
        this.nbplaces = nbplaces;
        this.dateEvenement = dateEvenement;
        this.titre = titre;
        this.description = description;
        this.titreCordination = titreCordination;
        this.datefin = datefin;
        this.prix = prix;
        this.adr = adr;
    }

    public Evenement(Integer id, Integer nbplaces, Date dateEvenement, String titre, String description, String titreCordination, Date datefin, double prix, String adr) {
        this.id = id;
        this.nbplaces = nbplaces;
        this.dateEvenement = dateEvenement;
        this.titre = titre;
        this.description = description;
        this.titreCordination = titreCordination;
        this.datefin = datefin;
        this.prix = prix;
        this.adr = adr;
    }
    
    
    

    public Evenement(Integer id) {
        this.id = id;
    }

   
    public Evenement(Date dtdebut, Date dtfin, String text, String text0, String text1, String text2, Integer parseInt, int parseInt0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Evenement(String name, Integer valueOf, Date dtdebut, String text, String text0, String text1, Date dtfin, String text2, Double valueOf0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   
public static int getEvent_courant() {
        return event_courant;
    }

    public static void setEvent_courant(int event_courant) {
        Evenement.event_courant = event_courant;
    }
    
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDatefin() {
        return datefin;
    }

    public void setDatefin(Date datefin) {
        this.datefin = datefin;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    
    public String getImageEve() {
        return imageEve;
    }

    public void setImageEve(String imageEve) {
        this.imageEve = imageEve;
    }


    public Integer getNbplaces() {
        return nbplaces;
    }

    public void setNbplaces(Integer nbplaces) {
        this.nbplaces = nbplaces;
    }

    public Date getDateEvenement() {
        return dateEvenement;
    }

    public void setDateEvenement(Date dateEvenement) {
        this.dateEvenement = dateEvenement;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitreCordination() {
        return titreCordination;
    }

    public void setTitreCordination(String titreCordination) {
        this.titreCordination = titreCordination;
    }

    public String getAdr() {
        return adr;
    }

    public void setAdr(String adr) {
        this.adr = adr;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Evenement)) {
            return false;
        }
        Evenement other = (Evenement) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Evenement{" + "id=" + id + ", imageEve=" + imageEve + ", nbplaces=" + nbplaces + ", dateEvenement=" + dateEvenement + ", titre=" + titre + ", description=" + description + ", titreCordination=" + titreCordination + ", datefin=" + datefin + ", prix=" + prix + ", adr=" + adr + '}';
    }

    

   
    
    
}
