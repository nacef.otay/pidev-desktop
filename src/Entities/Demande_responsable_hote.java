/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.Date;

/**
 *
 * @author Bhs Nada
 */
public class Demande_responsable_hote {
     
    private int id;
        private User id_user;
        private String description;;
        private  Date date;

    public Demande_responsable_hote() {
    }

    public Demande_responsable_hote(int id, User id_user, String description, Date date) {
        this.id = id;
        this.id_user = id_user;
        this.description = description;
        this.date = date;
    }

    public Demande_responsable_hote(String description) {
        this.description = description;
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getId_user() {
        return id_user;
    }

    public void setId_user(User id_user) {
        this.id_user = id_user;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Demande_responsable_hote{" + "id=" + id + ", id_user=" + id_user + ", description=" + description + ", date=" + date + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Demande_responsable_hote other = (Demande_responsable_hote) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
    
        
        
}
