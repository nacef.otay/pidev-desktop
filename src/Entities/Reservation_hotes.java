/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.Date;

/**
 *
 * @author Bhs Nada
 */
public class Reservation_hotes {

    private int numero_reservation;
    private Date date_debut;
    private Date date_fin;
    private int nb_personne;
    private double prix;
    private User user_id;
    private Maisons_hotes maisons_hotes_id;
    private int nb_jours;

    public Reservation_hotes() {
    }

    public Reservation_hotes(int numero_reservation, Date date_debut, Date date_fin, int nb_personne, double prix, User user_id, Maisons_hotes maisons_hotes_id) {
        this.numero_reservation = numero_reservation;
        this.date_debut = date_debut;
        this.date_fin = date_fin;
        this.nb_personne = nb_personne;
        this.prix = prix;
        this.user_id = user_id;
        this.maisons_hotes_id = maisons_hotes_id;
    }

    
    
    public int getNumero_reservation() {
        return numero_reservation;
    }

    public Reservation_hotes(Date date_debut, Date date_fin, int nb_personne, Maisons_hotes maisons_hotes_id) {
        this.date_debut = date_debut;
        this.date_fin = date_fin;
        this.nb_personne = nb_personne;
        this.maisons_hotes_id = maisons_hotes_id;
    }

    public Reservation_hotes(Date date_debut, int nb_personne, Maisons_hotes maisons_hotes_id, int nb_jours) {
        this.date_debut = date_debut;
        this.nb_personne = nb_personne;
        this.maisons_hotes_id = maisons_hotes_id;
        this.nb_jours = nb_jours;
    }

    public int getNb_jours() {
        return nb_jours;
    }

    public void setNb_jours(int nb_jours) {
        this.nb_jours = nb_jours;
    }

    public Reservation_hotes(Date date_debut, int nb_personne, double prix, User user_id, Maisons_hotes maisons_hotes_id, int nb_jours) {
        this.date_debut = date_debut;
        this.nb_personne = nb_personne;
        this.prix = prix;
        this.user_id = user_id;
        this.maisons_hotes_id = maisons_hotes_id;
        this.nb_jours = nb_jours;
    }
    

    public void setNumero_reservation(int numero_reservation) {
        this.numero_reservation = numero_reservation;
    }

    public Date getDate_debut() {
        return date_debut;
    }

    public void setDate_debut(Date date_debut) {
        this.date_debut = date_debut;
    }

    public Date getDate_fin() {
        return date_fin;
    }

    public void setDate_fin(Date date_fin) {
        this.date_fin = date_fin;
    }

    public int getNb_personne() {
        return nb_personne;
    }

    public void setNb_personne(int nb_personne) {
        this.nb_personne = nb_personne;
    }

    public double getPrix() {
          return prix;
    }
    

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public User getUser_id() {
        return user_id;
    }

    public void setUser_id(User user_id) {
        this.user_id = user_id;
    }

    public Maisons_hotes getMaisons_hotes_id() {
        return maisons_hotes_id;
    }

    public void setMaisons_hotes_id(Maisons_hotes maisons_hotes_id) {
        this.maisons_hotes_id = maisons_hotes_id;
    }

    @Override
    public String toString() {
        return "Reservation_hotes{" + "numero_reservation=" + numero_reservation + ", date_debut=" + date_debut + ", date_fin=" + date_fin + ", nb_personne=" + nb_personne + ", prix=" + prix + ", user_id=" + user_id + ", maisons_hotes_id=" + maisons_hotes_id + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + this.numero_reservation;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Reservation_hotes other = (Reservation_hotes) obj;
        if (this.numero_reservation != other.numero_reservation) {
            return false;
        }
        return true;
    }

  /*  public double PrixReservation()
    {
        int nb_jour = date_fin.getDay() - date_debut.getDay();
        return maisons_hotes_id.getPrix() * nb_jour * nb_personne;
    }

  */

    public Reservation_hotes(Date date_debut, int nb_personne, double prix, Maisons_hotes maisons_hotes_id, int nb_jours) {
        this.date_debut = date_debut;
        this.nb_personne = nb_personne;
        this.prix = prix;
        this.maisons_hotes_id = maisons_hotes_id;
        this.nb_jours = nb_jours;
    }
    
    
}
