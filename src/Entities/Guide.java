/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import java.util.Objects;

/**
 *
 * @author soumaya
 */
public class Guide extends RecursiveTreeObject<Guide>{
     private Integer id;
     private String nomEvent;
     private String eventType ;
     private String nom ;
     private String prenom ;
     private String mail ;
     private Integer tel ;
 public Guide(Integer id,String nomEvent, String eventType, String nom, String prenom, String mail, Integer tel) {
        this.id=id;
     this.nomEvent = nomEvent;
        this.eventType = eventType;
        this.nom = nom;
        this.prenom = prenom;
        this.mail = mail;
        this.tel = tel;
    }
    public Guide(String nomEvent, String eventType, String nom, String prenom, String mail, Integer tel) {
        this.nomEvent = nomEvent;
        this.eventType = eventType;
        this.nom = nom;
        this.prenom = prenom;
        this.mail = mail;
        this.tel = tel;
    }
     

    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.id);
        hash = 89 * hash + Objects.hashCode(this.nomEvent);
        hash = 89 * hash + Objects.hashCode(this.eventType);
        hash = 89 * hash + Objects.hashCode(this.nom);
        hash = 89 * hash + Objects.hashCode(this.prenom);
        hash = 89 * hash + Objects.hashCode(this.mail);
        hash = 89 * hash + this.tel;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Guide other = (Guide) obj;
        if (this.tel != other.tel) {
            return false;
        }
        if (!Objects.equals(this.nomEvent, other.nomEvent)) {
            return false;
        }
        if (!Objects.equals(this.eventType, other.eventType)) {
            return false;
        }
        if (!Objects.equals(this.nom, other.nom)) {
            return false;
        }
        if (!Objects.equals(this.prenom, other.prenom)) {
            return false;
        }
        if (!Objects.equals(this.mail, other.mail)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

     
     
     
   

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomEvent() {
        return nomEvent;
    }

    public void setNomEvent(String nomEvent) {
        this.nomEvent = nomEvent;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Integer getTel() {
        return tel;
    }

    public void setTel(Integer tel) {
        this.tel = tel;
    }

   

    @Override
    public String toString() {
        return "guide{" + "id=" + id + ", nomEvent=" + nomEvent + ", eventType=" + eventType + ", nom=" + nom + ", prenom=" + prenom + ", mail=" + mail + ", tel=" + tel + '}';
    }

    public String get() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
     
     
     
     
    
}
