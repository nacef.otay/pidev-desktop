/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Bhs Nada
 */
public class Maisons_hotes {
     private  int id;
    private String nom;
    private String description;
    private String pays;
    private String gouvernorat;
    private String image;
    private String adresse;
    private String site_web;
    private String mail;
    private int capacites;
    private int tel;
    private double prix;
    private User id_user;
    private int repliesnumber;
    private  ImageView img;

    public ImageView getImg() {
        return img;
    }

    public void setImg(ImageView img) {
        this.img = img;
    }

    public Maisons_hotes(String nom, ImageView img) {
        this.nom = nom;
        this.img = img;
    }
    
    
    

    public Maisons_hotes() {
    }
    

    public Maisons_hotes(int id, String nom, String description, String pays, String gouvernorat, String image, String adresse, String site_web, String mail, int capacites, int tel, double prix, User id_user, int repliesnumber) {
        this.id = id;
        this.nom = nom;
        this.description = description;
        this.pays = pays;
        this.gouvernorat = gouvernorat;
        this.image = image;
        this.adresse = adresse;
        this.site_web = site_web;
        this.mail = mail;
        this.capacites = capacites;
        this.tel = tel;
        this.prix = prix;
        this.id_user = id_user;
        this.repliesnumber = repliesnumber;
    }

    public Maisons_hotes(String nom, String description, String pays, String gouvernorat, String image, String adresse, String site_web, String mail, int capacites, int tel, double prix, User id_user, int repliesnumber) {
        this.nom = nom;
        this.description = description;
        this.pays = pays;
        this.gouvernorat = gouvernorat;
        this.image = image;
        this.adresse = adresse;
        this.site_web = site_web;
        this.mail = mail;
        this.capacites = capacites;
        this.tel = tel;
        this.prix = prix;
        this.id_user = id_user;
        this.repliesnumber = repliesnumber;
    }

    public Maisons_hotes(String nom, String description, String pays, String gouvernorat, String image, String adresse, String site_web, String mail, int capacites, int tel, double prix) {
        this.nom = nom;
        this.description = description;
        this.pays = pays;
        this.gouvernorat = gouvernorat;
        this.image = image;
        this.adresse = adresse;
        this.site_web = site_web;
        this.mail = mail;
        this.capacites = capacites;
        this.tel = tel;
        this.prix = prix;
     
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getGouvernorat() {
        return gouvernorat;
    }

    public void setGouvernorat(String gouvernorat) {
        this.gouvernorat = gouvernorat;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getSite_web() {
        return site_web;
    }

    public void setSite_web(String site_web) {
        this.site_web = site_web;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public int getCapacites() {
        return capacites;
    }

    public void setCapacites(int capacites) {
        this.capacites = capacites;
    }

    public int getTel() {
        return tel;
    }

    public void setTel(int tel) {
        this.tel = tel;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public User getId_user() {
        return id_user;
    }

    public void setId_user(User id_user) {
        this.id_user = id_user;
    }

    public int getRepliesnumber() {
        return repliesnumber;
    }

    public void setRepliesnumber(int repliesnumber) {
        this.repliesnumber = repliesnumber;
    }

    @Override
    public String toString() {
        return "Maisons_hotes{" + "id=" + id + ", nom=" + nom + ", description=" + description + ", pays=" + pays + ", gouvernorat=" + gouvernorat + ", image=" + image + ", adresse=" + adresse + ", site_web=" + site_web + ", mail=" + mail + ", capacites=" + capacites + ", tel=" + tel + ", prix=" + prix + ", id_user=" + id_user + ", repliesnumber=" + repliesnumber + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Maisons_hotes other = (Maisons_hotes) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    public Maisons_hotes(String nom, String image) {
        this.nom = nom;
        this.image = image;
    }

    public Maisons_hotes(String nom) {
        this.nom = nom;
      
    }


    
    
    
    
    
}
