/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author marie
 */
public class Bonplan {
    private int id ;
    private String name ;
    private String adresse ;
    private String phone ;
    private int note ;
    private String description ;
    private String image ;
    private int etoile ;
    private double prix ;
    private Date datePublication ;
    private String categorie ;
    private User user ;

    public Bonplan() {
    }

    public Bonplan(int id, String name, String adresse, String phone, int note, 
            String description, String image, int etoile, double prix,  String categorie, User user) {
        this.id = id;
        this.name = name;
        this.adresse = adresse;
        this.phone = phone;
        this.note = note;
        this.description = description;
        this.image = image;
        this.etoile = etoile;
        this.prix = prix;
        //this.datePublication =  new Date();
        this.categorie = categorie;
        this.user = user;
    }
    public Bonplan(String name, String adresse, String phone, int note,
            String description, int etoile, double prix,String image) {
        this.name = name;
        this.adresse = adresse;
        this.phone = phone;
        this.note = note;
        this.description = description;
        this.etoile = etoile;
        this.prix = prix;
        this.image=image;
       
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getEtoile() {
        return etoile;
    }

    public void setEtoile(int etoile) {
        this.etoile = etoile;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public Date getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(Date datePublication) {
        this.datePublication = datePublication;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Bonplan{" + "id=" + id + ", name=" + name + ", adresse=" + adresse + ", phone=" + phone + ", note=" + note + ", description=" + description + ", image=" + image + ", etoile=" + etoile + ", prix=" + prix + ", datePublication=" + datePublication + ", categorie=" + categorie + ", user=" + user + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + this.id;
        hash = 47 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Bonplan other = (Bonplan) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.phone != other.phone) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.categorie, other.categorie)) {
            return false;
        }
        if (!Objects.equals(this.user, other.user)) {
            return false;
        }
        return true;
    }
    
    
    
    
}
