/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author ZerOo
 */
public class panier {
    
     private int id;
      private int userid;
      private int produitid;
       private int quantite;
      private Date date;
      private int prix;

    public panier() {
    }

    public panier(int id, int userid, int produitid, int quantite, Date date, int prix) {
        this.id = id;
        this.userid = userid;
        this.produitid = produitid;
        this.quantite = quantite;
        this.date = date;
        this.prix = prix;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getProduitid() {
        return produitid;
    }

    public void setProduitid(int produitid) {
        this.produitid = produitid;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + this.id;
        hash = 43 * hash + this.userid;
        hash = 43 * hash + this.produitid;
        hash = 43 * hash + this.quantite;
        hash = 43 * hash + Objects.hashCode(this.date);
        hash = 43 * hash + this.prix;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final panier other = (panier) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.userid != other.userid) {
            return false;
        }
        if (this.produitid != other.produitid) {
            return false;
        }
        if (this.quantite != other.quantite) {
            return false;
        }
        if (this.prix != other.prix) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "panier{" + "id=" + id + ", userid=" + userid + ", produitid=" + produitid + ", quantite=" + quantite + ", date=" + date + ", prix=" + prix + '}';
    }
      
      
    
}
