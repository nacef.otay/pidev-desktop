/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.Date;
import java.util.Objects;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


/**
 *
 * @author hp
 */
public class Produit {
    private static int produit_courant;

    public static int getProduit_courant() {
        return produit_courant;
    }

    public static void setProduit_courant(int produit_courant) {
        Produit.produit_courant = produit_courant;
    }
    
      private int id;
      private int stock = 1;
      private String nom;
      private String description;
      private Integer prix;
      private Integer quantity;
      private Date date;
      private int owner;
      private String image_id;
      private int category;
      private int region;
       private int idf;
       public static ObservableList<Produit> Panier = FXCollections.observableArrayList();  

    public int getIdf() {
        return idf;
    }

    public void setIdf(int idf) {
        this.idf = idf;
    }
    
         
    public static ObservableList<Produit> getPanier() {
        return Panier;
    }

   
     public static boolean setPanier(Produit P) {
        Boolean Test=true;
         
                                           
    	for (int i = 0; i < Panier.size(); i++) {
        if(Panier.get(i).getId()==P.getId())
        {
                        Test=false;
                        Panier.get(i).setQuantity(Panier.get(i).getQuantity()+1);
                        P.setQuantity(P.getQuantity()+1);
                      //  System.out.println( Panier.get(i));
                     
        }
		}
       if(Test==true)
            {
             P.setQuantity(P.getQuantity()+1);
           
             Panier.add(P);
            
            }
     
     return Test;
    }


    public Produit() {
    }

    public Produit(int id, String nom, String description,Integer prix, Integer quantity, Date date, int owner, String image_id, int category, int region) {
        this.id = id;
        this.nom = nom;
        this.description = description;
        this.prix = prix;
        this.quantity = quantity;
        this.date = date;
        this.owner = owner;
        this.image_id = image_id;
        this.category = category;
        this.region = region;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    
    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + this.id;
        hash = 83 * hash + Objects.hashCode(this.nom);
        hash = 83 * hash + Objects.hashCode(this.description);
        hash = 83 * hash + this.prix;
        hash = 83 * hash + this.quantity;
        hash = 83 * hash + Objects.hashCode(this.date);
        hash = 83 * hash + this.owner;
        hash = 83 * hash + Objects.hashCode(this.image_id);
        hash = 83 * hash + this.category;
        hash = 83 * hash + this.region;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Produit other = (Produit) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.prix != other.prix) {
            return false;
        }
        if (this.quantity != other.quantity) {
            return false;
        }
        if (this.owner != other.owner) {
            return false;
        }
        if (this.category != other.category) {
            return false;
        }
        if (this.region != other.region) {
            return false;
        }
        if (!Objects.equals(this.nom, other.nom)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.image_id, other.image_id)) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        return true;
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPrix() {
        return prix;
    }

    public void setPrix(Integer prix) {
        this.prix = prix;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getOwner() {
        return owner;
    }

    public void setOwner(int owner) {
        this.owner = owner;
    }

    public String getImage_id() {
        return image_id;
    }

    public void setImage_id(String image_id) {
        this.image_id = image_id;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public int getRegion() {
        return region;
    }

    public void setRegion(int region) {
        this.region = region;
    }

    @Override
    public String toString() {
        return "Produit{" + "id=" + id + ", nom=" + nom + ", description=" + description + ", prix=" + prix + ", quantity=" + quantity + ", date=" + date + ", owner=" + owner + ", image_id=" + image_id + ", category=" + category + ", region=" + region + '}';
    }
      
        
      
    

  
    
}
