/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BarcodeQRCode;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Date;
import com.itextpdf.text.Rectangle;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.sql.Statement;

/**
 *
 * @author Bhs Nada
 */
public class ReservationPdf {
    // req qr code 
    Connection c= DataSource.getInstance().getConnection();
     public String num(int numRes ) throws SQLException{
      String query = "SELECT m.nom from maisons_hotes m JOIN reservation_hotes r on m.id=r.maisons_hotes_id where numero_reservation="+numRes;
            Statement stmt;
             stmt = c.createStatement();
              ResultSet rs = stmt.executeQuery(query);
              
               String num=null;
              if(rs.next()){
   num  =rs.getString("nom");
     System.out.println(num);
}
              
        
        
     return num;
     }
     ///////
     /// PDF 
     public void Reservation(int numRes,Date dateDeb,int  nb_jours, int nb_pers,Double totaleprix ) throws SQLException {
        Document document = new Document(PageSize.A4);
        try{
            PdfWriter.getInstance(document, new FileOutputStream("Reservation"+numRes+".pdf"));
            document.open();
            
            /*Rectangle pageSize = new Rectangle(216, 720);
           pageSize.setBackgroundColor(new BaseColor(0xFF, 0xFF, 0xDE));
            */
            Paragraph p1 = new Paragraph("Reservation N°  :   "+numRes);
            Paragraph p2 = new Paragraph("----------------------------");
            Paragraph p3 = new Paragraph("                                    ");
            
            Paragraph p4 = new Paragraph("Date de début   :    "+ dateDeb);
           Paragraph p5 = new Paragraph ("Nombre de jours   :    "+ nb_jours + " jour(s)");
            Paragraph p6 = new Paragraph("Nombre de personnes   :   "+ nb_pers + " personne(s) ");
            Paragraph Prix = new Paragraph("Prix Totale   :    "+ totaleprix + " $");
           
            Paragraph p7 =  new Paragraph("Scanner le QR_Code pour connaitre le nom de la maison d'hôte :");
            //QR_Code
         BarcodeQRCode my_code = new BarcodeQRCode(num(numRes),numRes,numRes,null);
           Paragraph p8 = new Paragraph("------------------------------------------------------------------");
        Image qr_image = my_code.getImage();
            
            document.add(p1);
            document.add(p2);
            document.add(p3);
            
            document.add(p7);
             document.add(qr_image);
             document.add(p8);
             
            document.add(p4);
              document.add(p3);
            document.add(p5);
              document.add(p3);
            document.add(p6);
              document.add(p3);
            document.add(Prix);
              document.add(p3);
           
        }
        catch(DocumentException | FileNotFoundException e){
            System.out.println(e);
        }
        document.close();
    }
}
