/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Core.Controller;
import static Core.Controller.holderPane;
import Core.LayoutFrontController;
import Entities.Produit;
import Service.ServiceProduit;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import utils.DataSource;

/**
 * FXML Controller class
 *
 * @author DJAZIA
 */
public class StatistiqueProduitController extends Controller implements Initializable {

    @FXML
    private BarChart<?, ?> participationChart;
    @FXML
    private NumberAxis y;
    @FXML
    private CategoryAxis x;
    
    private final ServiceProduit ess = new ServiceProduit();
    private Connection con = DataSource.getInstance().getConnection();
    @FXML
    private AnchorPane hhh;

                
    /**
     * Initializes the controller class.
     */
    public void initialize(URL url, ResourceBundle rb) {
        holderPane=hhh;

        x.setLabel("nom");
        y.setLabel("quantity");
        XYChart.Series set1 = new XYChart.Series<>();
        ObservableList ok = FXCollections.observableArrayList();
        List<Produit> ev = ess.ListProduits();
        int[] ee = new int[12];
        for (Produit e : ev) {
            int rami = e.getQuantity();
            set1.getData().add(new XYChart.Data<>(e.getNom(), e.getQuantity()));
        }
            participationChart.getData().addAll(set1);   
} 
        private Object loadView(String path) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource((path)));
        AnchorPane parentContent = null;
        try {
            parentContent = fxmlLoader.load();
        } catch (IOException ex) {
            Logger.getLogger(LayoutFrontController.class.getName()).log(Level.SEVERE, null, ex);
        }
        setNode(parentContent);
        System.gc();
        return fxmlLoader.getController();
    }

    private void setNode(Node node) {
        holderPane.getChildren().clear();
        holderPane.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.millis(1500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
    }

    @FXML
    private void retour(ActionEvent event) {
        
        loadView("/GUI/CategorieBack.fxml");
    }
}
