/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;
import javafx.scene.input.MouseEvent;
import Core.Controller;
import Entities.Evenement;
import Entities.Guide;
import Service.EvenementService;
import Service.GuideService;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.collections.ObservableList;
import com.jfoenix.controls.JFXTreeTableView;
import com.sun.javafx.collections.ElementObservableListDecorator;
import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author soumaya
 */
public class Consulter_guideController extends Controller implements Initializable {

    @FXML
    private AnchorPane ajouteventpane;
    @FXML
    private TableView<Guide> display;
    @FXML
    private TableColumn<?, ?> nomEvent1;
    @FXML
    private TableColumn<?, ?> typeEvent1;
    @FXML
    private TableColumn<?, ?> nomGuide1;
    @FXML
    private TableColumn<?, ?> prenomGuide1;
    @FXML
    private TableColumn<?, ?> telGuide1;
    @FXML
    private TableColumn<?, ?> emailGuide1;
    private TextField nomEvent;
    @FXML
    private JFXButton Update;
    @FXML
    private JFXButton DELETE;
    @FXML
    private JFXButton retour;
    @FXML
    private TextField prenomGuide;
    @FXML
    private TextField TelephoneGuide;
    @FXML
    private TextField nomGuide;
    @FXML
    private TextField emailGuide;
    @FXML
    private Label alerteTelephone;
    @FXML
    private Label alertenomGuide;
    @FXML
    private Label alerteEmailGuide;
    @FXML
    private Label alertePrenomGuide;
    @FXML
    private JFXButton Ajouter;
    @FXML
    private JFXButton Annuler;
    private String typeEventt="";
   
private final GuideService es = new GuideService();
     EvenementService ser=new EvenementService();
    @FXML
    private Label alerteTelephonenombre;
    @FXML
    private Label alerteEmailGuide1;
    @FXML
    private Label eventName;
    @FXML
    private Label eventType;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
         afficherguide();
         EvenementService es = new EvenementService();
         Evenement e = es.getEvenementById(Evenement.getEvent_courant());
         eventName.setText(e.getTitre());
         eventType.setText(e.getTitreCordination());
         
    }    
public void afficherguide() {

        display.getItems().clear();
        nomEvent1.setCellValueFactory(new PropertyValueFactory<>("nomEvent"));
        typeEvent1.setCellValueFactory(new PropertyValueFactory<>("eventType"));
        nomGuide1.setCellValueFactory(new PropertyValueFactory<>("nom"));
        prenomGuide1.setCellValueFactory(new PropertyValueFactory<>("prenom"));
        telGuide1.setCellValueFactory(new PropertyValueFactory<>("tel"));
        emailGuide1.setCellValueFactory(new PropertyValueFactory<>("mail"));
        System.out.println(Evenement.getEvent_courant());
        System.out.println("88888888888888"+es.getGuidesByEventId(Evenement.getEvent_courant()).size());
       display.setItems(es.getGuidesByEventId(Evenement.getEvent_courant()));

    }
    @FXML
    private void Fetch(MouseEvent event) {
      //typeEventt.setText(display.getSelectionModel().getSelectedItem().getEventType());
       nomGuide.setText(display.getSelectionModel().getSelectedItem().getNom());
       prenomGuide.setText(display.getSelectionModel().getSelectedItem().getPrenom());
       TelephoneGuide.setText(Integer.toString(display.getSelectionModel().getSelectedItem().getTel()));
       emailGuide.setText(display.getSelectionModel().getSelectedItem().getMail());
      
    }

    @FXML
    private void modifierGuide(ActionEvent event) {
        
            Guide e = new Guide( eventName.getText(),eventType.getText(),nomGuide.getText(), prenomGuide.getText(),  emailGuide.getText(),Integer.parseInt(TelephoneGuide.getText()));
    
    es.updateGuide(e, display.getSelectionModel().getSelectedItem().getId());
            afficherguide();
    }

    @FXML
    private void supprimerGuide(ActionEvent event) {
        Guide e = (Guide) display.getSelectionModel().getSelectedItem();
    Alert alert = new Alert(Alert.AlertType.CONFIRMATION,"Etes vous sure de vouloir supprimer ce guide ?",ButtonType.YES,ButtonType.NO,ButtonType.CANCEL);
           alert.showAndWait();
           if (alert.getResult() == ButtonType.YES)
      {
        GuideService ev = new GuideService();
        int ide = e.getId();
        ev.deleteGuide(ide);
        display.getItems().removeAll(display.getSelectionModel().getSelectedItem());
        display.getSelectionModel().select(null);
    }}
     
          

   
    @FXML
    private void ajouterGuide(ActionEvent event) {
        
    
         if (ValidateFields()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("ajout impossible!");
            alert.setHeaderText(null);
            alert.setContentText("veuiller remplir les champs correctemenet SVP!");
            alert.showAndWait(); 
            return ;
            }
        else {
         


            Guide e = new Guide(Evenement.getEvent_courant(), eventName.getText(),eventType.getText(),nomGuide.getText(), prenomGuide.getText(),  emailGuide.getText(),Integer.parseInt(TelephoneGuide.getText()));
            es.insertGuide(e);
            //afficherguide();
            TelephoneGuide.setText(null);
           // nomEventt.setText(null);
           // typeEventt.setText(null);
            nomGuide.setText(null);
            prenomGuide.setText(null);
            emailGuide.setText(null);
           
      //  } 
    }
    afficherguide();
    }
  
     public boolean ValidateFields() {
         int  alerteEmailGuide11 = 0;
                int  alerteTelephonenombre11 = 0;
        int alertenomEvent1 = 0, alerteTelephone1 = 0, alerteTypeEvent1 = 0 , alertenomGuide1 = 0 , alerteEmailGuide1 = 0, alertePrenomGuide1 = 0 ;

        if ( !"0123456789".contains(TelephoneGuide.getCharacters())) {
            alerteTelephonenombre11 = 1;
            alerteTelephonenombre.setVisible(true);
        } else {
            alerteTelephonenombre.setVisible(false);
        }
        if (TelephoneGuide.getText().length() == 0) {
            alerteTelephone1 = 1;
            alerteTelephone.setVisible(true);
        } else {
            alerteTelephone.setVisible(false);
        }
         
        if (nomGuide.getText().length() == 0) {
            alertenomGuide1 = 1;
            alertenomGuide.setVisible(true);
        } else {
            alertenomGuide.setVisible(false);
        }
        if (emailGuide.getText().length() == 0) {
            alerteEmailGuide1 = 1;
            alerteEmailGuide.setVisible(true);
        } else {
            alerteEmailGuide.setVisible(false);
        }
        
        
       
        return (alerteTelephonenombre11 == 1 || alertenomEvent1 == 1 || alerteTelephone1 == 1 || alerteTypeEvent1 == 1 || alertenomGuide1 == 1||  alerteEmailGuide1 == 1 || alertePrenomGuide1 == 1 );
      

    }

    @FXML
    private void retourclicked(ActionEvent event) throws IOException {
          System.out.println("test");
        ajouteventpane.getChildren().clear();
       ajouteventpane.getChildren().add(new FXMLLoader().load(getClass().getResource("/GUI/ListeEvent.fxml")));
    }

    @FXML
    private void annulerGuide(ActionEvent event) throws IOException {
         Parent p1 = FXMLLoader.load(getClass().getResource("/GUI/ListeEvent.fxml"));
                Scene test1 = new Scene(p1);
                Stage App1 = (Stage) ((Node) event.getSource()).getScene().getWindow();
                App1.setScene(test1);
                App1.show();
    }

    @FXML
    private void numerique(ActionEvent event) {
    }
    
}
