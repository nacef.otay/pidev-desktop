/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import static Core.Controller.holderPane;
import Entities.Demande_responsable_hote;
import Entities.Maisons_hotes;
import Entities.User;
import Service.DemandeRespoHoteService;
import Service.MaisonHoteService;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import tray.animations.AnimationType;
import static tray.notification.NotificationType.SUCCESS;
import tray.notification.TrayNotification;

/**
 * FXML Controller class
 *
 * @author Bhs Nada
 */
public class AdminHoteController implements Initializable {

    @FXML
    private TableView<Maisons_hotes> maison;
    @FXML
    private TableColumn<Maisons_hotes, String> nom;
    @FXML
    private TableColumn<Maisons_hotes, String> cap;
    @FXML
    private TableColumn<Maisons_hotes, String> pays;
    @FXML
    private TableColumn<Maisons_hotes, String> tel;
    @FXML
    private TableColumn<Maisons_hotes, String> mail;
    @FXML
    private TableColumn<Maisons_hotes, String> adres;
    @FXML
    private TableColumn<Maisons_hotes, String> site;
    @FXML
    private TableView<Demande_responsable_hote> Responsable;
    @FXML
    private TableColumn<Demande_responsable_hote, String> utilisateur;
    @FXML
    private TableColumn<Demande_responsable_hote, String> description;
    @FXML
    private Button supprimer;
    @FXML
    private Button accepter;
    @FXML
    private Button supp;

    MaisonHoteService maisonHoteService = new MaisonHoteService();
    DemandeRespoHoteService demandeRespoHoteService = new DemandeRespoHoteService();
    Demande_responsable_hote D = new Demande_responsable_hote();
     final ObservableList maisonh =(ObservableList) maisonHoteService.list();
  
     @FXML
    private TextField tf_recherche;
    @FXML
    private TextField tf_recherche1;
 final ObservableList<Maisons_hotes> data = FXCollections.observableArrayList();
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        loadHotes();
        loadDemande();
       
        nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        pays.setCellValueFactory(new PropertyValueFactory<>("pays"));
        adres.setCellValueFactory(new PropertyValueFactory<>("adresse"));
        cap.setCellValueFactory(new PropertyValueFactory<>("capacites"));
        mail.setCellValueFactory(new PropertyValueFactory<>("mail"));
        tel.setCellValueFactory(new PropertyValueFactory<>("tel"));
        site.setCellValueFactory(new PropertyValueFactory<>("site_web"));

        utilisateur.setCellValueFactory(new PropertyValueFactory<>("id"));
        description.setCellValueFactory(new PropertyValueFactory<>("description"));
        try {
            FilterMaison();
            FilterDemande();
        } catch (SQLException ex) {
            Logger.getLogger(AdminHoteController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadHotes() {
        MaisonHoteService maisonHoteService = new MaisonHoteService();
        ArrayList<Maisons_hotes> listeHote = maisonHoteService.AfficherMaisonsHotes();

        ObservableList observableList = FXCollections.observableArrayList(listeHote);
        maison.setItems(observableList);

    }

    public void loadDemande() {
        DemandeRespoHoteService demandeRespoHoteService = new DemandeRespoHoteService();
        ArrayList<Demande_responsable_hote> listeDemande = demandeRespoHoteService.AfficherDemande();

        ObservableList observableList = FXCollections.observableArrayList(listeDemande);
        Responsable.setItems(observableList);

    }

    private void setNode(Node node) {
        holderPane.getChildren().clear();
        holderPane.getChildren().add((Node) node);

        FadeTransition ft = new FadeTransition(Duration.millis(1500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
    }

    private Object loadView(String path) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource((path)));
        AnchorPane parentContent = null;
        try {
            parentContent = fxmlLoader.load();
        } catch (IOException ex) {
            Logger.getLogger(ListesHotesController.class.getName()).log(Level.SEVERE, null, ex);
        }
        setNode(parentContent);
        System.gc();
        return fxmlLoader.getController();
    }


    @FXML
    private void supprimerMaison(ActionEvent event) {
        Maisons_hotes maisons_hotes = new Maisons_hotes();
        maisons_hotes = maison.getSelectionModel().getSelectedItem();
        if (maisons_hotes == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Alerte");
            alert.setHeaderText("Alerte");
            alert.setContentText("Veillez Choisir une Maison d'hôte a supprimer");
            alert.show();

        } else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirmation");
            alert.setHeaderText(null);
            alert.setContentText("vous êtes sûr de supprimer la maison");
            Optional<ButtonType> action = alert.showAndWait();

            if (action.get() == ButtonType.OK) {
                maisonHoteService.SupprimerHote(maisons_hotes);
                TrayNotification tray = new TrayNotification("Avec succès", "Maison d'hôte  N° " + maisons_hotes.getId() + " a été Supprimer avec succés !", SUCCESS);
                tray.setAnimationType(AnimationType.POPUP);
                tray.showAndDismiss(Duration.seconds(10));

                loadHotes();
            }

        }
    }

    @FXML
    private void AccepterDemande(ActionEvent event) throws SQLException {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation ");
        alert.setHeaderText(null);
        alert.setContentText("Vous êtes sûr d'affecter le rôle ?");
        Optional<ButtonType> action = alert.showAndWait();

        if (action.get() == ButtonType.OK) {
            Demande_responsable_hote demande_responsable_hote = new Demande_responsable_hote();
            demande_responsable_hote = Responsable.getSelectionModel().getSelectedItem();
            
            DemandeRespoHoteService demandeRespoHoteService = new DemandeRespoHoteService();
            demandeRespoHoteService.AjouterRole(demande_responsable_hote.getId_user());
 demandeRespoHoteService.SupprimerDemande(demande_responsable_hote);
            
            Responsable.getItems().clear();
            Responsable.getItems().addAll(demandeRespoHoteService.AfficherDemande());
           
        }
          
    }

    @FXML
    private void suppDemande(ActionEvent event) {
        Demande_responsable_hote demande_responsable_hote = new Demande_responsable_hote();
        demande_responsable_hote = Responsable.getSelectionModel().getSelectedItem();
       DemandeRespoHoteService demandeRespoHoteService = new DemandeRespoHoteService();
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirmation");
            alert.setHeaderText(null);
            alert.setContentText("vous êtes sûr de supprimer la demande");
            Optional<ButtonType> action = alert.showAndWait();

            if (action.get() == ButtonType.OK) {
                demandeRespoHoteService.SupprimerDemande(demande_responsable_hote);  
                Responsable.getItems().clear();
              loadDemande();
                   

            }

        }


    
  private void FilterMaison() throws SQLException {
      MaisonHoteService rs = new MaisonHoteService();
        ArrayList AL = (ArrayList) rs.AfficherMaisonsHotes();
        ObservableList OReservation = FXCollections.observableArrayList(AL);
        FilteredList<Maisons_hotes> filtred_c = new FilteredList<>(OReservation, e -> true);
        tf_recherche.setOnKeyReleased(e -> {
            tf_recherche.textProperty().addListener((observableValue, oldValue, newValue) -> {
                filtred_c.setPredicate((Predicate<? super Maisons_hotes>) cat -> {
                    if (newValue == null || newValue.isEmpty()) {
                        return true;
                    }
                    String toLowerCaseNewValue = newValue.toLowerCase();
                    if ((cat.getNom().toLowerCase().contains(toLowerCaseNewValue)) || (cat.getPays().toLowerCase().contains(toLowerCaseNewValue)) ||(Integer.toString(cat.getCapacites()).toLowerCase().contains(toLowerCaseNewValue))   ) {
                        return true;

                    }

                    return false;
                });
            });
        });
        maison.setItems(filtred_c);
    }
    
  private void FilterDemande() throws SQLException {
      DemandeRespoHoteService rs = new DemandeRespoHoteService();
        ArrayList AL = (ArrayList) rs.AfficherDemande();
        ObservableList OReservation = FXCollections.observableArrayList(AL);
        FilteredList<Demande_responsable_hote> filtred_c = new FilteredList<>(OReservation, e -> true);
        tf_recherche1.setOnKeyReleased(e -> {
            tf_recherche1.textProperty().addListener((observableValue, oldValue, newValue) -> {
                filtred_c.setPredicate((Predicate<? super Demande_responsable_hote>) cat -> {
                    if (newValue == null || newValue.isEmpty()) {
                        return true;
                    }
                    String toLowerCaseNewValue = newValue.toLowerCase();
                    if (Integer.toString(cat.getId()).toLowerCase().contains(toLowerCaseNewValue) ||(cat.getDescription().toLowerCase().contains(toLowerCaseNewValue))) {
                        return true;

                    }

                    return false;
                });
            });
        });
        Responsable.setItems(filtred_c);
    }
}
