/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import static Core.Controller.holderPane;
import Core.LayoutFrontController;
import Entities.Groups;
import Service.GroupeService;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import controller.afficheproduit;
import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;

import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;


/**
 * FXML Controller class
 *
 * @author Hsine
 */
public class UpdateGroupeController implements Initializable {
  GroupeService service = new GroupeService();

    @FXML
    private JFXTextField titre;
    @FXML
    private JFXTextArea description;
    @FXML
    private Button ajouter;
    @FXML
    private JFXButton fichier;
    @FXML
    private ImageView image_p;
     final FileChooser fileChooser = new FileChooser();
    private Desktop desktop = Desktop.getDesktop();
    private String file_image;
    private Path pathfrom;
    private Path pathto;
    private File Current_file;
            private FileInputStream fis;


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Groups EvenementCourant = new Groups();
        EvenementCourant = service.getGroupeById(EvenementCourant.getGroupe_courant());

        titre.setText(EvenementCourant.getNom());
        
        description.setText(EvenementCourant.getDescription());
        String A = EvenementCourant.getImage();
        A = "C:\\wamp64\\www\\Pidev Desktop\\src\\Images\\" + A;
        File F1 = new File(A);
        Image image2 = new Image(F1.toURI().toString());
        image_p.setImage(image2);
    }

    @FXML
    private void ajouter(ActionEvent event) throws IOException, SQLException {
        Groups events = new Groups();
        System.out.println("222222222");
        
        events.setId(Groups.getGroupe_courant());
        
        
        events.setNom(titre.getText());
        
        events.setDescription(description.getText());
        events.setDate_de_creation(events.getDate_de_creation());
        events.setIdUser(1);
        events.setNbrMembre(events.getNbrMembre());
        events.setNbr_signal(events.getNbr_signal());
       
        
        //// upload image ///////
       // file_image = "/images/" + file_image;
        events.setImage(file_image);
        pathfrom = FileSystems.getDefault().getPath(Current_file.getPath());
        pathto = FileSystems.getDefault().getPath("C:\\wamp64\\www\\Pidev Desktop\\src\\Images\\" + Current_file.getName());
        Path targetDir = FileSystems.getDefault().getPath("C:\\wamp64\\www\\Pidev Desktop\\src\\Images\\");
        Files.copy(pathfrom, pathto, StandardCopyOption.REPLACE_EXISTING);

        ///////////////
        service.UpdateGroupe(events);
        loadView("/GUI/affichageGroupe.fxml");


    }

    @FXML
    private void fichier_image(ActionEvent event) throws MalformedURLException {
           FileChooser fc = new FileChooser();
        Current_file = fc.showOpenDialog(null);
        if (Current_file != null) {
        Image images = new Image(Current_file.toURI().toString(),100,100,true,true);
        image_p.setImage(images);
            try {
                fis = new FileInputStream(Current_file);
                file_image = Current_file.getName();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(UpdateGroupeController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }

        final GridPane inputGridPane = new GridPane();
        GridPane.setConstraints(fichier, 0, 0);
        inputGridPane.setHgap(6);
        inputGridPane.setVgap(6);
        final Pane rootGroup = new VBox(12);
        rootGroup.getChildren().addAll(inputGridPane);
        rootGroup.setPadding(new Insets(12, 12, 12, 12));

    }
    private void openFile(File file) {
        try {
            desktop.open(file);
        } catch (IOException ex) {

            Logger.getLogger(
                    afficheproduit.class.getName()).log(
                    Level.SEVERE, null, ex
            );
        }
    }
     private void setNode(Node node) {
        holderPane.getChildren().clear();
        holderPane.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.millis(1500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
    }
    
    private Object loadView(String path)
    {
        FXMLLoader fxmlLoader= new FXMLLoader(getClass().getResource((path)));
        AnchorPane parentContent = null;
        try {
            parentContent = fxmlLoader.load();
        } catch (IOException ex) {
            Logger.getLogger(LayoutFrontController.class.getName()).log(Level.SEVERE, null, ex);
        }
        setNode(parentContent);
        System.gc();
        return fxmlLoader.getController();
    }

}
