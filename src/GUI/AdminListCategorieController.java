/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Entities.Categorie;
import Service.CategorieService;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author marie
 */
public class AdminListCategorieController implements Initializable {

    @FXML
    private TableView<Categorie> table;
    @FXML
    private TableColumn<Categorie, String> type;
    @FXML
    private Button btnRetour;
    @FXML
    private Button btnAjouter;
    @FXML
    private Button btnModifier;
    @FXML
    private Button btnSupprimer;
    @FXML
    private Label titreCat;
    @FXML
    private TextField textType;
    @FXML
    private Button btnValiderAjout;
    @FXML
    private Button btnAnnulerAjout;
    @FXML
    private Label titreAjout;
    @FXML
    private Label titreModif;
    @FXML
    private Label titreCatModif;
    @FXML
    private TextField textTypeModif;
    @FXML
    private Button btnValiderModif;
    @FXML
    private Button btnAnnulerModif;
    CategorieService catService = new CategorieService();
    @FXML
    private AnchorPane holderPane;
    @FXML
    private TextField search;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       /* try {
            // TODO
            /////form ajout **********
            SearchCategorieType();
        } catch (SQLException ex) {
            Logger.getLogger(AdminListCategorieController.class.getName()).log(Level.SEVERE, null, ex);
        }*/
       search.setVisible(false);
        titreAjout.setVisible(false);
        btnAnnulerAjout.setVisible(false);
        btnValiderAjout.setVisible(false);
        textType.setVisible(false);
        titreCat.setVisible(false);
        //*************
        
        /////form modif
         btnAnnulerModif.setVisible(false);
        btnValiderModif.setVisible(false);

        textTypeModif.setVisible(false);

        titreCatModif.setVisible(false);
        titreModif.setVisible(false);
        ///***************
       ChargerCategorie();
        type.setCellValueFactory(new PropertyValueFactory<>("type"));
         btnSupprimer.setOnMouseClicked(x -> {
 Categorie cat = new Categorie();
cat = table.getSelectionModel().getSelectedItem();

        if (cat== null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Alerte");
            alert.setHeaderText("Alerte");
            alert.setContentText("Il faut tout d'abord sélectionner une catégorie");
            alert.show();
        } else {

            // get Selected Item
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Etes vous sure de vouloir supprimer cette Catégorie?", ButtonType.YES, ButtonType.NO, null);
            alert.showAndWait();

            if (alert.getResult() == ButtonType.YES) {
                //remove selected item from the table list
                catService.SupprimerCategorie(cat);
                 table.getItems().clear();
        
            table.getItems().addAll(catService.AfficherCategorie());
         ChargerCategorie();
            }
        }
        });

    }

    @FXML
    private void Retour(ActionEvent event) throws IOException {
         FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../GUI/AdminChoixBP.fxml"));
        AnchorPane parentContent = fxmlLoader.load();
        holderPane.getChildren().clear();
        holderPane.getChildren().setAll(parentContent);
    }

    @FXML
    private void AfficheAjouterCategorie(ActionEvent event) {
         /////form ajout **********
        titreAjout.setVisible(true);
        btnAnnulerAjout.setVisible(true);
        btnValiderAjout.setVisible(true);
        textType.setVisible(true);
        titreCat.setVisible(true);
        //*************
        
        
    }

      private void SearchCategorieType() throws SQLException {
        CategorieService bs = new CategorieService();
        ArrayList AL = (ArrayList) bs.AfficherCategorie();
        ObservableList OReservation = FXCollections.observableArrayList(AL);
        FilteredList<Categorie> filtred_c = new FilteredList<>(OReservation, e -> true);
        search.setOnKeyReleased(e -> {
            search.textProperty().addListener((observableValue, oldValue, newValue) -> {
                filtred_c.setPredicate((Predicate<? super Categorie>) cat -> {
                    if (newValue == null || newValue.isEmpty()) {
                        return true;
                    }
                    String toLowerCaseNewValue = newValue.toLowerCase();
                    if ((cat.getType().toLowerCase().contains(toLowerCaseNewValue)) ) {
                        return true;

                    }

                    return false;
                });
            });
        });
        table.setItems(filtred_c);
    }
public void ChargerCategorie(){
    CategorieService Categorieservice = new CategorieService();
        ArrayList<Categorie> listeCatgeorie = Categorieservice.AfficherCategorie();

        ObservableList observableList = FXCollections.observableArrayList(listeCatgeorie);
        table.setItems(observableList);
}

    @FXML
    private void AjouterCategorie(ActionEvent event) {
       String t= null;
            t=catService.getCategorie(textType.getText());
            System.out.println("***********" +t);
           if(t==null){
             Categorie cat = new Categorie(textType.getText());

           catService.ajouterCatgeorie(cat);
            System.out.println("****done");
             Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Catégorie  ");
            alert.setHeaderText(null);
            alert.setContentText("Catégorie ajouté !");
            alert.showAndWait();
            titreAjout.setVisible(false);
        btnAnnulerAjout.setVisible(false);
        btnValiderAjout.setVisible(false);
        textType.setVisible(false);
        titreCat.setVisible(false);
           ChargerCategorie();}
        else{
                  Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Catégorie  ");
            alert.setHeaderText(null);
            alert.setContentText("Catégorie existante!");
            alert.showAndWait();
                }
    }

    @FXML
    private void AnnulerAjout(ActionEvent event) {
        titreAjout.setVisible(false);
        btnAnnulerAjout.setVisible(false);
        btnValiderAjout.setVisible(false);
        textType.setVisible(false);
        titreCat.setVisible(false);
    }

    @FXML
    private void AfficherModifierCategorie(ActionEvent event) {
        Categorie cat = new Categorie();

        cat = table.getSelectionModel().getSelectedItem();

        if (cat== null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Alerte");
            alert.setHeaderText("Alerte");
            alert.setContentText("Il faut tout d'abord sélectionner une catégorie");
            alert.show();
        } else {

            /////form modif
         btnAnnulerModif.setVisible(true);
        btnValiderModif.setVisible(true);

        textTypeModif.setVisible(true);

        titreCatModif.setVisible(true);
        titreModif.setVisible(true);
        ///***************
        textTypeModif.setText(cat.getType());
       
       
            

        }
       
    }

    @FXML
    private void AfficherSupprimerCategorie(ActionEvent event) {
    }

    @FXML
    private void modifierCategorie(ActionEvent event) {
        Categorie cat = new Categorie();
        cat = table.getSelectionModel().getSelectedItem();
        
        cat.setType(textTypeModif.getText());
       
        catService.ModifierCategorie(cat, cat.getId());
        table.getItems().clear();
        
            table.getItems().addAll(catService.AfficherCategorie());
         ChargerCategorie();

          /////form modif
         btnAnnulerModif.setVisible(false);
        btnValiderModif.setVisible(false);

        textTypeModif.setVisible(false);

        titreCatModif.setVisible(false);
        titreModif.setVisible(false);
        ///***************
       
    }

    @FXML
    private void AnnulerModif(ActionEvent event) {
         /////form modif
         btnAnnulerModif.setVisible(false);
        btnValiderModif.setVisible(false);

        textTypeModif.setVisible(false);

        titreCatModif.setVisible(false);
        titreModif.setVisible(false);
        ///***************
    }
 
}
