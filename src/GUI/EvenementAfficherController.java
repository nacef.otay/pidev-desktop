/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Entities.AvisEvenement;
import Entities.Evenement;
import Service.AvisEvenementService;
import Service.EvenementService;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.cells.editors.TextFieldEditorBuilder;
import com.jfoenix.controls.cells.editors.base.GenericEditableTreeTableCell;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import com.sun.prism.impl.Disposer;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableColumn.CellEditEvent;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.web.HTMLEditor;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author soumaya
 */
public class EvenementAfficherController  implements Initializable {

    FileChooser saveFileChooser = new FileChooser();
    File saveFile;
    File srcFile, destFile;

    @FXML
    private TableView<Evenement> display;
    @FXML
    private TableColumn<Evenement, Number> nbplaces;
    @FXML
    private TableColumn<Evenement, Date> date;
    @FXML
    private TableColumn<Evenement, String> titre;
    @FXML
    private TableColumn<Evenement, String> description;
    @FXML
    private TableColumn<Evenement, String> typeevent;
    @FXML
    private TableColumn<Evenement, Date> datefin;
    @FXML
    private TableColumn<Evenement, String> lieu;
    @FXML
    private TableColumn<Evenement, Double> prix;

    private final EvenementService es = new EvenementService();
    private final AvisEvenementService as = new AvisEvenementService();

   
   
    
    
    @FXML
    private TextArea descriptionn;
    
  
    @FXML
    private JFXButton retour;
    @FXML
    private AnchorPane ajouteventpane;
    
    //text field
    @FXML
    private TextField titree;
    @FXML
    private DatePicker dateEvenement;
    
   
    
    private TextField type;
    @FXML
    private TextField adr;
  
   
    @FXML
    private DatePicker datefinn;
    @FXML
    private TextField prixx;
    
    @FXML
    private Label alertetitre;
    @FXML
    private Label alertetypeevent;
    @FXML
    private Label alertedescription;
    @FXML
    private Label alertedatedebut;
    @FXML
    private Label datesup;
    @FXML
    private Label alertedatefin;
    @FXML
    private Label alertenbreplace;
    @FXML
    private Label alertelieu;
    @FXML
    private Label alerteprix;
    @FXML
    private TextField nbreplace;
    @FXML
    private TextField nbplacess;
   
    @FXML
    private JFXButton Update;
    @FXML
    private JFXButton DELETE;
    @FXML
    private JFXButton Image;
    @FXML
    private TableView<AvisEvenement> display1;
    @FXML
    private TableColumn<?, ?> contenu;
     @FXML
    private TableColumn<AvisEvenement, String> ColumnIdEvenement;
     
     private String typeEventt="";
    @FXML
    private RadioButton culturelle;
    @FXML
    private RadioButton sport;
    @FXML
    private RadioButton aventure;
    
    
    
  
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        // TODO
        afficherevent();
        
        TableColumn col_action = new TableColumn<>("Consulter guide");

        display.getColumns().add(col_action);
        
        col_action.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<Disposer.Record, Boolean>, ObservableValue<Boolean>>() {

            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Disposer.Record, Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });

        //Adding the Button to the cell
        col_action.setCellFactory(
                new Callback<TableColumn<Disposer.Record, Boolean>, TableCell<Disposer.Record, Boolean>>() {

            @Override
            public TableCell<Disposer.Record, Boolean> call(TableColumn<Disposer.Record, Boolean> p) {
                return new GUI.ButtonGuide();
            }

        });
    }
     public void afficherAvis(ObservableList<AvisEvenement> listAvis)
    {
        display1.getItems().clear();
        contenu.setCellValueFactory(new PropertyValueFactory<>("contenu"));
        display1.setItems(listAvis);
    }

    public void afficherevent() {
//        ObservableList ok = FXCollections.observableArrayList();
//        ok = es.getEv();
//        display.setItems(null);
//        display.setItems(ok);
        display.getItems().clear();
        nbplaces.setCellValueFactory(new PropertyValueFactory<>("nbplaces"));
        titre.setCellValueFactory(new PropertyValueFactory<>("titre"));
        description.setCellValueFactory(new PropertyValueFactory<>("description"));
        typeevent.setCellValueFactory(new PropertyValueFactory<>("titreCordination"));
        date.setCellValueFactory(new PropertyValueFactory<>("dateEvenement"));
        datefin.setCellValueFactory(new PropertyValueFactory<>("datefin"));
        lieu.setCellValueFactory(new PropertyValueFactory<>("adr"));
        prix.setCellValueFactory(new PropertyValueFactory<>("prix"));
        display.setItems(es.getEv1());

    }
    



    @FXML
    private void supprimerEvenements(ActionEvent event) {
        Evenement e = display.getSelectionModel().getSelectedItem();
         Alert alert = new Alert(Alert.AlertType.CONFIRMATION,"Etes vous sure de vouloir supprimer ce événement ?",ButtonType.YES,ButtonType.NO,ButtonType.CANCEL);
           alert.showAndWait();
           if (alert.getResult() == ButtonType.YES)
      {
        EvenementService ev = new EvenementService();
        int ide = e.getId();
        ev.deleteEvenement(ide);
        display.getItems().removeAll(display.getSelectionModel().getSelectedItem());
        display.getSelectionModel().select(null);
    }}

    @FXML
    private void UploadImage(ActionEvent event) {
        
        File file = saveFileChooser.showOpenDialog(null);
        if (file != null) {
            srcFile = file;
            if (srcFile != null) {
                try {
                    String p = System.getProperty("user.dir") + "/src/images/" + srcFile.getName();
                    copyFile(srcFile, new File(p));
                } catch (IOException ex) {
                    Logger.getLogger(EvenementAfficherController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
    }

    public void copyFile(File sourceFile, File destFile) throws IOException {
        if (!destFile.exists()) {
            destFile.createNewFile();
        }

        try (
                    FileChannel in = new FileInputStream(sourceFile).getChannel();
                    FileChannel out = new FileOutputStream(destFile).getChannel();) {

            out.transferFrom(in, 0, in.size());
        }
    }

    public boolean ValidateFields() {
        int alertetitre1 = 0, alertetypeevent1 = 0, alertedescription1 = 0 , alertedatedebut1 = 0 , alertenbreplace1 = 0, alertelieu1 = 0, alertedatefin1 =0,alerteprix1 =0 ;

        if (titree.getText().length() == 0) {
            alertetitre1 = 1;
            alertetitre.setVisible(true);
        } else {
            alertetitre.setVisible(false);
        }
        if (type.getText().length() == 0) {
            alertetypeevent1 = 1;
            alertetypeevent.setVisible(true);
        } else {
            alertetypeevent.setVisible(false);
        }
         if (nbreplace.getText().length() == 0) {
            alertenbreplace1 = 1;
            alertenbreplace.setVisible(true);
        } else {
            alertenbreplace.setVisible(false);
        }
        if (descriptionn.getText().length() == 0) {
            alertedescription1 = 1;
            alertedescription.setVisible(true);
        } else {
            alertedescription.setVisible(false);
        }
        if (adr.getText().length() == 0) {
            alertedescription1 = 1;
            alertelieu.setVisible(true);
        } else {
            alertelieu.setVisible(false);
        }
        if (prixx.getText().length() == 0) {
            alerteprix1 = 1;
            alerteprix.setVisible(true);
        } else {
            alerteprix.setVisible(false);
        }
        
        //test date fin
        if (this.datefinn.getEditor().getText().length() == 0) {
             alertedatefin1 = 1;
            alertedatefin.setVisible(true);
             } else {
            alertedatefin.setVisible(false);
        }
        
        //teste date debut
        if (this.dateEvenement.getEditor().getText().length() == 0) {
             alertedatedebut1 = 1;
            alertedatedebut.setVisible(true);
            datesup.setVisible(false); }
            
            else if ((this.dateEvenement.getValue().isBefore(LocalDate.now()))) {
            alertedatedebut1 = 1;
            datesup.setVisible(true);
            alertedatedebut.setVisible(false);
        } else {
            datesup.setVisible(false);
            alertedatedebut.setVisible(false);
            System.out.println("date :" + this.dateEvenement.getValue());
            System.out.println(LocalDate.now());
        }
        return (alertetitre1 == 1 || alertetypeevent1 == 1 || alertedescription1 == 1 || alertedatedebut1 == 1||  alertenbreplace1 == 1);
      

    }

    @FXML
    private void Fetch(MouseEvent event) {
       titree.setText(display.getSelectionModel().getSelectedItem().getTitre());
      //type.setText(display.getSelectionModel().getSelectedItem().getTitreCordination());
        descriptionn.setText(display.getSelectionModel().getSelectedItem().getDescription());
        dateEvenement.setValue(LocalDate.parse(display.getSelectionModel().getSelectedItem().getDateEvenement().toString()));
       datefinn.setValue(LocalDate.parse(display.getSelectionModel().getSelectedItem().getDatefin().toString()));
     //  nbreplace.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 100,display.getSelectionModel().getSelectedItem().getNbplaces().getInteger(typeEventt)));
      nbreplace.setText(Integer.toString(display.getSelectionModel().getSelectedItem().getNbplaces()));
      prixx.setText(Double.toString(display.getSelectionModel().getSelectedItem().getPrix()));
             adr.setText(display.getSelectionModel().getSelectedItem().getAdr());

        int ide = display.getSelectionModel().getSelectedItem().getId();
        afficherAvis(as.getAvis(ide));
        Evenement.setEvent_courant(ide);
    }

    @FXML
    private void modifierEvenements(ActionEvent event) {
        if(aventure.isSelected())
                    {
                        typeEventt="Aventure";
                        
                    }
            if(sport.isSelected())
            {
                        typeEventt="Sport";
                    }
            if(culturelle.isSelected())
            {
                        typeEventt="Culturelle";
                    }
          
             //datedebut
            LocalDate localDateDebut = dateEvenement.getValue();
            Instant instant = Instant.from(localDateDebut.atStartOfDay(ZoneId.systemDefault()));
            java.util.Date date = Date.from(instant);
            java.sql.Date dtdebut = new java.sql.Date(date.getTime());
            //datefin
             LocalDate localDateFin = datefinn.getValue();
            Instant instant1 = Instant.from(localDateFin.atStartOfDay(ZoneId.systemDefault()));
            java.util.Date datefin = Date.from(instant1);
            java.sql.Date dtfin = new java.sql.Date(datefin.getTime());
            System.out.println("5555555555555555"+display.getSelectionModel().getSelectedItem().getId());
            
             Evenement e = new Evenement(srcFile.getName(), Integer.valueOf(nbreplace.getText()), dtdebut, titree.getText(), descriptionn.getText(), typeEventt, dtfin,  Double.valueOf(prixx.getText()),adr.getText());


            es.updateEvenement(e,display.getSelectionModel().getSelectedItem().getId());
            afficherevent();
    }

  public void setNode(Node node) {
        ajouteventpane.getChildren().clear();
        ajouteventpane.getChildren().add((Node) node);

        FadeTransition ft = new FadeTransition(Duration.millis(1500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
  }

    @FXML
    private void retourclicked(ActionEvent event) throws IOException {
         System.out.println("test");
        ajouteventpane.getChildren().clear();
       ajouteventpane.getChildren().add(new FXMLLoader().load(getClass().getResource("/GUI/ListeEvent.fxml")));
  
    }

    @FXML
    private void changedCulture(ActionEvent event) {
        culturelle.setSelected(true);
        aventure.setSelected(false);
        sport.setSelected(false);
    }

    @FXML
    private void changedSport(ActionEvent event) {
         culturelle.setSelected(false);
        aventure.setSelected(false);
        sport.setSelected(true);
    }

    @FXML
    private void changedAventure(ActionEvent event) {
         culturelle.setSelected(false);
        aventure.setSelected(true);
        sport.setSelected(false);
    }

    @FXML
    private void setdatedebut(ActionEvent event) {
    }

    @FXML
    private void numeriquePrix(ActionEvent event) {
    }

    @FXML
    private void numerique(ActionEvent event) {
    }

}
