/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Entities.Bonplan;
import Service.BonPlanService;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author marie
 */
public class AdminListBonPlanController implements Initializable {

    @FXML
    private TableView<Bonplan> table;
    @FXML
    private TableColumn<Bonplan, String> name;
    @FXML
    private TableColumn<Bonplan, String> adresse;
    @FXML
    private TableColumn<Bonplan, Integer> phone;
    @FXML
    private TableColumn<Bonplan, String> categorie;
    @FXML
    private TableColumn<Bonplan, Integer> note;
    @FXML
    private TableColumn<Bonplan, Integer> etoile;
    @FXML
    private TableColumn<Bonplan, Double> prix;
    @FXML
    private TableColumn<Bonplan, Integer> id;
    @FXML
    private Button btnSupprimer;
    @FXML
    private Button btnRetour;
    
     BonPlanService bonplanService = new BonPlanService();
    @FXML
    private AnchorPane holderPane;
    @FXML
    private TextField search;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        ChargerBonPlan();
      search.setVisible(false);
        name.setCellValueFactory(new PropertyValueFactory<>("name"));
        adresse.setCellValueFactory(new PropertyValueFactory<>("adresse"));
         phone.setCellValueFactory(new PropertyValueFactory<>("phone"));
        etoile.setCellValueFactory(new PropertyValueFactory<>("etoile"));
        note.setCellValueFactory(new PropertyValueFactory<>("note"));
        prix.setCellValueFactory(new PropertyValueFactory<>("prix"));
        categorie.setCellValueFactory(new PropertyValueFactory<>("categorie"));
        //////
              ///////
           btnSupprimer .setOnMouseClicked(x -> {
 Bonplan cat = new Bonplan();
cat = table.getSelectionModel().getSelectedItem();

        if (cat== null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Alerte");
            alert.setHeaderText("Alerte");
            alert.setContentText("Il faut tout d'abord sélectionner un bon plan");
            alert.show();
        } else {

            // get Selected Item
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Etes vous sure de vouloir supprimer ce bon plan?", ButtonType.YES, ButtonType.NO, null);
            alert.showAndWait();

            if (alert.getResult() == ButtonType.YES) {
                //remove selected item from the table list
                bonplanService.SupprimerBonPlan(cat);
                //bonplanService.SupprimerCategorie(cat);
                 table.getItems().clear();
        
            table.getItems().addAll(bonplanService.AfficherBonPlan());
            ChargerBonPlan();
            }
        }
        });
        /////
    }    
/////////////////
    
    public void ChargerBonPlan() {
        
        BonPlanService BonPlanService = new BonPlanService();
        ArrayList<Bonplan> listeBonPlan = BonPlanService.AfficherBonPlan();

        ObservableList observableList = FXCollections.observableArrayList(listeBonPlan);
        table.setItems(observableList);

    }
    /////////////////////
    @FXML
    private void tableclick(MouseEvent event) {
    }

      private void setNode(Node node) {
        holderPane.getChildren().clear();
        holderPane.getChildren().add((Node) node);

        FadeTransition ft = new FadeTransition(Duration.millis(1500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
    }


    @FXML
    private void Retour(ActionEvent event) throws IOException {
         FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(("../GUI/AdminChoixBP.fxml")));
        AnchorPane parentContent = fxmlLoader.load();
        holderPane.getChildren().clear();
        holderPane.getChildren().setAll(parentContent);
    }
    /*
    private void SearchBonPlanName() throws SQLException {
        BonPlanService bs = new BonPlanService();
        ArrayList AL = (ArrayList) bs.AfficherBonPlan();
        ObservableList OReservation = FXCollections.observableArrayList(AL);
        FilteredList<Bonplan> filtred_c = new FilteredList<>(OReservation, e -> true);
        search.setOnKeyReleased(e -> {
            search.textProperty().addListener((observableValue, oldValue, newValue) -> {
                filtred_c.setPredicate((Predicate<? super Bonplan>) cat -> {
                    if (newValue == null || newValue.isEmpty()) {
                        return true;
                    }
                    String toLowerCaseNewValue = newValue.toLowerCase();
                    if ((cat.getName().toLowerCase().contains(toLowerCaseNewValue)) ) {
                        return true;

                    }

                    return false;
                });
            });
        });
        table.setItems(filtred_c);
    }*/
}
