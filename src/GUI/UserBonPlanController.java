/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import static Core.Controller.holderPane;
import Core.LayoutFrontController;
import Entities.Bonplan;
import Entities.Categorie;
import Service.BonPlanService;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import org.controlsfx.control.Rating;

/**
 * FXML Controller class
 *
 * @author marie
 */
public class UserBonPlanController implements Initializable {
        BonPlanService bonplanService = new BonPlanService();

    
   
    @FXML
    private TableView<Bonplan> table;
    @FXML
    private TableColumn<Bonplan, String> name;
    @FXML
    private TableColumn<Bonplan, String> adresse;
    @FXML
    private TableColumn<Bonplan, Integer> phone;
    @FXML
    private TableColumn<Bonplan, String> categorie;
    @FXML
    private TableColumn<Bonplan, Integer> note;
    @FXML
    private TableColumn<Bonplan, Rating> etoile;
    @FXML
    private TableColumn<Bonplan, Double> prix;
    @FXML
    private TableColumn<Bonplan, Integer> id;
    @FXML
    private Button btnmodif;
    @FXML
    private Button btnsupprimer;
    @FXML
    private Button retourbtn;

    
    ObservableList<Bonplan> observableList = FXCollections.observableArrayList();
    @FXML
    private Label titremodif;
    @FXML
    private Label titrename;
    @FXML
    private Label titreadresse;
    @FXML
    private Label titrephone;
    @FXML
    private Label titrenote;
    @FXML
    private Label titreetoile;
    @FXML
    private Label titreprix;
    @FXML
    private Label titredescription;
    @FXML
    private TextField nameD;
    @FXML
    private TextField adresseD;
    @FXML
    private TextField phoneD;
    @FXML
    private TextField noteD;
    @FXML
    private Rating etoileD;
    @FXML
    private TextField prixD;
    @FXML
    private TextField descriptionD;
    @FXML
    private Button validerModif;
    @FXML
    private Button annulerModif;
    @FXML
    private TextField search;


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        ChargerBonPlan();
           search.setVisible(false);
        name.setCellValueFactory(new PropertyValueFactory<>("name"));
        adresse.setCellValueFactory(new PropertyValueFactory<>("adresse"));
         phone.setCellValueFactory(new PropertyValueFactory<>("phone"));
        etoile.setCellValueFactory(new PropertyValueFactory<>("etoile"));
        note.setCellValueFactory(new PropertyValueFactory<>("note"));
        prix.setCellValueFactory(new PropertyValueFactory<>("prix"));
        categorie.setCellValueFactory(new PropertyValueFactory<>("categorie"));
        ///////
          btnsupprimer .setOnMouseClicked(x -> {
 Bonplan cat = new Bonplan();
cat = table.getSelectionModel().getSelectedItem();

        if (cat== null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Alerte");
            alert.setHeaderText("Alerte");
            alert.setContentText("Il faut tout d'abord sélectionner un bon plan");
            alert.show();
        } else {

            // get Selected Item
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Etes vous sure de vouloir supprimer ce bon plan?", ButtonType.YES, ButtonType.NO, null);
            alert.showAndWait();

            if (alert.getResult() == ButtonType.YES) {
                //remove selected item from the table list
                bonplanService.SupprimerBonPlan(cat);
                //bonplanService.SupprimerCategorie(cat);
                 table.getItems().clear();
        
            table.getItems().addAll(bonplanService.AfficherBonPlan());
            ChargerBonPlan();
            }
        }
        });
        ////
        
        ///////// hide modif form
        titremodif.setVisible(false);
        titreadresse.setVisible(false);
        titredescription.setVisible(false);
        titreetoile.setVisible(false);
        titrename.setVisible(false);
        titrenote.setVisible(false);
        titrephone.setVisible(false);
        titreprix.setVisible(false);
        validerModif.setVisible(false);
        annulerModif.setVisible(false);
        
        adresseD.setVisible(false);
        descriptionD.setVisible(false);
        etoileD.setVisible(false);
        nameD.setVisible(false);
        noteD.setVisible(false);
        phoneD.setVisible(false);
        prixD.setVisible(false);
        
        ///////////
    }    

    @FXML
    public void retour (ActionEvent event){
        loadView("../GUI/ListBonPlan.fxml");

    }
    @FXML
    private void tableclick(MouseEvent event) {
    }
    
    /////////////////
    /*
        private void SearchBonPlanName() throws SQLException {
        BonPlanService bs = new BonPlanService();
        ArrayList AL = (ArrayList) bs.AfficherBonPlan();
        ObservableList OReservation = FXCollections.observableArrayList(AL);
        FilteredList<Bonplan> filtred_c = new FilteredList<>(OReservation, e -> true);
        search.setOnKeyReleased(e -> {
            search.textProperty().addListener((observableValue, oldValue, newValue) -> {
                filtred_c.setPredicate((Predicate<? super Bonplan>) cat -> {
                    if (newValue == null || newValue.isEmpty()) {
                        return true;
                    }
                    String toLowerCaseNewValue = newValue.toLowerCase();
                    if ((cat.getName().toLowerCase().contains(toLowerCaseNewValue)) ) {
                        return true;

                    }

                    return false;
                });
            });
        });
        table.setItems(filtred_c);
    }
    */
    
    ////
    
    public void ChargerBonPlan() {
        
        BonPlanService BonPlanService = new BonPlanService();
        ArrayList<Bonplan> listeBonPlan = BonPlanService.listBonPlanPropreUser();

        ObservableList observableList = FXCollections.observableArrayList(listeBonPlan);
        table.setItems(observableList);

    }
    /////////////////////
     private Object loadView(String path)
    {
        FXMLLoader fxmlLoader= new FXMLLoader(getClass().getResource((path)));
        AnchorPane parentContent = null;
        try {
            parentContent = fxmlLoader.load();
        } catch (IOException ex) {
            Logger.getLogger(LayoutFrontController.class.getName()).log(Level.SEVERE, null, ex);
        }
        setNode(parentContent);
        System.gc();
        return fxmlLoader.getController();
    }
     
      private void setNode(Node node) {
        holderPane.getChildren().clear();
        holderPane.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.millis(1500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
    }
      /////////////////////////

    @FXML
    private void showModif(ActionEvent event) {
          Bonplan bonplan= new Bonplan();

        bonplan = table.getSelectionModel().getSelectedItem();

        if (bonplan == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Alerte");
            alert.setHeaderText("Alerte");
            alert.setContentText("Il faut tout d'abord sélectionner un bon plan parmi la liste de vos bons plans");
            alert.show();
        } else {

            /////////show modif form
        titremodif.setVisible(true);
        titreadresse.setVisible(true);
        titredescription.setVisible(true);
        titreetoile.setVisible(true);
        titrename.setVisible(true);
        titrenote.setVisible(true);
        titrephone.setVisible(true);
        titreprix.setVisible(true);
        validerModif.setVisible(true);
        annulerModif.setVisible(true);
        ///
        adresseD.setVisible(true);
        descriptionD.setVisible(true);
        etoileD.setVisible(true);
        nameD.setVisible(true);
        noteD.setVisible(true);
        phoneD.setVisible(true);
        prixD.setVisible(true);
        ///////////
        adresseD.setText(bonplan.getAdresse());
        descriptionD.setText(bonplan.getDescription());
        etoileD.setRating(bonplan.getEtoile());
       // etoileD.setText(Integer.toString(bonplan.getEtoile()));
        nameD.setText(bonplan.getName());
        noteD.setText(Integer.toString(bonplan.getNote()));
        phoneD.setText(bonplan.getPhone());
        prixD.setText(Double.toString(bonplan.getPrix()));
       
            

        }
    }

    @FXML
    private void modifMonPropreBonPlan(ActionEvent event) {
        Bonplan bonplan = new Bonplan();
        bonplan = table.getSelectionModel().getSelectedItem();
        
        bonplan.setAdresse(adresseD.getText());
        bonplan.setDescription(descriptionD.getText());
        bonplan.setEtoile((int) etoileD.getRating());
        bonplan.setPrix(Double.parseDouble(prixD.getText()));
        bonplan.setPhone(phoneD.getText());
        bonplan.setNote(Integer.parseInt(noteD.getText()));
        bonplan.setName(nameD.getText());
        
        bonplanService.ModifierBonPlan(bonplan, bonplan.getId());
        table.getItems().clear();
        
            table.getItems().addAll(bonplanService.listBonPlanPropreUser());
   
            ChargerBonPlan();

         ///////// hide modif form
        titremodif.setVisible(false);
        titreadresse.setVisible(false);
        titredescription.setVisible(false);
        titreetoile.setVisible(false);
        titrename.setVisible(false);
        titrenote.setVisible(false);
        titrephone.setVisible(false);
        titreprix.setVisible(false);
        validerModif.setVisible(false);
        annulerModif.setVisible(false);
        
         adresseD.setVisible(false);
        descriptionD.setVisible(false);
        etoileD.setVisible(false);
        nameD.setVisible(false);
        noteD.setVisible(false);
        phoneD.setVisible(false);
        prixD.setVisible(false);
        ///////////
       
        
        

    }

    @FXML
    private void annulermodif(ActionEvent event) {
        ///////// hide modif form
        titremodif.setVisible(false);
        titreadresse.setVisible(false);
        titredescription.setVisible(false);
        titreetoile.setVisible(false);
        titrename.setVisible(false);
        titrenote.setVisible(false);
        titrephone.setVisible(false);
        titreprix.setVisible(false);
        validerModif.setVisible(false);
        annulerModif.setVisible(false);
        
         adresseD.setVisible(false);
        descriptionD.setVisible(false);
        etoileD.setVisible(false);
        nameD.setVisible(false);
        noteD.setVisible(false);
        phoneD.setVisible(false);
        prixD.setVisible(false);
        ///////////
        
    }

  /*  @FXML
    private void supprimerMonBonPlan(ActionEvent event) {
        Bonplan bonplan= new Bonplan();
        bonplan = table.getSelectionModel().getSelectedItem();
        if (bonplan == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Suppression");
            alert.setContentText("Sélectionner un bon plan parmi les uns qui se trouvent dans votre liste");
            alert.show();

        } else {
            bonplanService.SupprimerBonPlan(bonplan);

            table.getItems().clear();
        
            table.getItems().addAll(bonplanService.AfficherBonPlan());
   
            ChargerBonPlan();

        }
    }*/
}
