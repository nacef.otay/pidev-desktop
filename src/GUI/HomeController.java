/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author OTAY Nacef
 */
public class HomeController implements Initializable {

    @FXML
    private AnchorPane holderPane;
    @FXML
    private JFXButton btnProfile;
    @FXML
    private JFXButton btnEvenement;
    @FXML
    private JFXButton btnGroupes;
    @FXML
    private JFXButton btnhotes;
    @FXML
    private JFXButton btnbonplans;
    @FXML
    private JFXButton btnShop;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    


    private void setNode(Node node) {
        holderPane.getChildren().clear();
        holderPane.getChildren().add((Node) node);

        FadeTransition ft = new FadeTransition(Duration.millis(1500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
    }

    private void switchHome(ActionEvent event) throws IOException {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(("../GUI/Home.fxml")));
        AnchorPane parentContent = fxmlLoader.load();
        holderPane.getChildren().clear();
        holderPane.getChildren().setAll(parentContent);
        System.out.println("dfsfsdf");

    }
    
    @FXML
    private void switchProfile(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(("../GUI/UserAdmin.fxml")));
        AnchorPane parentContent = fxmlLoader.load();
        holderPane.getChildren().clear();
        holderPane.getChildren().setAll(parentContent);
    
    }

    @FXML
    private void switchShop(ActionEvent event) {
    }

    @FXML
    private void switchGroupes(ActionEvent event) throws IOException {
<<<<<<< src/GUI/HomeController.java
           FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(("../GUI/GroupAdmin.fxml")));
        AnchorPane parentContent = fxmlLoader.load();
        holderPane.getChildren().clear();
        holderPane.getChildren().setAll(parentContent);
    
=======
         FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(("../GUI/GroupAdmin.fxml")));
        AnchorPane parentContent = fxmlLoader.load();
        holderPane.getChildren().clear();
        holderPane.getChildren().setAll(parentContent);
>>>>>>> src/GUI/HomeController.java
    }

    @FXML
    private void switchBonPlans(ActionEvent event) throws IOException {
         FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(("../GUI/AdminChoixBP.fxml")));
        AnchorPane parentContent = fxmlLoader.load();
        holderPane.getChildren().clear();
        holderPane.getChildren().setAll(parentContent);
    }

    @FXML
    private void switchEvent(ActionEvent event) {
    }

    @FXML
    private void switchHotes(ActionEvent event) throws IOException {
         FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(("../GUI/AdminHote.fxml")));
        AnchorPane parentContent = fxmlLoader.load();
        holderPane.getChildren().clear();
        holderPane.getChildren().setAll(parentContent);
    }
    
}
