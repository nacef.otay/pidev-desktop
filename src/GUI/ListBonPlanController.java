/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import APIs.sendSMS;
import static Core.Controller.getUserId;

import static Core.Controller.holderPane;
import Core.LayoutFrontController;
import Entities.Bonplan;
import Entities.CommentaireBonPlan;
import Entities.User;
import Service.BonPlanService;
import Service.CommentaireBonPlanService;
import Service.UserService;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.Duration;
import org.controlsfx.control.Rating;

/**
 * FXML Controller class
 *
 * @author marie
 */
public class ListBonPlanController implements Initializable {
    
    
   @FXML
    private ListView<Bonplan> listView;
    ObservableList<Bonplan> mmbonplan= FXCollections.observableArrayList();
    BonPlanService servicebon = new BonPlanService();
    
    CommentaireBonPlanService serviceComment = new CommentaireBonPlanService();
    ObservableList<CommentaireBonPlan> mmcomment = FXCollections.observableArrayList();

    UserService us = new UserService();
    
    @FXML
    private Button btnajouter;
    @FXML
    private Label titredetails;
    @FXML
    private Label titreprix;
    @FXML
    private Label titreadresse;
    @FXML
    private Label titrenote;
    @FXML
    private Label titrephone;
    @FXML
    private Label prixD;
    @FXML
    private Label adresseD;
    @FXML
    private Label noteD;
    @FXML
    private Label phoneD;
    @FXML
    private Button masquerdetail;
    @FXML
    private Button btngestion;
    @FXML
    private TextField searchtext;
    @FXML
    private Button btnSearch;
    
    
    @FXML
    private Label titreComment;
    @FXML
    private TextField textComment;
    @FXML
    private Button btnComment;
    @FXML
    private Button btnAnnulerComment;
    @FXML
    private Label id_bp;
    @FXML
    private ListView<CommentaireBonPlan> listViewComment;
    @FXML
    private ImageView imgviewcoment;
    @FXML
    private Label sender;
    @FXML
    private Label number;
    @FXML
    private Label mess;
    @FXML
    private TextField txtsender;
    @FXML
    private TextField txtnumber;
    @FXML
    private TextField txtmess;
    @FXML
    private Button btnenvoi;
    @FXML
    private Button btnenvoiAnnuler;
    @FXML
    private Button btnstat;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
               // ChargerBonPlan();
               titreadresse.setVisible(false);
               titredetails.setVisible(false);
               titrenote.setVisible(false);
               titrephone.setVisible(false);
               titreprix.setVisible(false);
               
               adresseD.setVisible(false);
               noteD.setVisible(false);
               phoneD.setVisible(false);
               prixD.setVisible(false);
               masquerdetail.setVisible(false);
               ///////
               
               ///////hide form commentaire
               titreComment.setVisible(false);
               textComment.setVisible(false);
               btnAnnulerComment.setVisible(false);
               btnComment.setVisible(false);
               id_bp.setVisible(false);
               listViewComment.setVisible(false);
               imgviewcoment.setVisible(false);
               
               ////
               
               ////hide sms 
               sender.setVisible(false);
               number.setVisible(false);
               mess.setVisible(false);
               txtmess.setVisible(false);
               txtnumber.setVisible(false);
               txtsender.setVisible(false);
               btnenvoiAnnuler.setVisible(false);
               btnenvoi.setVisible(false);
               /////
                
                  mmbonplan = (ObservableList) servicebon.list();
        listView.setItems(mmbonplan);
        listView.getSelectionModel().clearSelection();
        
        listView.setCellFactory(new Callback<ListView<Bonplan>, ListCell<Bonplan>>() {
            @Override
            public ListCell<Bonplan> call(ListView<Bonplan> param) {
                ListCell<Bonplan> cell;
                cell = new ListCell<Bonplan>() {
                    protected void updateItem(Bonplan item, boolean empty) {
                        if (item != null) {

                            HBox hbox = new HBox();
                            hbox.setSpacing(50);
                            hbox.setAlignment(Pos.CENTER);

                            VBox vbox = new VBox();
                            vbox.setSpacing(2.5);
                            vbox.setAlignment(Pos.CENTER);

                            VBox Buttons = new VBox();
                            Buttons.setSpacing(2.5);
                            Buttons.setAlignment(Pos.CENTER);

                            Image img;
                            img = new Image("file:" + item.getImage());
                            ImageView imgView = new ImageView(img);

                            //**************************ImgView Resize*****************
                            imgView.setFitWidth(150);
                            imgView.setFitHeight(150);
                            imgView.setPreserveRatio(false);
                            imgView.setSmooth(true);
                            imgView.setCache(true);
                            //***********************************************************

                            Button detail = new Button("Détails");
                            Button comment = new Button("Commenter");
                            Button allcomments = new Button("Afficher Commentaire");
                            Button envoi = new Button("Envoyer sms");
 detail.setStyle("-fx-background-color: #00BA95; -fx-padding:3px;-fx-pref-height: 20px;-fx-pref-width:125px;");
                           
 comment.setStyle("-fx-background-color: #00BA95; -fx-padding:3px;-fx-pref-height: 20px;-fx-pref-width:125px;");
 allcomments.setStyle("-fx-background-color: #00BA95; -fx-padding:3px;-fx-pref-height: 20px;-fx-pref-width:125px;");
 envoi.setStyle("-fx-background-color: #00BA95; -fx-padding:3px;-fx-pref-height: 20px;-fx-pref-width:125px;");
                          
                            
                         /////////////******all comments btn////******
                            
                           allcomments.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<Event>()  {
                                @Override
                                public void handle(Event event) {
                                    listView.setVisible(false);
                                    // ChargerBonPlan();
               id_bp.setVisible(false);
               titreadresse.setVisible(false);
               titredetails.setVisible(false);
               titrenote.setVisible(false);
               titrephone.setVisible(false);
               titreprix.setVisible(false);
               
               adresseD.setVisible(false);
               noteD.setVisible(false);
               phoneD.setVisible(false);
               prixD.setVisible(false);
               masquerdetail.setVisible(false);
               ///////
               
               ///////hide form commentaire
               titreComment.setVisible(false);
               textComment.setVisible(false);
               btnAnnulerComment.setVisible(false);
               btnComment.setVisible(false);
               
               listViewComment.setVisible(false);
               imgviewcoment.setVisible(false);
               
               ////
               
               ////hide sms 
               sender.setVisible(false);
               number.setVisible(false);
               mess.setVisible(false);
               txtmess.setVisible(false);
               txtnumber.setVisible(false);
               txtsender.setVisible(false);
               btnenvoiAnnuler.setVisible(false);
               btnenvoi.setVisible(false);
               /////
                                    
                                    listViewComment.setVisible(true);
                                    
                                     mmcomment = (ObservableList) serviceComment.listbpcomment(item.getId());
                                     listViewComment.setItems(mmcomment);
                                     listViewComment.getSelectionModel().clearSelection();
        
                                    ////
        listViewComment.setCellFactory(new Callback<ListView<CommentaireBonPlan>, ListCell<CommentaireBonPlan>>() {
            @Override
            public ListCell<CommentaireBonPlan> call(ListView<CommentaireBonPlan> param) {
                ListCell<CommentaireBonPlan> cellc;
                cellc = new ListCell<CommentaireBonPlan>() {
                    protected void updateItem(CommentaireBonPlan itemc, boolean empty) {
                        if (item != null) {

                            HBox hbox = new HBox();
                            hbox.setSpacing(50);
                            hbox.setAlignment(Pos.CENTER);

                            VBox vbox = new VBox();
                            vbox.setSpacing(2.5);
                            vbox.setAlignment(Pos.CENTER);

                            VBox Buttons = new VBox();
                            Buttons.setSpacing(2.5);
                            Buttons.setAlignment(Pos.CENTER);

                            Image img;
                            img = new Image("file:" + item.getImage());
                            ImageView imgView = new ImageView(img);
                            imgviewcoment=imgView;
                            imgviewcoment.setVisible(true);
                           
                            /////////////////////////////////////////////////////
                           
//                           Label nombonplan = new Label(itemc.getContenu());
                            
                            //vbox.getChildren().add(nombonplan);
                            //vbox.getChildren().add(itemc.setContenu(contenu));
                            hbox.getChildren().add(vbox);
                            
                            setGraphic(hbox);

                        }

                    }

                };
                return cellc;
            }

        });
     
                                }
                               
                           }); 
                           
                            ///***********envoi btn ************
                            envoi.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<Event>()  {
                                @Override
                                public void handle(Event event) {
////hide sms 
               sender.setVisible(true);
               number.setVisible(true);
               mess.setVisible(true);
               txtmess.setVisible(true);
               txtnumber.setVisible(true);
               txtsender.setVisible(true);
               btnenvoiAnnuler.setVisible(true);
               btnenvoi.setVisible(true);
                txtsender.setText(us.getUserById(getUserId()).getNom());
               txtnumber.setText(item.getPhone());
               /////
              }
           });
                            
                            
                            
 ///////////////////**************
                            //*****************************details btn click******************************
                            detail.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<Event>()  {
                                @Override
                                public void handle(Event event) {
                                     System.out.println("clicked");
                                    titredetails.setVisible(true);
                                    titreadresse.setVisible(true);
                                    titrenote.setVisible(true);
                                    titrephone.setVisible(true);
                                    titreprix.setVisible(true);
                                    
                                    adresseD.setVisible(true);
                                    adresseD.setText(item.getAdresse());
                                    noteD.setVisible(true);
                                    noteD.setText(Integer.toString(item.getNote()));
                                    phoneD.setVisible(true);
                                    phoneD.setText(item.getPhone());
                                    prixD.setVisible(true);
                                    prixD.setText(Double.toString(item.getPrix()));
                                    
                                    /////////////*****
                                   
                                    
                                    ///////****************masquer btn
                                    masquerdetail.setVisible(true);
                                    masquerdetail.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<Event>(){
                                         @Override
                                         public void handle(Event event) {
                                             titreadresse.setVisible(false);
                                        titredetails.setVisible(false);
                                         titrenote.setVisible(false);
                                         titrephone.setVisible(false);
                                    titreprix.setVisible(false);
               
                                   adresseD.setVisible(false);
                                     noteD.setVisible(false);
                                    phoneD.setVisible(false);
                                    prixD.setVisible(false);
                                    masquerdetail.setVisible(false);
                                         }
                                        
                                    });
                                    /////////////////*****************
                                    
                                }
                                
                            } );

                            ///////////////////////////////////////////////////////////////////////////// 
                             //////*********comment btn
                                      comment.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<Event>(){
                                         @Override
                                         public void handle(Event event) {
          System.out.println("commment ***********"+ item.getName());
                                             titreComment.setVisible(true);
                                             textComment.setVisible(true);
                                             btnAnnulerComment.setVisible(true);
                                             btnComment.setVisible(true);
                                             id_bp.setText(Integer.toString(item.getId()));
                                             
                    

                                         }
                                          
                                      });
                                    
                      
                            /////////////////////////////////////////////////////
                            Label nombonplan = new Label(item.getName());
                            Label categoriebonplan = new Label(item.getCategorie());
                            Label datebonplan = new Label(""+item.getDatePublication());
                            
                            vbox.getChildren().add(nombonplan);
                            vbox.getChildren().add(categoriebonplan);
                            vbox.getChildren().add(datebonplan);
                            
                            Buttons.getChildren().add(detail);
                            Buttons.getChildren().add(comment);
                            Buttons.getChildren().add(allcomments);
                            Buttons.getChildren().add(envoi);
                            hbox.getChildren().add(imgView);
                            hbox.getChildren().add(vbox);
                            hbox.getChildren().add(Buttons);

                            setGraphic(hbox);

                        }

                    }

                };
                return cell;
            }

        });
        }    

   
     @FXML
    private void ajouterBonPlan(ActionEvent event) {

         loadView("../GUI/AjoutBonPlan.fxml");

    }
    
    @FXML
    private void propreBonPlan(ActionEvent event){
        loadView("../GUI/UserBonPlan.fxml");
    }
    

    
    ///////////////
     private void setNode(Node node) {
        holderPane.getChildren().clear();
        holderPane.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.millis(1500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
    }
    
    private Object loadView(String path)
    {
        FXMLLoader fxmlLoader= new FXMLLoader(getClass().getResource((path)));
        AnchorPane parentContent = null;
        try {
            parentContent = fxmlLoader.load();
        } catch (IOException ex) {
            Logger.getLogger(LayoutFrontController.class.getName()).log(Level.SEVERE, null, ex);
        }
        setNode(parentContent);
        System.gc();
        return fxmlLoader.getController();
    }
//////////////////

    @FXML
    private void chercherBonPlanName(ActionEvent event) {
         mmbonplan.clear();
        mmbonplan.addAll((ObservableList)servicebon.SearchBonPlanName(searchtext.getText()));
        listView.setItems(mmbonplan);
        
        
        /** appel ll liste view pour l affichage **/
         
        listView.setCellFactory(new Callback<ListView<Bonplan>, ListCell<Bonplan>>() {
            @Override
            public ListCell<Bonplan> call(ListView<Bonplan> param) {
                ListCell<Bonplan> cell ;
                cell = new ListCell<Bonplan>() {
                    protected void updateItem(Bonplan item, boolean empty) {
                        if (item != null) {

                            HBox hbox = new HBox();
                            hbox.setSpacing(100);
                            hbox.setAlignment(Pos.CENTER);

                            VBox vbox = new VBox();
                            vbox.setSpacing(2.5);
                            vbox.setAlignment(Pos.CENTER);

                            VBox Buttons = new VBox();
                            Buttons.setSpacing(2.5);
                            Buttons.setAlignment(Pos.CENTER);

                            Image img;
                            img = new Image("file:" + item.getImage());
                            ImageView imgView = new ImageView(img);

                            //**************************ImgView Resize*****************
                            imgView.setFitWidth(150);
                            imgView.setFitHeight(150);
                            imgView.setPreserveRatio(false);
                            imgView.setSmooth(true);
                            imgView.setCache(true);
                            //***********************************************************
                            Button detail = new Button("Détails");

                            //*****************************Details_button_OnClick******************************
                            detail.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<Event>() {
                                @Override
                                public void handle(Event event) {

                                   titredetails.setVisible(true);
                                    titreadresse.setVisible(true);
                                    titrenote.setVisible(true);
                                    titrephone.setVisible(true);
                                    titreprix.setVisible(true);
                                    
                                    adresseD.setVisible(true);
                                    adresseD.setText(item.getAdresse());
                                    noteD.setVisible(true);
                                    noteD.setText(Integer.toString(item.getNote()));
                                    phoneD.setVisible(true);
                                    phoneD.setText(item.getPhone());
                                    prixD.setVisible(true);
                                    prixD.setText(Double.toString(item.getPrix()));
                                    
                                    masquerdetail.setVisible(true);
                                    

                                }

                            });
                            detail.setMaxWidth(120);
                            masquerdetail.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<Event>(){
                                         @Override
                                         public void handle(Event event) {
                                             titreadresse.setVisible(false);
               titredetails.setVisible(false);
               titrenote.setVisible(false);
               titrephone.setVisible(false);
               titreprix.setVisible(false);
               
               adresseD.setVisible(false);
               noteD.setVisible(false);
               phoneD.setVisible(false);
               prixD.setVisible(false);
               masquerdetail.setVisible(false);
                                         }
                                        
                                    });

                            ///////////////////////////////////////////////////////////////////////////// 
                            //*****************************Details_button_Reserver******************************
                          
                            /////////////////////////////////////////////////////
                            Label nombonplan = new Label(item.getName());
                            Label categoriebonplan = new Label(item.getCategorie());
                            Label datebonplan = new Label(""+item.getDatePublication());
                            vbox.getChildren().add(nombonplan);
                            vbox.getChildren().add(categoriebonplan);
                            vbox.getChildren().add(datebonplan);
                            Buttons.getChildren().add(detail);
                            //Buttons.getChildren().add(Reserver);
                            hbox.getChildren().add(imgView);
                            hbox.getChildren().add(vbox);
                            hbox.getChildren().add(Buttons);

                            setGraphic(hbox);

                        }

                    }

                };
                return cell;
            }

        });
    }

    @FXML
    private void DeposerComment(ActionEvent event) {
        if (textComment.getText().isEmpty()
                ) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Erreur");
            alert.setHeaderText(null);
            alert.setContentText("Il faut remplir tout les champs");
            alert.showAndWait();
        }
        else 
        {
            String contenucomm = textComment.getText();
            Bonplan bp = new Bonplan();
            bp.setId(Integer.parseInt(id_bp.getText()));
             CommentaireBonPlan commentbp = new CommentaireBonPlan(bp);
              commentbp.setContenu(contenucomm);
             System.out.println("******************");
           
            CommentaireBonPlanService commentService= new CommentaireBonPlanService();
         
            commentService.ajouterCommentaire(commentbp);
            
             Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Commentaire");
            alert.setHeaderText(null);
            alert.setContentText("Vous avez commenter ");
            alert.showAndWait();
              titreComment.setVisible(false);
               textComment.setVisible(false);
               btnAnnulerComment.setVisible(false);
               btnComment.setVisible(false);
               id_bp.setVisible(false);
        }
    }

    @FXML
    private void AnnulerComment(ActionEvent event) {
               titreComment.setVisible(false);
               textComment.setVisible(false);
               btnAnnulerComment.setVisible(false);
               btnComment.setVisible(false);
               id_bp.setVisible(false);

    }

 
        @FXML
    private void EnvoyerSMS(ActionEvent event) {
        
               sendSMS sm = new sendSMS();
               sm.sendSms(txtnumber.getText(),txtmess.getText(),txtsender.getText());
               Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Information");
                alert.setHeaderText("Envoi d'SMS ");
                alert.setContentText("votre SMS est envoyé avec succès");
                alert.showAndWait();
                sender.setVisible(false);
               number.setVisible(false);
               mess.setVisible(false);
               txtmess.setVisible(false);
               txtnumber.setVisible(false);
               txtsender.setVisible(false);
               btnenvoiAnnuler.setVisible(false);
               btnenvoi.setVisible(false);
    }
    
    @FXML
     public void AnnulerEnvoiSMS(ActionEvent event) {
               sender.setVisible(false);
               number.setVisible(false);
               mess.setVisible(false);
               txtmess.setVisible(false);
               txtnumber.setVisible(false);
               txtsender.setVisible(false);
               btnenvoiAnnuler.setVisible(false);
               btnenvoi.setVisible(false);
                                         }
    @FXML
    private void statistiqueBonPlan(ActionEvent event) {

         loadView("../GUI/StatistiqueBonPlan.fxml");

    }                            
             @FXML
    private void BonPlanNote(ActionEvent event) {

         loadView("../GUI/BonsPlansPlusNotes.fxml");

    }                        
    }

