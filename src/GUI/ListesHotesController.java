/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import static Core.Controller.holderPane;
import Entities.Maisons_hotes;
import Service.MaisonHoteService;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import tray.animations.AnimationType;
import static tray.notification.NotificationType.SUCCESS;
import tray.notification.TrayNotification;
import utils.DataSource;
import GUI.FXMLDocumentController;
import java.util.Optional;
import java.util.function.Predicate;
import javafx.animation.FadeTransition;
import javafx.collections.transformation.FilteredList;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Bhs Nada
 */
public class ListesHotesController implements Initializable {

    @FXML
    private TableColumn<Maisons_hotes, String> nom;
    @FXML
    private TableColumn<Maisons_hotes, String> pays;

    @FXML
    private TableView<Maisons_hotes> table_view;

    @FXML
    private TableColumn<Maisons_hotes, String> gouvernorat;
    @FXML
    private TableColumn<Maisons_hotes, String> capacites;
    @FXML
    private TableColumn<Maisons_hotes, String> prix;
    @FXML
    private Button modifier;
    @FXML
    private Button supprimer;
    @FXML
    private Button ajouter;

    MaisonHoteService maisonHoteService = new MaisonHoteService();
    Maisons_hotes maison = new Maisons_hotes();
    @FXML
    private TextArea description;
    @FXML
    private Button annulerBtn;
    @FXML
    private Button ModifierBtn;
    @FXML
    private TextField nom1;
    @FXML
    private TextField mail;
    @FXML
    private TextField site;
    @FXML
    private TextField tel;
    @FXML
    private TextField gouvernorat1;
    @FXML
    private TextField capacites1;
    @FXML
    private TextField prix1;
    @FXML
    private TextField Adresse;
    @FXML
    private Text titre_modif;
    @FXML
    private Button Affiche;
    @FXML
    private Label m_nom;
    @FXML
    private Label m_mail;
    @FXML
    private Label m_tel;
    @FXML
    private Label m_gouv;
    @FXML
    private Label m_prix;
    @FXML
    private Label m_site;
    @FXML
    private Label m_cap;
    @FXML
    private Label m_adr;
    @FXML
    private Label m_des;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        /*Connection c=DataSource.getInstance().getConnection();
        try {
            ResultSet rs=c.createStatement().executeQuery("select * from maisons_hotes");
            while (rs.next()){
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListesHotesController.class.getName()).log(Level.SEVERE, null, ex);
        }*/

        loadHotes();

        /**
         * ******************** Modifier form
         */
        nom1.setVisible(false);
        mail.setVisible(false);
        site.setVisible(false);
        tel.setVisible(false);
        gouvernorat1.setVisible(false);
        capacites1.setVisible(false);
        prix1.setVisible(false);
        Adresse.setVisible(false);
        annulerBtn.setVisible(false);
        titre_modif.setVisible(false);
        ModifierBtn.setVisible(false);
        description.setVisible(false);
        /**
         * ******************
         */

        m_nom.setVisible(false);
        m_tel.setVisible(false);
        m_gouv.setVisible(false);
        m_adr.setVisible(false);
        m_cap.setVisible(false);
        m_prix.setVisible(false);
        m_des.setVisible(false);
        m_site.setVisible(false);
        m_mail.setVisible(false);
////////////////////////////////////
        nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        pays.setCellValueFactory(new PropertyValueFactory<>("pays"));
        gouvernorat.setCellValueFactory(new PropertyValueFactory<>("gouvernorat"));
        capacites.setCellValueFactory(new PropertyValueFactory<>("capacites"));
        prix.setCellValueFactory(new PropertyValueFactory<>("prix"));

    
    
    
    }

    public void loadHotes() {
        MaisonHoteService maisonHoteService = new MaisonHoteService();
        ArrayList<Maisons_hotes> listeHote = maisonHoteService.AfficherMaisonsHotes();

        ObservableList observableList = FXCollections.observableArrayList(listeHote);
        table_view.setItems(observableList);

    }

    private void setNode(Node node) {
        holderPane.getChildren().clear();
        holderPane.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.millis(1500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
    }

    private Object loadView(String path) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource((path)));
        AnchorPane parentContent = null;
        try {
            parentContent = fxmlLoader.load();
        } catch (IOException ex) {
            Logger.getLogger(ListesHotesController.class.getName()).log(Level.SEVERE, null, ex);
        }
        setNode(parentContent);
        System.gc();
        return fxmlLoader.getController();
    }

    @FXML
    private void ajouter(ActionEvent event) {
        loadView("/GUI/AddHote.fxml");

    }

    @FXML
    private void modifier(ActionEvent event) {

        Maisons_hotes maisons_hotes = new Maisons_hotes();

        maisons_hotes = table_view.getSelectionModel().getSelectedItem();

        if (maisons_hotes == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Alerte");
            alert.setHeaderText("Alerte");
            alert.setContentText("Veillez Choisir une maison d'hôte");
            alert.show();
        } else {

            System.out.println(maisons_hotes);
            /**
             * ********************
             */
            nom1.setVisible(true);
            mail.setVisible(true);
            site.setVisible(true);
            tel.setVisible(true);
            gouvernorat1.setVisible(true);
            capacites1.setVisible(true);
            prix1.setVisible(true);
            Adresse.setVisible(true);
            annulerBtn.setVisible(true);
            titre_modif.setVisible(true);
            ModifierBtn.setVisible(true);
            description.setVisible(true);
            /**
             * ******************
             */
            
        m_nom.setVisible(true);
        m_tel.setVisible(true);
        m_gouv.setVisible(true);
        m_adr.setVisible(true);
        m_cap.setVisible(true);
        m_prix.setVisible(true);
        m_des.setVisible(true);
        m_site.setVisible(true);
        m_mail.setVisible(true);
            

            nom1.setText(maisons_hotes.getNom());
            mail.setText(maisons_hotes.getMail());
            site.setText(maisons_hotes.getSite_web());
            tel.setText(Integer.toString(maisons_hotes.getTel()));
            capacites1.setText(Integer.toString(maisons_hotes.getCapacites()));
            prix1.setText(Double.toString(maisons_hotes.getPrix()));
            gouvernorat1.setText(maisons_hotes.getGouvernorat());
            Adresse.setText(maisons_hotes.getAdresse());
            description.setText(maisons_hotes.getDescription());

        }

    }

    @FXML
    private void supprimer(ActionEvent event) {
        Maisons_hotes maisons_hotes = new Maisons_hotes();
        maisons_hotes = table_view.getSelectionModel().getSelectedItem();
        if (maisons_hotes == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Alerte");
            alert.setHeaderText("Alerte");
            alert.setContentText("Veillez Choisir une Maison d'hôte a supprimer");
            alert.show();

        } else {
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Confirmation ");
            alert.setHeaderText(null);
            alert.setContentText("vous êtes sûr de supprimer la maison ?");
            Optional<ButtonType> action = alert.showAndWait();

            if (action.get() == ButtonType.OK) {
                maisonHoteService.SupprimerHote(maisons_hotes);

                TrayNotification tray = new TrayNotification("Avec succès", "Maison d'hôte  N° " + maisons_hotes.getId() + " a été Supprimer avec succés !", SUCCESS);
                tray.setAnimationType(AnimationType.POPUP);
                tray.showAndDismiss(Duration.seconds(10));

                loadHotes();
            }

        }
    }

    @FXML
    private void AnnulerHôte(ActionEvent event) {

        /**
         * ********************
         */
        nom1.setVisible(false);
        mail.setVisible(false);
        site.setVisible(false);
        tel.setVisible(false);
        gouvernorat1.setVisible(false);
        capacites1.setVisible(false);
        prix1.setVisible(false);
        Adresse.setVisible(false);
        annulerBtn.setVisible(false);
        titre_modif.setVisible(false);
        ModifierBtn.setVisible(false);
        description.setVisible(false);
        /**
         * ******************
         */
        
        
         m_nom.setVisible(false);
        m_tel.setVisible(false);
        m_gouv.setVisible(false);
        m_adr.setVisible(false);
        m_cap.setVisible(false);
        m_prix.setVisible(false);
        m_des.setVisible(false);
        m_site.setVisible(false);
        m_mail.setVisible(false);
        
    }

    @FXML
    private void ModifierHôte(ActionEvent event) {

         Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Confirmation Dialog");
            alert.setHeaderText(null);
            alert.setContentText("vous êtes sûr de modifier la maison?");
            Optional<ButtonType> action = alert.showAndWait();

            if (action.get() == ButtonType.OK) {
        Maisons_hotes maisons_hotes = new Maisons_hotes();
        maisons_hotes = table_view.getSelectionModel().getSelectedItem();
        MaisonHoteService maisonService = new MaisonHoteService();

        maisons_hotes.setNom(nom1.getText());
        maisons_hotes.setMail(mail.getText());
        maisons_hotes.setSite_web(site.getText());
        maisons_hotes.setTel(Integer.parseInt(tel.getText()));
        maisons_hotes.setCapacites(Integer.parseInt(capacites1.getText()));
        maisons_hotes.setPrix(Double.parseDouble(prix1.getText()));
        maisons_hotes.setGouvernorat(gouvernorat1.getText());
        maisons_hotes.setAdresse(Adresse.getText());
        maisons_hotes.setDescription(description.getText());

        maisonService.ModifierMaison(maisons_hotes, maisons_hotes.getId());
            
        /**
         * ********************
         */
        nom1.setVisible(false);
        mail.setVisible(false);
        site.setVisible(false);
        tel.setVisible(false);
        gouvernorat1.setVisible(false);
        capacites1.setVisible(false);
        prix1.setVisible(false);
        Adresse.setVisible(false);
        annulerBtn.setVisible(false);
        titre_modif.setVisible(false);
        ModifierBtn.setVisible(false);
        description.setVisible(false);
        /**
         * ******************
         */
        m_nom.setVisible(false);
        m_tel.setVisible(false);
        m_gouv.setVisible(false);
        m_adr.setVisible(false);
        m_cap.setVisible(false);
        m_prix.setVisible(false);
        m_des.setVisible(false);
        m_site.setVisible(false);
        m_mail.setVisible(false);
        
        
        
        table_view.getItems().clear();        
        table_view.getItems().addAll(maisonService.AfficherMaisonsHotes());

        /**
         * ******* notif ******
         */
        TrayNotification tray = new TrayNotification("Avec succès", "Maison N° " + maisons_hotes.getId() + " Modification Effectuée avec Succés !", SUCCESS);
        tray.setAnimationType(AnimationType.POPUP);
        tray.showAndDismiss(Duration.seconds(10));
        /**
         * **********************
         */
            }
    }

    @FXML
    private void Affichage(ActionEvent event) throws IOException {
        loadView("/GUI/maisonhotelist.fxml");
    }
  
}
