/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Duration;
import tray.animations.AnimationType;
import static tray.notification.NotificationType.SUCCESS;
import tray.notification.TrayNotification;
import utils.EnvoyerMail;

/**
 * FXML Controller class
 *
 * @author Bhs Nada
 */
public class MailContactController implements Initializable {

   
    @FXML
    private TextArea contenuMsg;
    @FXML
    private Button envoyerBtn;
    @FXML
    private TextField mailre;
    @FXML
    private TextField objetMail;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void EnvoyerMail(ActionEvent event) {
        
        if (contenuMsg.getText()!= null && !mailre.getText().equals("")){
            //SendMail email= new SendMail();
            EnvoyerMail email = new EnvoyerMail();
            
            email.Send( mailre.getText(), objetMail.getText(),contenuMsg.getText());
            
            /**
             * ******* notif ******
             */
            TrayNotification tray = new TrayNotification("Email Envoyee", " avec Succés !", SUCCESS);
            tray.setAnimationType(AnimationType.POPUP);
            tray.showAndDismiss(Duration.seconds(10));
            /**
             * **********************
             */
        }else{
            
            /**
             * ******* notif ******
             */
            TrayNotification tray = new TrayNotification("??? Attention ???", " Email n'est pas Envoyee !", SUCCESS);
            tray.setAnimationType(AnimationType.POPUP);
            tray.showAndDismiss(Duration.seconds(10));
            /**
             * **********************
             */
        }
        
        /* ((Node) (event.getSource())).getScene().getWindow().hide();
        //Parent root = FXMLLoader.load(getClass().getResource("ListeEventsClient.fxml"));
        Scene scene = new Scene(root);
        Stage stage1 = new Stage();
        stage1.setScene(scene);
        stage1.show();*/
    }
    
}
