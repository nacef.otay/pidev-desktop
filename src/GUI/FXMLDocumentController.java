/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Core.Controller;
import static Core.Controller.holderPane;
import Core.LayoutFrontController;
import Entities.Maisons_hotes;
import Entities.User;
import GUI.ListesHotesController;
import IService.IAlbumService;
import Service.MaisonHoteService;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.animation.FadeTransition;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
import javax.imageio.ImageIO;
import tray.animations.AnimationType;
import static tray.notification.NotificationType.SUCCESS;
import tray.notification.TrayNotification;

/**
 * FXML Controller class
 *
 * @author Bhs Nada
 */
public class FXMLDocumentController  extends Controller implements Initializable {
       
   

    
    

    @FXML
    private TextArea description;
    @FXML
    private Button importerBtn;
    @FXML
    private ImageView image;
    @FXML
    private Button annulerBtn;
    @FXML
    private Button ajouterBtn;
    @FXML
    private TextField nom;
    @FXML
    private ComboBox<String> pays;
    @FXML
    private TextField mail;
    @FXML
    private TextField site;
    @FXML
    private TextField tel;
    @FXML
    private TextField gouvernorat;
    @FXML
    private TextField capacites;
    @FXML
    private TextField prix;
    @FXML
    private TextField Adresse;
      
 String chaine3="";
    @FXML
    private Button btnRetour;
    @FXML
    private Label Nullable_warn;
     
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
        
  
       
         pays.getItems().add("Algérie");
        pays.getItems().add("Allemagne");
        pays.getItems().add("France");
        pays.getItems().add("Turque");
        pays.getItems().add("Tunis");
 
        
                  final FileChooser fileChooser = new FileChooser();
       importerBtn.setOnAction(new EventHandler<ActionEvent>() {
                          @Override
                public void handle(final ActionEvent e) {
                    setExtFilters(fileChooser);
                    File file = fileChooser.showOpenDialog(null);
                    
                    if (file != null) {
                        openNewImageWindow(file);
                    }
                    
            }

            
        
        });

        /**
         * ********** Animation sur les bouttons ***********************
         */
        DropShadow shadow = new DropShadow();
        importerBtn.addEventHandler(MouseEvent.MOUSE_ENTERED, (MouseEvent e) -> {
            importerBtn.setEffect(shadow);
        });
        importerBtn.addEventHandler(MouseEvent.MOUSE_EXITED, (MouseEvent e) -> {
            importerBtn.setEffect(null);
        });
        annulerBtn.addEventHandler(MouseEvent.MOUSE_ENTERED, (MouseEvent e) -> {
            annulerBtn.setEffect(shadow);
        });
        annulerBtn.addEventHandler(MouseEvent.MOUSE_EXITED, (MouseEvent e) -> {
            annulerBtn.setEffect(null);
        });
        ajouterBtn.addEventHandler(MouseEvent.MOUSE_ENTERED, (MouseEvent e) -> {
            ajouterBtn.setEffect(shadow);
        });
        ajouterBtn.addEventHandler(MouseEvent.MOUSE_EXITED, (MouseEvent e) -> {
            ajouterBtn.setEffect(null);
        });
        /**
         * ******************************************************************************
         */
    }    

    @FXML
    private void AnnulerHôte(ActionEvent event) {
        
        
    }
/*
    Ajouter Maison d'hote 
    */
    @FXML
    private void AjouterHôte(ActionEvent event) {
         if (nom.getText().isEmpty() || tel.getText().isEmpty() || capacites.getText().isEmpty()
                || Adresse.getText().isEmpty() || prix.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Erreur");
            alert.setHeaderText(null);
            alert.setContentText("Il faut remplir les champs obligatoires ");
            alert.showAndWait();
        }
        else if (validateEmaill()& validateNumberCapacite()&validatePrix()&validateMobileNo())
        {
          
             
          Maisons_hotes  maison = new Maisons_hotes(nom.getText(),description.getText(),
                    pays.getSelectionModel().getSelectedItem(),gouvernorat.getText(),chaine3,Adresse.getText(),site.getText(),mail.getText()
                    ,Integer.parseInt(capacites.getText()),Integer.parseInt(tel.getText()),Integer.parseInt(prix.getText()));
         // maison.setId_user(connectedUser);
            MaisonHoteService maisonHoteService= new MaisonHoteService();
            maisonHoteService.ajouterHote(maison);
            
           
        /**
         * ******* notif ******
         */
        TrayNotification tray = new TrayNotification("Avec succès", "Maison N° " + maison.getNom()+ " Ajouter avec Succés !", SUCCESS);
        tray.setAnimationType(AnimationType.POPUP);
        tray.showAndDismiss(Duration.seconds(10));
        /**
         * **********************
         */
        }
      
    }
    ///////////////////////////////////////////////////////
     private void openNewImageWindow(File file){
        Stage secondStage = new Stage();
        MenuBar menuBar = new MenuBar();
        Menu menuFile = new Menu("Fichier");
        
        MenuItem menuItem_Save1 = new MenuItem("Enregistrer");

        menuFile.getItems().addAll(menuItem_Save1);
        menuBar.getMenus().addAll(menuFile);
        
        Label name = new Label(file.getName());
        Image imageu = new Image(file.toURI().toString());
        ImageView image2 = new ImageView();
        image.setImage(imageu);
        
        menuItem_Save1.setOnAction(new EventHandler<ActionEvent>(){
         @Override
            public void handle(ActionEvent event) {

               secondStage.close();
            }
        
        });  
          final VBox vbox = new VBox();
        vbox.setAlignment(Pos.CENTER);
        vbox.setSpacing(10);
        vbox.setPadding(new Insets(0, 10, 0, 10));
        vbox.getChildren().addAll(name, image2);
         
        image2.setFitHeight(400);
        image2.setPreserveRatio(true);
        image2.setImage(imageu);
        image2.setSmooth(true);
        image2.setCache(true);
         
        Scene scene = new Scene(new VBox(),500, 500);
        ((VBox)scene.getRoot()).getChildren().addAll(menuBar, vbox);
         
        secondStage.setTitle(file.getName());
        secondStage.setScene(scene);
        secondStage.show();
         File source = new File(file.toString());
        
        chaine3=""+source;

}
     ////////////////////////////////////////////////////////
     private void setExtFilters(FileChooser chooser){
        chooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Images", "*.*"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );
    }
     
     
     /////////////////////////////////////////////////////////
     /* Methodes de controle de saisie */
     
      private boolean validateEmaill(){
        Pattern p = Pattern.compile("[a-zA-Z0-9][a-zA-Z0-9._]*@[a-zA-Z0-9]+([.][a-zA-Z]+)+");
        Matcher m = p.matcher(mail.getText());
        if(m.find() && m.group().equals(mail.getText())){
            return true;
        }else{
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle("Syntaxe Email");
                alert.setHeaderText(null);
                alert.setContentText("S'il vous plait saisir un email valide");
                alert.showAndWait();
            
            return false;            
        }
      }
        private boolean validateNumberCapacite(){
        Pattern p = Pattern.compile("[0-9]+");
        Matcher m = p.matcher(capacites.getText());
        if(m.find() && m.group().equals(capacites.getText())){
            return true;
        }else{
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle("Syntaxe Capacite");
                alert.setHeaderText(null);
                alert.setContentText("S'il vous plait saisir une capacite valide");
                alert.showAndWait();
            
            return false;            
        }
        }
        
        private boolean validatePrix(){
        Pattern p = Pattern.compile("[0-9]+");
        Matcher m = p.matcher(prix.getText());
        if(m.find() && m.group().equals(prix.getText())){
            return true;
        }else{
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle(" Syntaxe Prix");
                alert.setHeaderText(null);
                alert.setContentText("S'il vous plait saisir un prix valide");
                alert.showAndWait();
            
            return false;            
        }
        }
         private boolean validateMobileNo(){
        Pattern p = Pattern.compile("[1-9][0-9]{7}");
        Matcher m = p.matcher(tel.getText());
        if(m.find() && m.group().equals(tel.getText())){
            return true;
        }else{
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle("Syntaxe Numero de télephone");
                alert.setHeaderText(null);
                alert.setContentText("S'il vous plait saisir un numéro de télephone valide");
                alert.showAndWait();
            
            return false;            
        }
}
        //////////////////////////////////////////////////////////////////
         private void setNode(Node node) {
        holderPane.getChildren().clear();
        holderPane.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.millis(1500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
    }
    
    private Object loadView(String path)
    {
        FXMLLoader fxmlLoader= new FXMLLoader(getClass().getResource((path)));
        AnchorPane parentContent = null;
        try {
            parentContent = fxmlLoader.load();
        } catch (IOException ex) {
            Logger.getLogger(ListesHotesController.class.getName()).log(Level.SEVERE, null, ex);
        }
        setNode(parentContent);
        System.gc();
        return fxmlLoader.getController();
    }

    @FXML
    private void btnRetour(ActionEvent event) throws IOException {
        
        loadView("/GUI/ListesHotes.fxml");
    }

     

}
     

