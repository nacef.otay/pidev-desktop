/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Core.Controller;
import Core.LayoutFrontController;
import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author OTAY Nacef
 */
public class AdminController implements Initializable {
 Stage stage= new Stage();
    Scene scene;
    @FXML
    private JFXButton btnHome;
    @FXML
    private JFXButton btnProfile;
    @FXML
    private JFXButton btnExit;
    @FXML
    private AnchorPane holderPane;
    AnchorPane Home, Profile , BonPlans,Hotes,Groups,magazin,btnEvent;
    public JFXButton btnProfile1;
    public JFXButton btnHome1;

    public static AnchorPane rootP;
    @FXML
    private JFXButton btnEvenement;
    @FXML
    private JFXButton maisonHotes;
    @FXML
    private JFXButton Groupes;
    @FXML
    private JFXButton BonsPlans;
    @FXML
    private JFXButton bmagazin;


    @Override
    public void initialize(URL url, ResourceBundle rb) {
         rootP = holderPane;
    }    
    public static void setNode(Node node) {
        rootP.getChildren().clear();
        rootP.getChildren().add(node);

        FadeTransition ft = new FadeTransition(Duration.millis(1500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
    }

    @FXML
    private void switchHome(ActionEvent event) throws IOException {
    
      Home = FXMLLoader.load(getClass().getResource("../GUI/HomeAdmin.fxml"));
        setNode(Home);
    }


    @FXML
    private void switchProfile(ActionEvent event) throws IOException {
           Profile = FXMLLoader.load(getClass().getResource("../GUI/UserAdmin.fxml"));
        setNode(Profile);
    }
    @FXML
    private void exit(ActionEvent event) {
       Controller.setUserId(0);              

            Node node =(Node)event.getSource();
        stage = (Stage)node.getScene().getWindow();
        stage.close();
     try {
         scene = new Scene(FXMLLoader.load(getClass().getResource("../GUI/Login.fxml")));
     } catch (IOException ex) {
         Logger.getLogger(LayoutFrontController.class.getName()).log(Level.SEVERE, null, ex);
     }
        stage.setScene(scene);
        stage.show();
                
    }

    @FXML
    private void maisonHotes(ActionEvent event) throws IOException {
    Hotes = FXMLLoader.load(getClass().getResource("../GUI/AdminHote.fxml"));
        setNode(Hotes);
    
    }

    @FXML
    private void Groupes(ActionEvent event) throws IOException {
   
        Groups = FXMLLoader.load(getClass().getResource("../GUI/GroupAdmin.fxml"));
        setNode(Groups);
    }
    
    
    
    @FXML
    private void BonPlans(ActionEvent event) throws IOException {

            BonPlans = FXMLLoader.load(getClass().getResource("../GUI/AdminChoixBP.fxml"));
        setNode(BonPlans);
    }

    @FXML
    private void switchEvenement(ActionEvent event) throws IOException {
        

            btnEvent = FXMLLoader.load(getClass().getResource("../GUI/EventAdmin.fxml"));
        setNode(btnEvent);
    }
    
    @FXML
    private void magazin(ActionEvent event) throws IOException {
        
            magazin = FXMLLoader.load(getClass().getResource("../GUI/CategorieBack.fxml"));
        setNode(magazin);
    }

}
