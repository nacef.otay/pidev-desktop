/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import static Core.Controller.getUserId;
import Entities.Produit;
import Entities.panier;
import Service.ServiceProduit;
import Service.ServicePanier;
import static Core.Controller.holderPane;
import Core.LayoutFrontController;
import Core.LayoutFrontController;
import Entities.category;
import Entities.region;
import Service.ServiceCategory;
import Service.ServiceRegion;
import com.jfoenix.controls.JFXButton;
import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;
import org.mindrot.jbcrypt.BCrypt;

/**
 * FXML Controller class
 *
 * @author ZerOo
 */
public class Produits1Controller implements Initializable {
    
    @FXML
    private ImageView panier;
    private boolean Verif;
    public static int nombreproduits = 0;
    @FXML
    private Label nbrnotif;
    
    ServiceProduit service_pr = new ServiceProduit();
    ServicePanier pan = new ServicePanier();
    
    ServiceRegion srrg = new ServiceRegion();
    ServiceCategory srct = new ServiceCategory();
    Produit p = new Produit();
    @FXML
    private ListView<Pane> ListView_Produits;
    @FXML
    private ListView<Pane> ListView_Produits1;
    @FXML
    private TextField prix;
    @FXML
    private TextField quantity;
    @FXML
    private ChoiceBox<String> category;
    @FXML
    private ChoiceBox<String> region;
    @FXML
    private TextField nomproduit;
    @FXML
    private TextArea description;
    @FXML
    private TextField recherche;
    @FXML
    private Button tous;
    @FXML
    private Button my;
    @FXML
    private Button fichier;
    @FXML
    private Button outre;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        for (category ct : srct.ListCategory()) {
            category.getItems().add(ct.getCategory());
        }
        for (region rg : srrg.ListRegion()) {
            region.getItems().add(rg.getRegion());
        }
        
        ListView_Produits.setFocusTraversable(false);
        getShowPane();

        //panier
        //  if (nombreproduits >= 1) {
        nombreproduits = pan.ListPanier().size();
        ListeCommande();
        //   }
        ListView_Produits1.setVisible(false);
        ListView_Produits1.setPrefWidth(ListView_Produits1.getWidth() + 300);
        ListView_Produits1.setPrefHeight(ListView_Produits1.getHeight() + 500);
        
    }
    
    public void ListeCommande() {
        ObservableList<Pane> refresh = FXCollections.observableArrayList();
        ListView_Produits1.setItems(refresh);
        // nombreproduits = nombreproduits + 1;
        nbrnotif.setText(String.valueOf(nombreproduits));

/////////////////panier ///////////////////////      
        ObservableList<Pane> Panes = FXCollections.observableArrayList();
        for (panier p3 : pan.ListPanierUser(getUserId())) {
            
            Pane pane = new Pane();
            pane.setStyle(" -fx-background-color: white");
            Pane pane2 = new Pane();
            pane2.setLayoutX(10);
            pane2.setLayoutY(10);
            pane2.setPrefWidth(pane2.getWidth() + 200);
            pane2.setPrefHeight(pane2.getHeight() + 80);

            //pane2.setStyle("-fx-background-radius: 50;");
            pane2.setStyle(" -fx-border-radius: 10 10 10 10;-fx-border-color: #383d3b ;");
            
            Pane panequantitet = new Pane();
            panequantitet.setLayoutX(100);
            panequantitet.setLayoutY(40);
            panequantitet.setPrefWidth(panequantitet.getWidth() + 160);
            panequantitet.setPrefHeight(panequantitet.getHeight() + 30);
            
            Text quan1 = new Text("Quantite : ");
            Label quant2 = new Label(String.valueOf(p3.getQuantite()));
            quan1.setLayoutX(10);
            quan1.setLayoutY(20);
            quant2.setLayoutX(90);
            quant2.setLayoutY(5);
            quan1.setStyle("-fx-font-weight: bold;-fx-fill : #d82819;-fx-font-size:15px;");
            quant2.setStyle("-fx-font-weight: bold;-fx-fill : #d82819;-fx-font-size:15px;");
            panequantitet.getChildren().addAll(quan1, quant2);
            
            Text nomt = new Text("Nom : ");
            Produit pname = new Produit();
            pname = service_pr.getAcualiteById(p3.getProduitid());
            Label nom = new Label(pname.getNom() + "");
            Text prixt = new Text("prix : ");
            Label prix = new Label(String.valueOf(p3.getPrix()) + " DT");
            nomt.setLayoutX(100);
            nomt.setLayoutY(20);
            nom.setLayoutX(150);
            nom.setLayoutY(10);
            prixt.setLayoutX(100);
            prixt.setLayoutY(35);
            prix.setLayoutX(150);
            prix.setLayoutY(25);
            nomt.setStyle("-fx-font-weight: bold;-fx-fill : #ce3b67");
            prixt.setStyle("-fx-font-weight: bold;-fx-fill : #ce3b67");
            pane2.getChildren().addAll(nomt, prixt, nom, prix, panequantitet);
            Panes.add(pane2);
            
        }
        
        if (Verif || Produit.getPanier().size() == 1) {
            
            ListView_Produits1.setPrefHeight(ListView_Produits1.getHeight() + 25);
        }
        ListView_Produits1.setItems(Panes);
    }
    
    public void getShowPane() {
        List<Produit> AllProducts = new ArrayList();
        
              int id = getUserId();
             for (Produit p : service_pr.My(id)) {
                AllProducts.add(p);
             }
        
        System.out.println(AllProducts);
        int i = 0;
        int j = 0;
        ObservableList<Pane> Panes = FXCollections.observableArrayList();
        
        List<Produit> ThreeProducts = new ArrayList();
        for (Produit p : AllProducts) {
            if (i == 0) {
                ThreeProducts.add(p);
                i++;
                j++;
                
                if (j == AllProducts.size()) {
                    Panes.add(AddPane(ThreeProducts));
                    
                    ThreeProducts.clear();
                }
                
            } else {
                ThreeProducts.add(p);
                i++;
                j++;
                if ((i % 3 == 0) || (j == AllProducts.size())) {
                    Panes.add(AddPane(ThreeProducts));
                    
                    ThreeProducts.clear();
                    
                }
            }
        }
        
        ObservableList<Pane> refresh = FXCollections.observableArrayList();
        ListView_Produits.setItems(refresh);
        ListView_Produits.setItems(Panes);
    }
    
    private void setNode(Node node) {
        holderPane.getChildren().clear();
        holderPane.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.millis(1500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
    }
    
    private Object loadView(String path) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource((path)));
        AnchorPane parentContent = null;
        try {
            parentContent = fxmlLoader.load();
        } catch (IOException ex) {
            Logger.getLogger(LayoutFrontController.class.getName()).log(Level.SEVERE, null, ex);
        }
        setNode(parentContent);
        System.gc();
        return fxmlLoader.getController();
    }
    
    public Pane AddPane(List<Produit> ThreeProduct) {
        Pane pane = new Pane();
        pane.setStyle(" -fx-background-color: white");
        int k = 1;
        for (Produit p3 : ThreeProduct) {
            if (k == 1) {
                Pane pane2 = new Pane();
                pane2.setLayoutX(25);
                pane2.setLayoutY(50);
                pane2.setPrefWidth(pane2.getWidth() + 200);
                pane2.setPrefHeight(pane2.getHeight() + 200);

                //pane2.setStyle("-fx-background-radius: 50;");
                pane2.setStyle(" -fx-border-radius: 10;-fx-border-color: #383d3b ;-fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.5), 8, 0, 0, 0); ");
                
                Button t1 = new Button("acheter");
                
                t1.setStyle("-fx-font-weight: bold;");
                t1.setStyle("-fx-background-color: #99c0ff");
                
                HBox hb2 = new HBox(t1);
                
                hb2.setLayoutX(70);
                hb2.setLayoutY(170);
                
                hb2.setStyle("-fx-background-color: #ea7066; ; -fx-background-radius: 0 0 10 0;");
                
                JFXButton t2 = new JFXButton("Modifier");
                t2.setStyle("-fx-font-weight: bold;");
                
                HBox hb3 = new HBox(t2);
                
                hb3.setLayoutX(20);
                hb3.setLayoutY(170);
                
                hb3.setStyle("-fx-background-color: #ea7066; ; -fx-background-radius: 0 0 10 0;");
                
                JFXButton t3 = new JFXButton("supprimer");
                
                t3.setStyle("-fx-font-weight: bold;");
                
                HBox hb4 = new HBox(t3);
                
                hb4.setLayoutX(100);
                hb4.setLayoutY(170);
                
                hb4.setStyle("-fx-background-color: #ea7066; ; -fx-background-radius: 0 0 10 0;");
                
                if (p3.getOwner() != getUserId()) {
                    pane2.getChildren().addAll(hb2);
                    System.out.println("moch taah");
                } else {
                    pane2.getChildren().addAll(hb4);
                    pane2.getChildren().addAll(hb3);
                }
                
                Text nomt = new Text("Nom : ");
                Label nom = new Label(p3.getNom());
                Text prixt = new Text("prix : ");
                Label prix = new Label(String.valueOf(p3.getPrix()) + " DT");
                
                nomt.setLayoutX(50);
                nomt.setLayoutY(160);
                nom.setLayoutX(100);
                nom.setLayoutY(145);
                prixt.setLayoutX(50);
                prixt.setLayoutY(180);
                prix.setLayoutX(100);
                prix.setLayoutY(165);
                nomt.setStyle("-fx-font-weight: bold;-fx-fill : #ce3b67");
                prixt.setStyle("-fx-font-weight: bold;-fx-fill : #ce3b67");
                System.out.println("houni" + Produit.getPanier());
                
                t3.setOnMouseClicked((MouseEvent event) -> {
                    
                    service_pr.supprimerProduit(p3.getId());
                    loadView("/GUI/produits.fxml");
                    
                });
                
                t2.setOnMouseClicked((MouseEvent event) -> {
                    Produit a = new Produit();
                    a = service_pr.getAcualiteById(p3.getId());

                    //------------------
                    TextInputDialog dialog = new TextInputDialog("");
                    dialog.setTitle("Modifier Produit");
                    dialog.setResizable(true);
                    dialog.getDialogPane().setPrefSize(700, 320);

                    //dialog.setContentText("Please enter your name:");
                    dialog.setHeaderText(null);
                    dialog.setGraphic(null);
                    //dialog.initStyle(StageStyle.UNDECORATED);
                    dialog.getDialogPane().setBackground(new Background(new BackgroundFill(Color.GREY, new CornerRadii(2), new Insets(2))));
                    dialog.getDialogPane().setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
                    //dialog.getDialogPane().setStyle("-fx-border-color: black");

                    GridPane grid = new GridPane();
                    grid.setHgap(10);
                    grid.setVgap(10);
                    grid.setPadding(new Insets(20, 150, 10, 10));
                    
                    TextField textNom = new TextField();
                    textNom.setPrefWidth(300);
                    textNom.setPrefHeight(40);
                    textNom.setText(a.getNom());
                    TextField textDescription = new TextField();
                    textDescription.setPrefWidth(300);
                    textDescription.setPrefHeight(40);
                    textDescription.setText(a.getDescription());
                    TextField textPrix = new TextField();
                    textPrix.setPrefWidth(300);
                    textPrix.setPrefHeight(40);
                    textPrix.setText(a.getPrix().toString());
                    TextField textQuantity = new TextField();
                    textQuantity.setPrefWidth(300);
                    textQuantity.setPrefHeight(40);
                    textQuantity.setText(a.getQuantity().toString());
                    
                    JFXButton upload = new JFXButton("Telecharger votre image");
                    ImageView image_p = new ImageView();
                    upload.setStyle("-fx-background-color: #724848; ");
                    upload.setTextFill(Color.web("#e8f8ff"));
                    upload.setOnMouseClicked(d -> {
                        FileChooser fc = new FileChooser();
                        Current_file = fc.showOpenDialog(null);
                        if (Current_file != null) {
                            Image images = new Image(Current_file.toURI().toString(), 100, 100, true, true);
                            image_p.setImage(images);
                            try {
                                fis = new FileInputStream(Current_file);
                                file_image = Current_file.getName();
                            } catch (FileNotFoundException ex) {
                                Logger.getLogger(ProduitsController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            
                        }
                        
                    });
                    grid.add(new Label("Nom: "), 0, 0);
                    grid.add(new Label("Description: "), 0, 1);
                    grid.add(new Label("Prix: "), 0, 2);
                    grid.add(new Label("Quantity: "), 0, 3);
                    
                    grid.add(textNom, 1, 0);
                    grid.add(textDescription, 1, 1);
                    grid.add(textPrix, 1, 2);
                    grid.add(textQuantity, 1, 3);
                    
                    grid.add(upload, 0, 4);
                    grid.add(image_p, 1, 4);
                    
                    dialog.getDialogPane().setContent(grid);
                    System.out.println("ahlan   " + a.getNom());
                    // Traditional way to get the response value.
                    Optional<String> result = dialog.showAndWait();
                    Produit gh = service_pr.getAcualiteById(p3.getId());
                    if (result.isPresent()) {
                        
                        Produit ass = new Produit();
                        ass.setId(p3.getId());
                        ass.setNom(textNom.getText());
                        ass.setDescription(textDescription.getText());
                        ass.setOwner(getUserId());
                        ass.setPrix(Integer.valueOf(textPrix.getText()));
                        ass.setQuantity(Integer.valueOf(textQuantity.getText()));

                        //// upload image ///////
                        try {
                            ass.setImage_id(file_image);
                            pathfrom = FileSystems.getDefault().getPath(Current_file.getPath());
                            pathto = FileSystems.getDefault().getPath("C:\\xampp\\htdocs\\Pi-Dev-Web\\web\\shopImg\\" + Current_file.getName());
                            Path targetDir = FileSystems.getDefault().getPath("C:\\xampp\\htdocs\\Pi-Dev-Web\\web\\shopImg\\");
                            
                            Files.copy(pathfrom, pathto, StandardCopyOption.REPLACE_EXISTING);
                        } catch (IOException ex) {
                            Logger.getLogger(ProduitsController.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        ///////////////
                        service_pr.modifierProduit(ass);
                        
                        loadView("/GUI/produits.fxml");
                        
                    }
                    
                });
                
                t1.setOnMouseClicked((MouseEvent event) -> {
                    System.out.println("Yes There is One Here ");
                    boolean found = false;
                    for (panier p : pan.ListPanier()) {
                        if (!found) {
                            if ((p3.getId() == p.getProduitid()) && (p.getUserid() == getUserId())) {
                                System.out.println("Exist");
                                found = true;
                                p.setQuantite(p.getQuantite() + 1);
                                p.setPrix(p.getPrix() + p3.getPrix());
                                pan.modifierpanier(p);
                            }
                        }
                    }
                    
                    if (!found) {
                        System.out.println("Nop");
                        panier events = new panier();
                        events.setProduitid(p3.getId());
                        events.setUserid(getUserId());
                        events.setQuantite(1);
                        events.setPrix(p3.getPrix());
                        pan.ajouterPanier(events);
                    }
                    
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("../GUI/produits.fxml"));
                    try {
                        holderPane.getChildren().clear();
                        holderPane.getChildren().add(loader.load());
                    } catch (IOException ex) {
                        Logger.getLogger(ProduitsController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    if (Produit.getPanier().contains(p3)) {
                        
                        for (int i = 0; i < Produit.getPanier().size(); i++) {
                            if (Produit.getPanier().get(i).getId() == p3.getId()) {
                                if (Produit.getPanier().get(i).getQuantity() < p3.getQuantity()) {
                                    Verif = Produit.setPanier(p3);
                                    System.out.println("aaaaaaaaaaa");
                                    
                                    Notifications n = Notifications.create().title("Notification")
                                            .text("Votre produit \"" + p3.getNom() + "\" a été ajouter au panier")
                                            .graphic(null)
                                            .position(Pos.BASELINE_LEFT)
                                            .onAction(new EventHandler<ActionEvent>() {
                                                
                                                public void handle(ActionEvent event) {
                                                    System.out.println("notifocation");
                                                }
                                            });
                                    n.showInformation();
                                    
                                    ListeCommande();
                                    getShowPane();
                                    
                                } else {
                                    Notifications n = Notifications.create().title("Notification")
                                            .text("Vous avez depasser le Stock du fournisseur on va lui notifier")
                                            .graphic(null)
                                            .position(Pos.BASELINE_LEFT)
                                            .onAction(new EventHandler<ActionEvent>() {
                                                
                                                public void handle(ActionEvent event) {
                                                    System.out.println("notifocation");
                                                }
                                            });
                                    n.showError();
                                    
                                }
                            }
                        }
                    } else {
                        if (p3.getQuantity() > 0) {
                            Verif = Produit.setPanier(p3);
                            System.out.println(Verif);
                            System.out.println(p3.getQuantity());
                            System.out.println(p3.getPanier());
                            ListeCommande();
                            getShowPane();
                            Notifications n = Notifications.create().title("Notification")
                                    .text("Votre produit \"" + p3.getNom() + "\" a été ajouter au panier")
                                    .graphic(null)
                                    .position(Pos.BASELINE_LEFT)
                                    .onAction(new EventHandler<ActionEvent>() {
                                        
                                        public void handle(ActionEvent event) {
                                            System.out.println("notifocation");
                                        }
                                    });
                            n.showInformation();
                            
                        } else {
                            Notifications n = Notifications.create().title("Notification")
                                    .text("Vous avez depasser le Stock du fournisseur on va lui notifier")
                                    .graphic(null)
                                    .position(Pos.BASELINE_LEFT)
                                    .onAction(new EventHandler<ActionEvent>() {
                                        
                                        public void handle(ActionEvent event) {
                                            System.out.println("notifocation");
                                        }
                                    });
                            n.showError();
                            
                        }
                    }
                });
                
                String A = p3.getImage_id();
                A = "C:\\xampp\\htdocs\\Pi-Dev-Web\\web\\shopImg\\" + A;
                File F1 = new File(A);
                Image image2 = new Image(F1.toURI().toString());
                
                ImageView image = new ImageView();
                image.setFitWidth(140);
                image.setFitHeight(130);
                image.setStyle("-fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.5), 8, 0, 0, 0);");
                
                image.setImage(image2);
                image.setLayoutX(30);
                image.setLayoutY(-45);
                pane2.getChildren().add(image);
                
                pane.getChildren().addAll(pane2, nomt, prixt, nom, prix);
            }
            ///////////////////////////////////              
            ///////////////////////////////////              
            ///////////////////////////////////              
            ///////////////////////////////////              
            ///////////////////////////////////              
            if (k == 2) {
                Pane pane2 = new Pane();
                pane2.setLayoutX(250);
                pane2.setLayoutY(50);
                pane2.setPrefWidth(pane2.getWidth() + 200);
                pane2.setPrefHeight(pane2.getHeight() + 200);
                //pane2.setStyle("-fx-background-radius: 50;");
                pane2.setStyle(" -fx-border-radius: 10 ;-fx-border-color: #383d3b ;-fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.5), 8, 0, 0, 0); ");
                
                Button t1 = new Button("acheter");
                
                t1.setStyle("-fx-font-weight: bold;");
                t1.setStyle("-fx-background-color: #99c0ff");
                
               
                
                HBox hb2 = new HBox(t1);
                
                hb2.setLayoutX(80);
                hb2.setLayoutY(170);
                
                hb2.setStyle("-fx-background-color: #ea7066; ; -fx-background-radius: 0 0 10 0;");
                
                JFXButton t2 = new JFXButton("Modifier");
                t2.setStyle("-fx-font-weight: bold;");
                HBox hb3 = new HBox(t2);
                
                hb3.setLayoutX(20);
                hb3.setLayoutY(170);
                
                hb3.setStyle("-fx-background-color: #ea7066; ; -fx-background-radius: 0 0 10 0;");
                
                JFXButton t3 = new JFXButton("supprimer");
                
                t3.setStyle("-fx-font-weight: bold;");
                
                HBox hb4 = new HBox(t3);
                
                hb4.setLayoutX(100);
                hb4.setLayoutY(170);
                
                hb4.setStyle("-fx-background-color: #ea7066; ; -fx-background-radius: 0 0 10 0;");
                
                if (p3.getOwner() != getUserId()) {
                    pane2.getChildren().addAll(hb2);
                    System.out.println("moch taah");
                } else {
                    pane2.getChildren().addAll(hb4);
                    pane2.getChildren().addAll(hb3);
                }
                
                Text nomt = new Text("Nom : ");
                Label nom = new Label(p3.getNom());
                Text prixt = new Text("prix : ");
                Label prix = new Label(String.valueOf(p3.getPrix()) + " DT");
                
                nomt.setLayoutX(275);
                nomt.setLayoutY(160);
                nom.setLayoutX(325);
                nom.setLayoutY(145);
                prixt.setLayoutX(275);
                prixt.setLayoutY(180);
                prix.setLayoutX(325);
                prix.setLayoutY(165);
                nomt.setStyle("-fx-font-weight: bold;-fx-fill : #ce3b67");
                prixt.setStyle("-fx-font-weight: bold;-fx-fill : #ce3b67");
                t3.setOnMouseClicked((MouseEvent event) -> {
                    
                    service_pr.supprimerProduit(p3.getId());
                    loadView("/GUI/produits.fxml");
                    
                });
                t2.setOnMouseClicked((MouseEvent event) -> {
                    Produit a = new Produit();
                    a = service_pr.getAcualiteById(p3.getId());

                    //------------------
                    TextInputDialog dialog = new TextInputDialog("");
                    dialog.setTitle("Modifier Produit");
                    dialog.setResizable(true);
                    dialog.getDialogPane().setPrefSize(700, 320);

                    //dialog.setContentText("Please enter your name:");
                    dialog.setHeaderText(null);
                    dialog.setGraphic(null);
                    //dialog.initStyle(StageStyle.UNDECORATED);
                    dialog.getDialogPane().setBackground(new Background(new BackgroundFill(Color.GREY, new CornerRadii(2), new Insets(2))));
                    dialog.getDialogPane().setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
                    //dialog.getDialogPane().setStyle("-fx-border-color: black");

                    GridPane grid = new GridPane();
                    grid.setHgap(10);
                    grid.setVgap(10);
                    grid.setPadding(new Insets(20, 150, 10, 10));
                    
                    TextField textNom = new TextField();
                    textNom.setPrefWidth(300);
                    textNom.setPrefHeight(40);
                    textNom.setText(a.getNom());
                    TextField textDescription = new TextField();
                    textDescription.setPrefWidth(300);
                    textDescription.setPrefHeight(40);
                    textDescription.setText(a.getDescription());
                    TextField textPrix = new TextField();
                    textPrix.setPrefWidth(300);
                    textPrix.setPrefHeight(40);
                    textPrix.setText(a.getPrix().toString());
                    TextField textQuantity = new TextField();
                    textQuantity.setPrefWidth(300);
                    textQuantity.setPrefHeight(40);
                    textQuantity.setText(a.getQuantity().toString());
                    
                    JFXButton upload = new JFXButton("Telecharger votre image");
                    ImageView image_p = new ImageView();
                    upload.setStyle("-fx-background-color: #724848; ");
                    upload.setTextFill(Color.web("#e8f8ff"));
                    upload.setOnMouseClicked(d -> {
                        FileChooser fc = new FileChooser();
                        Current_file = fc.showOpenDialog(null);
                        if (Current_file != null) {
                            Image images = new Image(Current_file.toURI().toString(), 100, 100, true, true);
                            image_p.setImage(images);
                            try {
                                fis = new FileInputStream(Current_file);
                                file_image = Current_file.getName();
                            } catch (FileNotFoundException ex) {
                                Logger.getLogger(ProduitsController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            
                        }
                        
                    });
                    grid.add(new Label("Nom: "), 0, 0);
                    grid.add(new Label("Description: "), 0, 1);
                    grid.add(new Label("Prix: "), 0, 2);
                    grid.add(new Label("Quantity: "), 0, 3);
                    
                    grid.add(textNom, 1, 0);
                    grid.add(textDescription, 1, 1);
                    grid.add(textPrix, 1, 2);
                    grid.add(textQuantity, 1, 3);
                    
                    grid.add(upload, 0, 4);
                    grid.add(image_p, 1, 4);
                    
                    dialog.getDialogPane().setContent(grid);
                    System.out.println("ahlan   " + a.getNom());
                    // Traditional way to get the response value.
                    Optional<String> result = dialog.showAndWait();
                    Produit gh = service_pr.getAcualiteById(p3.getId());
                    if (result.isPresent()) {
                        
                        Produit ass = new Produit();
                        ass.setId(p3.getId());
                        ass.setNom(textNom.getText());
                        ass.setDescription(textDescription.getText());
                        ass.setOwner(getUserId());
                        ass.setPrix(Integer.valueOf(textPrix.getText()));
                        ass.setQuantity(Integer.valueOf(textQuantity.getText()));

                        //// upload image ///////
                        try {
                            ass.setImage_id(file_image);
                            pathfrom = FileSystems.getDefault().getPath(Current_file.getPath());
                            pathto = FileSystems.getDefault().getPath("C:\\xampp\\htdocs\\Pi-Dev-Web\\web\\shopImg\\" + Current_file.getName());
                            Path targetDir = FileSystems.getDefault().getPath("C:\\xampp\\htdocs\\Pi-Dev-Web\\web\\shopImg\\");
                            
                            Files.copy(pathfrom, pathto, StandardCopyOption.REPLACE_EXISTING);
                        } catch (IOException ex) {
                            Logger.getLogger(ProduitsController.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        ///////////////
                        service_pr.modifierProduit(ass);
                        
                        loadView("/GUI/produits.fxml");
                        
                    }
                    
                });
                t1.setOnMouseClicked((MouseEvent event) -> {
                    System.out.println("Yes There is One Here ");
                    boolean found = false;
                    for (panier p : pan.ListPanier()) {
                        if (!found) {
                            if ((p3.getId() == p.getProduitid()) && (p.getUserid() == getUserId())) {
                                System.out.println("Exist");
                                found = true;
                                p.setQuantite(p.getQuantite() + 1);
                                p.setPrix(p.getPrix() + p3.getPrix());
                                pan.modifierpanier(p);
                            }
                        }
                    }
                    
                    if (!found) {
                        System.out.println("Nop");
                        panier events = new panier();
                        events.setProduitid(p3.getId());
                        events.setUserid(getUserId());
                        events.setQuantite(1);
                        events.setPrix(p3.getPrix());
                        pan.ajouterPanier(events);
                    }
                    
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("../GUI/produits.fxml"));
                    try {
                        holderPane.getChildren().clear();
                        holderPane.getChildren().add(loader.load());
                    } catch (IOException ex) {
                        Logger.getLogger(ProduitsController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    if (Produit.getPanier().contains(p3)) {
                        
                        for (int i = 0; i < Produit.getPanier().size(); i++) {
                            if (Produit.getPanier().get(i).getId() == p3.getId()) {
                                if (Produit.getPanier().get(i).getQuantity() < p3.getQuantity()) {
                                    Verif = Produit.setPanier(p3);
                                    System.out.println("aaaaaaaaaaa");
                                    
                                    Notifications n = Notifications.create().title("Notification")
                                            .text("Votre produit \"" + p3.getNom() + "\" a été ajouter au panier")
                                            .graphic(null)
                                            .position(Pos.BASELINE_LEFT)
                                            .onAction(new EventHandler<ActionEvent>() {
                                                
                                                public void handle(ActionEvent event) {
                                                    System.out.println("notifocation");
                                                }
                                            });
                                    n.showInformation();
                                    
                                    ListeCommande();
                                    getShowPane();
                                    
                                } else {
                                    Notifications n = Notifications.create().title("Notification")
                                            .text("Vous avez depasser le Stock du fournisseur on va lui notifier")
                                            .graphic(null)
                                            .position(Pos.BASELINE_LEFT)
                                            .onAction(new EventHandler<ActionEvent>() {
                                                
                                                public void handle(ActionEvent event) {
                                                    System.out.println("notifocation");
                                                }
                                            });
                                    n.showError();
                                    
                                }
                            }
                        }
                    } else {
                        if (p3.getQuantity() > 0) {
                            Verif = Produit.setPanier(p3);
                            System.out.println(Verif);
                            System.out.println(p3.getQuantity());
                            System.out.println(p3.getPanier());
                            ListeCommande();
                            getShowPane();
                            Notifications n = Notifications.create().title("Notification")
                                    .text("Votre produit \"" + p3.getNom() + "\" a été ajouter au panier")
                                    .graphic(null)
                                    .position(Pos.BASELINE_LEFT)
                                    .onAction(new EventHandler<ActionEvent>() {
                                        
                                        public void handle(ActionEvent event) {
                                            System.out.println("notifocation");
                                        }
                                    });
                            n.showInformation();
                            
                        } else {
                            Notifications n = Notifications.create().title("Notification")
                                    .text("Vous avez depasser le Stock du fournisseur on va lui notifier")
                                    .graphic(null)
                                    .position(Pos.BASELINE_LEFT)
                                    .onAction(new EventHandler<ActionEvent>() {
                                        
                                        public void handle(ActionEvent event) {
                                            System.out.println("notifocation");
                                        }
                                    });
                            n.showError();
                            
                        }
                    }
                });
                
                String A = p3.getImage_id();
                A = "C:\\xampp\\htdocs\\Pi-Dev-Web\\web\\shopImg\\" + A;
                File F1 = new File(A);
                Image image2 = new Image(F1.toURI().toString());
                
                ImageView image = new ImageView();
                image.setFitWidth(140);
                image.setFitHeight(130);
                image.setStyle("-fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.5), 8, 0, 0, 0);");
                
                image.setImage(image2);
                image.setLayoutX(30);
                image.setLayoutY(-45);
                pane2.getChildren().add(image);
                
                pane.getChildren().addAll(pane2, nomt, prixt, nom, prix);
            }
            
            if (k == 3) {
                Pane pane2 = new Pane();
                pane2.setLayoutX(475);
                pane2.setLayoutY(50);
                pane2.setPrefWidth(pane2.getWidth() + 200);
                pane2.setPrefHeight(pane2.getHeight() + 200);
                //pane2.setStyle("-fx-background-radius: 50;");
                pane2.setStyle(" -fx-border-radius: 10;-fx-border-color: #383d3b ;-fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.5), 8, 0, 0, 0); ");
                
                    Button t1 = new Button("acheter");
                
                    t1.setStyle("-fx-font-weight: bold;");
                    t1.setStyle("-fx-background-color: #99c0ff");
                
                
                
                HBox hb2 = new HBox(t1);
                
                hb2.setLayoutX(80);
                hb2.setLayoutY(170);
                
                hb2.setStyle("-fx-background-color: #ea7066; ; -fx-background-radius: 0 0 10 0;");
                
                JFXButton t2 = new JFXButton("Modifier");
                t2.setStyle("-fx-font-weight: bold;");
                HBox hb3 = new HBox(t2);
                
                hb3.setLayoutX(20);
                hb3.setLayoutY(170);
                
                hb3.setStyle("-fx-background-color: #ea7066; ; -fx-background-radius: 0 0 10 0;");
                
                JFXButton t3 = new JFXButton("supprimer");
                
                t3.setStyle("-fx-font-weight: bold;");
                
                HBox hb4 = new HBox(t3);
                
                hb4.setLayoutX(100);
                hb4.setLayoutY(170);
                
                hb4.setStyle("-fx-background-color: #ea7066; ; -fx-background-radius: 0 0 10 0;");
                
                if (p3.getOwner() != getUserId()) {
                    pane2.getChildren().addAll(hb2);
                    System.out.println("moch taah");
                } else {
                    pane2.getChildren().addAll(hb4);
                    pane2.getChildren().addAll(hb3);
                    
                }
                
                Text nomt = new Text("Nom : ");
                Label nom = new Label(p3.getNom());
                Text prixt = new Text("prix : ");
                Label prix = new Label(String.valueOf(p3.getPrix()) + " DT");
                
                nomt.setLayoutX(500);
                nomt.setLayoutY(160);
                nom.setLayoutX(550);
                nom.setLayoutY(145);
                prixt.setLayoutX(500);
                prixt.setLayoutY(180);
                prix.setLayoutX(550);
                prix.setLayoutY(165);
                nomt.setStyle("-fx-font-weight: bold;-fx-fill : #ce3b67");
                prixt.setStyle("-fx-font-weight: bold;-fx-fill : #ce3b67");
                t3.setOnMouseClicked((MouseEvent event) -> {
                    
                    service_pr.supprimerProduit(p3.getId());
                    loadView("/GUI/produits.fxml");
                    
                });
                t2.setOnMouseClicked((MouseEvent event) -> {
                    Produit a = new Produit();
                    a = service_pr.getAcualiteById(p3.getId());

                    //------------------
                    TextInputDialog dialog = new TextInputDialog("");
                    dialog.setTitle("Modifier Produit");
                    dialog.setResizable(true);
                    dialog.getDialogPane().setPrefSize(700, 320);

                    //dialog.setContentText("Please enter your name:");
                    dialog.setHeaderText(null);
                    dialog.setGraphic(null);
                    //dialog.initStyle(StageStyle.UNDECORATED);
                    dialog.getDialogPane().setBackground(new Background(new BackgroundFill(Color.GREY, new CornerRadii(2), new Insets(2))));
                    dialog.getDialogPane().setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
                    //dialog.getDialogPane().setStyle("-fx-border-color: black");

                    GridPane grid = new GridPane();
                    grid.setHgap(10);
                    grid.setVgap(10);
                    grid.setPadding(new Insets(20, 150, 10, 10));
                    
                    TextField textNom = new TextField();
                    textNom.setPrefWidth(300);
                    textNom.setPrefHeight(40);
                    textNom.setText(a.getNom());
                    TextField textDescription = new TextField();
                    textDescription.setPrefWidth(300);
                    textDescription.setPrefHeight(40);
                    textDescription.setText(a.getDescription());
                    TextField textPrix = new TextField();
                    textPrix.setPrefWidth(300);
                    textPrix.setPrefHeight(40);
                    textPrix.setText(a.getPrix().toString());
                    TextField textQuantity = new TextField();
                    textQuantity.setPrefWidth(300);
                    textQuantity.setPrefHeight(40);
                    textQuantity.setText(a.getQuantity().toString());
                    
                    JFXButton upload = new JFXButton("Telecharger votre image");
                    ImageView image_p = new ImageView();
                    upload.setStyle("-fx-background-color: #724848; ");
                    upload.setTextFill(Color.web("#e8f8ff"));
                    upload.setOnMouseClicked(d -> {
                        FileChooser fc = new FileChooser();
                        Current_file = fc.showOpenDialog(null);
                        if (Current_file != null) {
                            Image images = new Image(Current_file.toURI().toString(), 100, 100, true, true);
                            image_p.setImage(images);
                            try {
                                fis = new FileInputStream(Current_file);
                                file_image = Current_file.getName();
                            } catch (FileNotFoundException ex) {
                                Logger.getLogger(ProduitsController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            
                        }
                        
                    });
                    grid.add(new Label("Nom: "), 0, 0);
                    grid.add(new Label("Description: "), 0, 1);
                    grid.add(new Label("Prix: "), 0, 2);
                    grid.add(new Label("Quantity: "), 0, 3);
                    
                    grid.add(textNom, 1, 0);
                    grid.add(textDescription, 1, 1);
                    grid.add(textPrix, 1, 2);
                    grid.add(textQuantity, 1, 3);
                    
                    grid.add(upload, 0, 4);
                    grid.add(image_p, 1, 4);
                    
                    dialog.getDialogPane().setContent(grid);
                    System.out.println("ahlan   " + a.getNom());
                    // Traditional way to get the response value.
                    Optional<String> result = dialog.showAndWait();
                    Produit gh = service_pr.getAcualiteById(p3.getId());
                    if (result.isPresent()) {
                        
                        Produit ass = new Produit();
                        ass.setId(p3.getId());
                        ass.setNom(textNom.getText());
                        ass.setDescription(textDescription.getText());
                        ass.setOwner(getUserId());
                        ass.setPrix(Integer.valueOf(textPrix.getText()));
                        ass.setQuantity(Integer.valueOf(textQuantity.getText()));

                        //// upload image ///////
                        try {
                            ass.setImage_id(file_image);
                            pathfrom = FileSystems.getDefault().getPath(Current_file.getPath());
                            pathto = FileSystems.getDefault().getPath("C:\\xampp\\htdocs\\Pi-Dev-Web\\web\\shopImg\\" + Current_file.getName());
                            Path targetDir = FileSystems.getDefault().getPath("C:\\xampp\\htdocs\\Pi-Dev-Web\\web\\shopImg\\");
                            
                            Files.copy(pathfrom, pathto, StandardCopyOption.REPLACE_EXISTING);
                        } catch (IOException ex) {
                            Logger.getLogger(ProduitsController.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        ///////////////
                        service_pr.modifierProduit(ass);
                        
                        loadView("/GUI/produits.fxml");
                        
                    }
                    
                });
                t1.setOnMouseClicked((MouseEvent event) -> {
                    System.out.println("Yes There is One Here ");
                    boolean found = false;
                    for (panier p : pan.ListPanier()) {
                        if (!found) {
                            if ((p3.getId() == p.getProduitid()) && (p.getUserid() == getUserId())) {
                                System.out.println("Exist");
                                found = true;
                                p.setQuantite(p.getQuantite() + 1);
                                p.setPrix(p.getPrix() + p3.getPrix());
                                pan.modifierpanier(p);
                            }
                        }
                    }
                    
                    if (!found) {
                        System.out.println("Nop");
                        panier events = new panier();
                        events.setProduitid(p3.getId());
                        events.setUserid(getUserId());
                        events.setQuantite(1);
                        events.setPrix(p3.getPrix());
                        pan.ajouterPanier(events);
                    }
                    
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("../GUI/produits.fxml"));
                    try {
                        holderPane.getChildren().clear();
                        holderPane.getChildren().add(loader.load());
                    } catch (IOException ex) {
                        Logger.getLogger(ProduitsController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    if (Produit.getPanier().contains(p3)) {
                        
                        for (int i = 0; i < Produit.getPanier().size(); i++) {
                            if (Produit.getPanier().get(i).getId() == p3.getId()) {
                                if (Produit.getPanier().get(i).getQuantity() < p3.getQuantity()) {
                                    Verif = Produit.setPanier(p3);
                                    System.out.println("aaaaaaaaaaa");
                                    
                                    Notifications n = Notifications.create().title("Notification")
                                            .text("Votre produit \"" + p3.getNom() + "\" a été ajouter au panier")
                                            .graphic(null)
                                            .position(Pos.BASELINE_LEFT)
                                            .onAction(new EventHandler<ActionEvent>() {
                                                
                                                public void handle(ActionEvent event) {
                                                    System.out.println("notifocation");
                                                }
                                            });
                                    n.showInformation();
                                    
                                    ListeCommande();
                                    getShowPane();
                                    
                                } else {
                                    Notifications n = Notifications.create().title("Notification")
                                            .text("Vous avez depasser le Stock du fournisseur on va lui notifier")
                                            .graphic(null)
                                            .position(Pos.BASELINE_LEFT)
                                            .onAction(new EventHandler<ActionEvent>() {
                                                
                                                public void handle(ActionEvent event) {
                                                    System.out.println("notifocation");
                                                }
                                            });
                                    n.showError();
                                    
                                }
                            }
                        }
                    } else {
                        if (p3.getQuantity() > 0) {
                            Verif = Produit.setPanier(p3);
                            System.out.println(Verif);
                            System.out.println(p3.getQuantity());
                            System.out.println(p3.getPanier());
                            ListeCommande();
                            getShowPane();
                            Notifications n = Notifications.create().title("Notification")
                                    .text("Votre produit \"" + p3.getNom() + "\" a été ajouter au panier")
                                    .graphic(null)
                                    .position(Pos.BASELINE_LEFT)
                                    .onAction(new EventHandler<ActionEvent>() {
                                        
                                        public void handle(ActionEvent event) {
                                            System.out.println("notifocation");
                                        }
                                    });
                            n.showInformation();
                            
                        } else {
                            Notifications n = Notifications.create().title("Notification")
                                    .text("Vous avez depasser le Stock du fournisseur on va lui notifier")
                                    .graphic(null)
                                    .position(Pos.BASELINE_LEFT)
                                    .onAction(new EventHandler<ActionEvent>() {
                                        
                                        public void handle(ActionEvent event) {
                                            System.out.println("notifocation");
                                        }
                                    });
                            n.showError();
                            
                        }
                    }
                });
                
                String A = p3.getImage_id();
                A = "C:\\xampp\\htdocs\\Pi-Dev-Web\\web\\shopImg\\" + A;
                File F1 = new File(A);
                Image image2 = new Image(F1.toURI().toString());
                
                ImageView image = new ImageView();
                image.setFitWidth(140);
                image.setFitHeight(130);
                image.setStyle("-fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.5), 8, 0, 0, 0);");
                
                image.setImage(image2);
                image.setLayoutX(30);
                image.setLayoutY(-45);
                pane2.getChildren().add(image);
                
                pane.getChildren().addAll(pane2, nomt, prixt, nom, prix);
                
            }
            
            k++;
            
        }
        return pane;
    }
    
    @FXML
    private void panierhide(MouseEvent event) {

//         ListView_Produits1.setItems(Produit.getPanier());
        ListView_Produits1.setVisible(false);
    }
    
    @FXML
    private void paniershow(MouseEvent event) {
//         ListView_Produits1.setItems(Produit.getPanier());
        ListView_Produits1.setVisible(true);
    }
    
    @FXML
    private void hidepanier2(MouseEvent event) {
        ListView_Produits1.setVisible(false);
    }
    
    @FXML
    private void showpanier2(MouseEvent event) {
        ListView_Produits1.setVisible(true);
    }
    
    final FileChooser fileChooser = new FileChooser();
    private Desktop desktop = Desktop.getDesktop();
    private String file_image;
    private Path pathfrom;
    private Path pathto;
    private File Current_file;
    private FileInputStream fis;
    
    @FXML
    private void add(ActionEvent event) throws IOException, SQLException {
        
        
        region rname = srrg.getRegionByName(region.getSelectionModel().getSelectedItem());
        category cname = srct.getCategoryByName(category.getSelectionModel().getSelectedItem());
        
        if (!checkText(nomproduit.getText())) {
            Alert a = new Alert(null, "Vérifier le nom !! ", ButtonType.CLOSE);
            a.showAndWait();
        } else if (!checkText(description.getText())) {
            Alert a = new Alert(null, "Vérifier votre description !!", ButtonType.CLOSE);
            a.showAndWait();
            System.out.println("Incorrect");
            
        } else if (!checkNumber(prix.getText())) {
            Alert a = new Alert(null, "Vérifier votre prix !!", ButtonType.CLOSE);
            a.showAndWait();
            System.out.println("Incorrect");
        } else if (!checkNumber(quantity.getText())) {
            Alert a = new Alert(null, "Vérifier votre Quantitie !!", ButtonType.CLOSE);
            a.showAndWait();
            System.out.println("Incorrect");
        } else {
            Produit events = new Produit();
            
            events.setNom(nomproduit.getText());
            events.setDescription(description.getText());
            int qnt = Integer.parseInt(quantity.getText());
            events.setQuantity(qnt);
            int pri = Integer.parseInt(prix.getText());
            events.setPrix(pri);
                
            events.setCategory(cname.getId());
            events.setRegion(rname.getId());
            
            events.setImage_id(file_image);
            pathfrom = FileSystems.getDefault().getPath(Current_file.getPath());
            pathto = FileSystems.getDefault().getPath("C:\\xampp\\htdocs\\Pi-Dev-Web\\web\\shopImg\\" + Current_file.getName());
            Path targetDir = FileSystems.getDefault().getPath("C:\\xampp\\htdocs\\Pi-Dev-Web\\web\\shopImg\\");
            Files.copy(pathfrom, pathto, StandardCopyOption.REPLACE_EXISTING);

            ///////////////
            service_pr.ajouterPrdouit(events);
            
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../GUI/produits.fxml"));
            try {
                holderPane.getChildren().clear();
                holderPane.getChildren().add(loader.load());
            } catch (IOException ex) {
                Logger.getLogger(ProduitsController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }

    }
    
    private boolean checkText(String b) {
        
        if (this.testTextInput(b) || (this.testTextInput(b)) || (this.testTextInput(b)) || (this.testTextInput(b))) {
            return true;
        } else {
            return false;
        }
    }
    
    private boolean checkNumber(String b) {
        
        if (this.testInput(b) || (this.testInput(b)) || (this.testInput(b)) || (this.testInput(b))) {
            return true;
        } else {
            return false;
        }
    }
    
    private boolean testTextInput(String a) {
        
        boolean b = true;
        if (a.length() == 0 || testNumberInput(a)) {
            b = false;
        }
        
        return b;
        
    }
    
    private boolean testInput(String a) {
        
        boolean b = true;
        if (a.length() == 0 || teststringInput(a)) {
            b = false;
        }
        
        return b;
        
    }
    
    private boolean testNumberInput(String a) {
        boolean b = false;
        if (a.matches("^[0-9]*")) {
            b = true;
        }
        return b;
    }
    
    private boolean teststringInput(String a) {
        boolean b = false;
        if (a.matches("^[a-z A-Z]*")) {
            b = true;
        }
        return b;
    }
    
    @FXML
    private void upload(ActionEvent event) {
        
        FileChooser fc = new FileChooser();
        Current_file = fc.showOpenDialog(null);
        if (Current_file != null) {
            Image images = new Image(Current_file.toURI().toString(), 100, 100, true, true);
            try {
                fis = new FileInputStream(Current_file);
                file_image = Current_file.getName();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ProduitsController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        
        final GridPane inputGridPane = new GridPane();
        GridPane.setConstraints(fichier, 0, 0);
        inputGridPane.setHgap(6);
        inputGridPane.setVgap(6);
        final Pane rootGroup = new VBox(12);
        rootGroup.getChildren().addAll(inputGridPane);
        rootGroup.setPadding(new Insets(12, 12, 12, 12));
        
    }
    
    private void openFile(File file) {
        try {
            desktop.open(file);
        } catch (IOException ex) {
            
            Logger.getLogger(
                    ProduitsController.class.getName()).log(
                    Level.SEVERE, null, ex
            );
        }
    }
    
    public String rechercher = "";
    
    @FXML
    private void search(KeyEvent event) {
        rechercher = recherche.getText();
        getShowPane();
    }
    
    @FXML
    private void tous(ActionEvent event) {
        
        loadView("/GUI/produits.fxml");
    }
    
    
    @FXML
    private void my(ActionEvent event) {
        
        loadView("/GUI/produits_1.fxml");
    }

    @FXML
    private void paniershowall(MouseEvent event) {
        loadView("/GUI/panier.fxml");
    }

    @FXML
    private void outre(ActionEvent event) {
        
        loadView("/GUI/produits_2.fxml");
    }

    @FXML
    private void tous(MouseEvent event) {
    }
    
}
