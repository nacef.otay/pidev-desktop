/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Core.Controller;
import static Core.Controller.holderPane;
import Entities.Album;
import Entities.User;
import IService.IAlbumService;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import static java.lang.Thread.sleep;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;

/**
 * FXML Controller class
 *
 * @author Achrafoun
 */
public class AlbumController extends Controller implements Initializable {
    
    private static User connectedUser;
    private IAlbumService albumService = this.getService().getAlbumService();

    public static User getConnectedUser() {
        return connectedUser;
    }

    public static void setConnectedUser(User connectedUser) {
        AlbumController.connectedUser = connectedUser;
    }
    
    FileChooser saveFileChooser = new FileChooser();    
    File saveFile;
    File srcFile, destFile;
    
    @FXML
    private Label nomp;
    @FXML
    private ImageView photop;
    @FXML
    private Button journalButton;
    @FXML
    private Button aproposButton;
    @FXML
    private Button albumButton;
    @FXML
    private ScrollPane scrollPanGrid;
    @FXML
    private GridPane album;
    @FXML
    private Button ajouterPhoto;
    @FXML
    private Button paramsProfil;
    
        
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        nomp.setText(connectedUser.getNom()+" "+connectedUser.getPrenom());
        //-------------        
        photop.setImage(new Image(getClass().getResource("../Images/"+connectedUser.getImage()).toExternalForm()));
        //-------------
        List<Album> all_photos = albumService.getPhotosByUser(connectedUser);
        int c=0;
        int l=0;
        for(Album photo:all_photos)
        {
            ImageView img = new ImageView(getClass().getResource("../Images/"+photo.getUrl()).toExternalForm());
            img.setFitWidth(228);
            img.setFitHeight(155);
            img.setPickOnBounds(true);
            img.setPreserveRatio(true);
            img.setId(photo.getId().toString());
            img.setOnMouseClicked(this::deleteAction);
            album.add(img, c++, l);
            if(c==4)
            {
                c=0;
                l++;
            }
        }
        //------------
        
        
    }
    
    private void deleteAction(Event event)
    {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirmation de suppression");
        //alert.setHeaderText("Look, a Confirmation Dialog");
        alert.setContentText("Voulez vous supprimer cette photo ?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            ImageView x = (ImageView) event.getSource();
        Album a = new Album();
        a.setId(Integer.parseInt(x.getId()));
        albumService.supprimerPhoto(a);
        //-------
        try {
            //---
            sleep(3000);
        } catch (InterruptedException ex) {
            Logger.getLogger(AlbumController.class.getName()).log(Level.SEVERE, null, ex);
        }
        //---
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../GUI/album.fxml"));
        try {
            holderPane.getChildren().clear();
            holderPane.getChildren().add(loader.load());
        } catch (IOException ex) {
            Logger.getLogger(ProfilController.class.getName()).log(Level.SEVERE, null, ex);
        }
        } else {
            // ... user chose CANCEL or closed the dialog
        }
        //-------------
        
    }
    
    
    protected String genString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }
    
    @FXML
    private void journalAction(ActionEvent event) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../GUI/Journal.fxml"));
        try {
            holderPane.getChildren().clear();
            holderPane.getChildren().add(loader.load());
            ProfilController profilController = loader.getController();
            profilController.setConnectedUser(connectedUser);
        } catch (IOException ex) {
            Logger.getLogger(ProfilController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void aProposAction(ActionEvent event) {
        AProposController.setaProposUser(connectedUser);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../GUI/aPropos.fxml"));
        try {
            holderPane.getChildren().clear();
            holderPane.getChildren().add(loader.load());
            AProposController aproposController = loader.getController();
        } catch (IOException ex) {
            Logger.getLogger(ProfilController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void albumAction(ActionEvent event) {
    }

   @FXML
    private void ajouterPhotoAction(ActionEvent event) throws IOException {
    File file = saveFileChooser.showOpenDialog(null);
    try {
                srcFile = file;

         String p = System.getProperty("user.dir") + "/src/Images/" + srcFile.getName();
                    copyFile(srcFile, new File(p));
    } catch (Exception ex) {
    Logger.getLogger(AlbumController.class.getName()).log(Level.SEVERE, null, ex);
    }
    Album new_al = new Album(srcFile.getName(),connectedUser);
    albumService.ajouterPhoto(new_al);
    try {
    //---
    sleep(8000);
    } catch (InterruptedException ex) {
    Logger.getLogger(AlbumController.class.getName()).log(Level.SEVERE, null, ex);
    }
    //---
    FXMLLoader loader = new FXMLLoader(getClass().getResource("../GUI/album.fxml"));
    try {
    holderPane.getChildren().clear();
    holderPane.getChildren().add(loader.load());
    } catch (IOException ex) {
    Logger.getLogger(ProfilController.class.getName()).log(Level.SEVERE, null, ex);
    }
    }
    
    

    public void copyFile(File sourceFile, File destFile) throws IOException {
        if (!destFile.exists()) {
            destFile.createNewFile();
        }

        try (
                    FileChannel in = new FileInputStream(sourceFile).getChannel();
                    FileChannel out = new FileOutputStream(destFile).getChannel();) {

            out.transferFrom(in, 0, in.size());
        }
    }

    @FXML
    private void paramsProfilAction(ActionEvent event) {
        ParamsProfilController.setConnectedUser(connectedUser);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../GUI/paramsProfil.fxml"));
        try {
            holderPane.getChildren().clear();
            holderPane.getChildren().add(loader.load());
            ParamsProfilController paramsProfilController = loader.getController();
            paramsProfilController.setConnectedUser(connectedUser);
        } catch (IOException ex) {
            Logger.getLogger(ProfilController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
