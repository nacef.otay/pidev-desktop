/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import static Core.Controller.holderPane;
import Core.LayoutFrontController;
import Entities.Demande_responsable_hote;
import Entities.Maisons_hotes;
import Service.DemandeRespoHoteService;
import Service.MaisonHoteService;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import tray.animations.AnimationType;
import static tray.notification.NotificationType.SUCCESS;
import tray.notification.TrayNotification;

/**
 * FXML Controller class
 *
 * @author Bhs Nada
 */
public class DemandeRespHoteController implements Initializable {

    @FXML
    private TextArea description;
    @FXML
    private Button envoyerBtn;
    @FXML
    private Button AnnulerBtn;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void EnvoyerDemande(ActionEvent event) {
        if(description.getText().isEmpty())
        { Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Erreur");
            alert.setHeaderText(null);
            alert.setContentText("Il faut remplir les champs obligatoires ");
            alert.showAndWait();
        }
        else 
        { Demande_responsable_hote  demande = new Demande_responsable_hote(description.getText());
     
            DemandeRespoHoteService demandeRespoHoteService= new DemandeRespoHoteService();
            demandeRespoHoteService.ajouterDemande(demande);
            
           
        /**
         * ******* notif ******
         */
        TrayNotification tray = new TrayNotification(" succès", "Demande Envoyer avec Succés !", SUCCESS);
        tray.setAnimationType(AnimationType.POPUP);
        tray.showAndDismiss(Duration.seconds(10));
        /**
         * **********************
         */
                 loadView("../GUI/maisonhotelist.fxml");

        }
    }

    @FXML
    private void AnnulerDemande(ActionEvent event) {
         loadView("../GUI/maisonhotelist.fxml");
    }
    ///
     private void setNode(Node node) {
        holderPane.getChildren().clear();
        holderPane.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.millis(1500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
    }

    private Object loadView(String path) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource((path)));
        AnchorPane parentContent = null;
        try {
            parentContent = fxmlLoader.load();
        } catch (IOException ex) {
            Logger.getLogger(LayoutFrontController.class.getName()).log(Level.SEVERE, null, ex);
        }
        setNode(parentContent);
        System.gc();
        return fxmlLoader.getController();
    }
    ////
}
