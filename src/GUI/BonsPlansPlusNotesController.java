/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import static Core.Controller.holderPane;
import Core.LayoutFrontController;
import Entities.Bonplan;
import Entities.Categorie;
import Service.BonPlanService;
import Service.CategorieService;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.Duration;
import org.controlsfx.control.Rating;

/**
 * FXML Controller class
 *
 * @author marie
 */
public class BonsPlansPlusNotesController implements Initializable {

    @FXML
    private ListView<Bonplan> listPlusNote;
    ObservableList<Bonplan> mmbonplan= FXCollections.observableArrayList();
    ObservableList<Bonplan> mmRatingbonplan= FXCollections.observableArrayList();
    BonPlanService servicebon = new BonPlanService();
    CategorieService serviceCategorie = new CategorieService();
    ObservableList<String> mmcat= FXCollections.observableArrayList();

    @FXML
    private ListView<Bonplan> listRating;
    @FXML
    private Button btnRetour;
    @FXML
    private ComboBox<String> filterCategorie;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        mmcat= (ObservableList<String>) serviceCategorie.list();
        filterCategorie.getItems().addAll(mmcat);
        
        
        ////
        mmbonplan = (ObservableList) servicebon.BonsPlansPlusNotes();
        listPlusNote.setItems(mmbonplan);
        listPlusNote.getSelectionModel().clearSelection();
        
     listPlusNote.setCellFactory(new Callback<ListView<Bonplan>, ListCell<Bonplan>>() {
            @Override
            public ListCell<Bonplan> call(ListView<Bonplan> param) {
                ListCell<Bonplan> cell;
                cell = new ListCell<Bonplan>() {
                    protected void updateItem(Bonplan item, boolean empty) {
                        if (item != null) {

                            HBox hbox = new HBox();
                            hbox.setSpacing(70);
                            hbox.setAlignment(Pos.CENTER);

                            VBox vbox = new VBox();
                            vbox.setSpacing(2.5);
                            vbox.setAlignment(Pos.CENTER);

                            VBox Buttons = new VBox();
                            Buttons.setSpacing(2.5);
                            Buttons.setAlignment(Pos.CENTER);

                            Image img;
                            img = new Image("file:" + item.getImage());
                            ImageView imgView = new ImageView(img);

                            //**************************ImgView Resize*****************
                            imgView.setFitWidth(150);
                            imgView.setFitHeight(150);
                            imgView.setPreserveRatio(false);
                            imgView.setSmooth(true);
                            imgView.setCache(true);
                            //***********************************************************

                            Button detail = new Button("Details.");
                            Button Reserver = new Button("Reserver");

                            //*****************************Details_button_OnClick******************************
                            

                            ///////////////////////////////////////////////////////////////////////////// 
                            //*****************************Details_button_Reserver******************************
                           

                            /////////////////////////////////////////////////////
                            Label lbnom= new Label("Nom: "+item.getName());
                            Label lbNote = new Label("Note: "+Integer.toString(item.getNote()));
                            vbox.getChildren().add(lbnom);
                            vbox.getChildren().add(lbNote);
                            hbox.getChildren().add(imgView);
                            hbox.getChildren().add(vbox);
                            hbox.getChildren().add(Buttons);

                            setGraphic(hbox);

                        }

                    }

                };
                return cell;
            }

        });
     ////////////////////
           mmRatingbonplan = (ObservableList) servicebon.BonsPlansByRating();
        listRating.setItems(mmRatingbonplan);
        listRating.getSelectionModel().clearSelection();
        
     listRating.setCellFactory(new Callback<ListView<Bonplan>, ListCell<Bonplan>>() {
            @Override
            public ListCell<Bonplan> call(ListView<Bonplan> param) {
                ListCell<Bonplan> cell;
                cell = new ListCell<Bonplan>() {
                    protected void updateItem(Bonplan item, boolean empty) {
                        if (item != null) {

                            HBox hbox = new HBox();
                            hbox.setSpacing(70);
                            hbox.setAlignment(Pos.CENTER);

                            VBox vbox = new VBox();
                            vbox.setSpacing(2.5);
                            vbox.setAlignment(Pos.CENTER);

                            VBox Buttons = new VBox();
                            Buttons.setSpacing(2.5);
                            Buttons.setAlignment(Pos.CENTER);

                            Image img;
                            img = new Image("file:" + item.getImage());
                            ImageView imgView = new ImageView(img);

                            //**************************ImgView Resize*****************
                            imgView.setFitWidth(150);
                            imgView.setFitHeight(150);
                            imgView.setPreserveRatio(false);
                            imgView.setSmooth(true);
                            imgView.setCache(true);
                            //***********************************************************

                            Button detail = new Button("Details.");
                            Button Reserver = new Button("Reserver");

                            //*****************************Details_button_OnClick******************************
                            

                            ///////////////////////////////////////////////////////////////////////////// 
                            //*****************************Details_button_Reserver******************************
                           

                            /////////////////////////////////////////////////////
                            Label lbnom= new Label("Nom: "+item.getName());
                            //Label lbNote = new Label("Note: "+Integer.toString(item.getNote()));
                            Rating rt = new Rating(5, item.getEtoile());
                            vbox.getChildren().add(lbnom);
                            vbox.getChildren().add(rt);
                            hbox.getChildren().add(imgView);
                            hbox.getChildren().add(vbox);
                            hbox.getChildren().add(Buttons);

                            setGraphic(hbox);

                        }

                    }

                };
                return cell;
            }

        });
     /////////////////////////
     
        
    }    

    ///////////////
     private void setNode(Node node) {
        holderPane.getChildren().clear();
        holderPane.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.millis(1500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
    }
    
    private Object loadView(String path)
    {
        FXMLLoader fxmlLoader= new FXMLLoader(getClass().getResource((path)));
        AnchorPane parentContent = null;
        try {
            parentContent = fxmlLoader.load();
        } catch (IOException ex) {
            Logger.getLogger(LayoutFrontController.class.getName()).log(Level.SEVERE, null, ex);
        }
        setNode(parentContent);
        System.gc();
        return fxmlLoader.getController();
    }
//////////////////

    @FXML
    private void Retour(ActionEvent event) {
        loadView("../GUI/ListBonPlan.fxml");
        
    }

    @FXML
    private void FiltrerByCategorie(ActionEvent event) {
          mmbonplan.clear();
          mmRatingbonplan.clear();
         mmbonplan.addAll((ObservableList) servicebon.FindByCategorie(filterCategorie.getSelectionModel().getSelectedItem()));
         mmRatingbonplan.addAll((ObservableList) servicebon.FindByCategorie(filterCategorie.getSelectionModel().getSelectedItem()));
        listPlusNote.setItems(mmbonplan);
        listRating.setItems(mmRatingbonplan);
        
        
        ////////////
         listPlusNote.setCellFactory(new Callback<ListView<Bonplan>, ListCell<Bonplan>>() {
            @Override
            public ListCell<Bonplan> call(ListView<Bonplan> param) {
                ListCell<Bonplan> cell;
                cell = new ListCell<Bonplan>() {
                    protected void updateItem(Bonplan item, boolean empty) {
                        if (item != null) {

                            HBox hbox = new HBox();
                            hbox.setSpacing(70);
                            hbox.setAlignment(Pos.CENTER);

                            VBox vbox = new VBox();
                            vbox.setSpacing(2.5);
                            vbox.setAlignment(Pos.CENTER);

                            VBox Buttons = new VBox();
                            Buttons.setSpacing(2.5);
                            Buttons.setAlignment(Pos.CENTER);

                            Image img;
                            img = new Image("file:" + item.getImage());
                            ImageView imgView = new ImageView(img);

                            //**************************ImgView Resize*****************
                            imgView.setFitWidth(150);
                            imgView.setFitHeight(150);
                            imgView.setPreserveRatio(false);
                            imgView.setSmooth(true);
                            imgView.setCache(true);
                            //***********************************************************

                            Button detail = new Button("Details.");
                            Button Reserver = new Button("Reserver");

                            //*****************************Details_button_OnClick******************************
                            

                            ///////////////////////////////////////////////////////////////////////////// 
                            //*****************************Details_button_Reserver******************************
                           

                            /////////////////////////////////////////////////////
                            Label lbnom= new Label("Nom: "+item.getName());
                            Label lbNote = new Label("Note: "+Integer.toString(item.getNote()));
                            vbox.getChildren().add(lbnom);
                            vbox.getChildren().add(lbNote);
                            hbox.getChildren().add(imgView);
                            hbox.getChildren().add(vbox);
                            hbox.getChildren().add(Buttons);

                            setGraphic(hbox);

                        }

                    }

                };
                return cell;
            }

        });
     ////////////////////
     listRating.setCellFactory(new Callback<ListView<Bonplan>, ListCell<Bonplan>>() {
            @Override
            public ListCell<Bonplan> call(ListView<Bonplan> param) {
                ListCell<Bonplan> cell;
                cell = new ListCell<Bonplan>() {
                    protected void updateItem(Bonplan item, boolean empty) {
                        if (item != null) {

                            HBox hbox = new HBox();
                            hbox.setSpacing(70);
                            hbox.setAlignment(Pos.CENTER);

                            VBox vbox = new VBox();
                            vbox.setSpacing(2.5);
                            vbox.setAlignment(Pos.CENTER);

                            VBox Buttons = new VBox();
                            Buttons.setSpacing(2.5);
                            Buttons.setAlignment(Pos.CENTER);

                            Image img;
                            img = new Image("file:" + item.getImage());
                            ImageView imgView = new ImageView(img);

                            //**************************ImgView Resize*****************
                            imgView.setFitWidth(150);
                            imgView.setFitHeight(150);
                            imgView.setPreserveRatio(false);
                            imgView.setSmooth(true);
                            imgView.setCache(true);
                            //***********************************************************

                            Button detail = new Button("Details.");
                            Button Reserver = new Button("Reserver");

                            //*****************************Details_button_OnClick******************************
                            

                            ///////////////////////////////////////////////////////////////////////////// 
                            //*****************************Details_button_Reserver******************************
                           

                            /////////////////////////////////////////////////////
                            Label lbnom= new Label("Nom: "+item.getName());
                            //Label lbNote = new Label("Note: "+Integer.toString(item.getNote()));
                            Rating rt = new Rating(5, item.getEtoile());
                            vbox.getChildren().add(lbnom);
                            vbox.getChildren().add(rt);
                            hbox.getChildren().add(imgView);
                            hbox.getChildren().add(vbox);
                            hbox.getChildren().add(Buttons);

                            setGraphic(hbox);

                        }

                    }

                };
                return cell;
            }

        });
        
        //////////////
        
    }
    
}
