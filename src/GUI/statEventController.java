/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import static Core.Controller.holderPane;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import utils.DataSource;

/**
 * FXML Controller class
 *
 * @author Hsine
 */
public class statEventController implements Initializable {

    @FXML
    private JFXButton statReclamationBtn;
    @FXML
    private BarChart<String, Integer> barChart;
    @FXML
    private AnchorPane statistique;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        

        
       
    }    

    @FXML
    private void AfficherStatReclamation(ActionEvent event) {
        String req = "select nomEvent, count(*) as nbrmembre from participants group by nomEvent";
    
        XYChart.Series<String,Integer> series = new XYChart.Series<>();
        try{       
       
            Statement stm = DataSource.getInstance().getConnection().createStatement();
            ResultSet rs = stm.executeQuery(req);
            while(rs.next()){
                
                series.getData().add(new XYChart.Data<>(rs.getString(1)
                        ,rs.getInt(2)));
            }                    

            barChart.getData().add(series);

        }catch(Exception e){
            
        }
    }

    @FXML
    private void retourclicked(ActionEvent event) throws IOException {
          AnchorPane parentContent = FXMLLoader.load(getClass().getResource(("/GUI/EventAdmin.fxml")));
        holderPane = statistique;
        holderPane.getChildren().setAll(parentContent);
    }
    
}
