/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;


import static GUI.ProduitsController.nombreproduits;
import static Core.Controller.getUserId;
import static Core.Controller.holderPane;
import Core.LayoutFrontController;
import Core.LayoutFrontController;
import Entities.Produit;
import Entities.category;
import Entities.panier;
import Entities.region;
import Service.ServiceCategory;
import Service.ServicePanier;
import Service.ServiceProduit;
import Service.ServiceRegion;
import com.jfoenix.controls.JFXButton;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;

/**
 * FXML Controller class
 *
 * @author ZerOo
 */
public class PanierController implements Initializable {

    @FXML
    private ListView<Pane> ListView_Produits;
    @FXML
    private TextField prix;
    @FXML
    private TextField quantity;
    @FXML
    private ChoiceBox<String> category;
    @FXML
    private ChoiceBox<String> region;
    @FXML
    private Button fichier;
    @FXML
    private TextField nomproduit;
    @FXML
    private TextArea description;
    @FXML
    private ImageView panier;
    @FXML
    private Label nbrnotif;
    @FXML
    private ListView<Pane> ListView_Produits1;

    ServiceProduit service_pr = new ServiceProduit();
    ServicePanier pan = new ServicePanier();

    ServiceRegion srrg = new ServiceRegion();
    ServiceCategory srct = new ServiceCategory();
    Produit p = new Produit();
    private boolean Verif;
    @FXML
    private Label totalprix;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        for (category ct : srct.ListCategory()) {
            category.getItems().add(ct.getCategory());
        }
        for (region rg : srrg.ListRegion()) {
            region.getItems().add(rg.getRegion());
        }

        ListView_Produits.setFocusTraversable(false);
        getShowPane();

        //panier
        //  if (nombreproduits >= 1) {
        nombreproduits = pan.ListPanier().size();
        ListeCommande();
        //   }
        ListView_Produits1.setVisible(false);
        ListView_Produits1.setPrefWidth(ListView_Produits1.getWidth() + 300);
        ListView_Produits1.setPrefHeight(ListView_Produits1.getHeight() + 500);
    }

    public void getShowPane() {
        System.out.println("SHow Panier HAHAHAH");
        List<panier> AllProducts = new ArrayList();

        int id = getUserId();
        for (panier p : pan.ListPanierUser(getUserId())) {
            AllProducts.add(p);
        }

        System.out.println(AllProducts);
        int i = 0;
        int j = 0;
        ObservableList<Pane> Panes = FXCollections.observableArrayList();

        List<panier> ThreeProducts = new ArrayList();
        for (panier p : AllProducts) {
            if (i == 0) {
                ThreeProducts.add(p);
                i++;
                j++;

                if (j == AllProducts.size()) {
                    Panes.add(AddPanePanier(ThreeProducts));

                    ThreeProducts.clear();
                }

            } else {
                ThreeProducts.add(p);
                i++;
                j++;
                if ((i % 3 == 0) || (j == AllProducts.size())) {
                    Panes.add(AddPanePanier(ThreeProducts));

                    ThreeProducts.clear();

                }
            }
        }

        ObservableList<Pane> refresh = FXCollections.observableArrayList();
        ListView_Produits.setItems(refresh);
        ListView_Produits.setItems(Panes);
    }

    public Pane AddPanePanier(List<panier> ThreeProduct) {
        Pane pane = new Pane();
        VBox myVox = new VBox();
        int total = 0;
        for (panier pn : ThreeProduct) {
            pane.setStyle(" -fx-background-color: white");
            Pane pane2 = new Pane();
            pane2.setLayoutX(10);
            pane2.setLayoutY(10);
            pane2.setPrefWidth(pane2.getWidth() + 400);
            pane2.setPrefHeight(pane2.getHeight() + 80);

            //pane2.setStyle("-fx-background-radius: 50;");
            pane2.setStyle(" -fx-border-radius: 10 10 10 10;-fx-border-color: #383d3b ;");

            Pane panequantitet = new Pane();
            panequantitet.setLayoutX(100);
            panequantitet.setLayoutY(40);
            panequantitet.setPrefWidth(panequantitet.getWidth() + 260);
            panequantitet.setPrefHeight(panequantitet.getHeight() + 30);

            Text quan1 = new Text("Quantite : ");
            Label quant2 = new Label(String.valueOf(pn.getQuantite()));
            quan1.setLayoutX(10);
            quan1.setLayoutY(20);
            quant2.setLayoutX(90);
            quant2.setLayoutY(5);
            quan1.setStyle("-fx-font-weight: bold;-fx-fill : #d82819;-fx-font-size:15px;");
            quant2.setStyle("-fx-font-weight: bold;-fx-fill : #d82819;-fx-font-size:15px;");
            panequantitet.getChildren().addAll(quan1, quant2);

            Text nomt = new Text("Nom : ");
            Produit pname = new Produit();
            pname = service_pr.getAcualiteById(pn.getProduitid());
            Label nom = new Label(pname.getNom() + "");
            Text prixt = new Text("prix : ");
            Label prix = new Label(String.valueOf(pn.getPrix()) + " DT");
            Button delete = new Button("Delete");
            nomt.setLayoutX(100);
            nomt.setLayoutY(20);
            nom.setLayoutX(150);
            nom.setLayoutY(10);
            prixt.setLayoutX(100);
            prixt.setLayoutY(35);
            prix.setLayoutX(150);
            prix.setLayoutY(25);
            delete.setLayoutX(250);
            delete.setLayoutY(30);
            nomt.setStyle("-fx-font-weight: bold;-fx-fill : #ce3b67");
            prixt.setStyle("-fx-font-weight: bold;-fx-fill : #ce3b67");
            total += pn.getPrix();
            delete.setOnAction((event) -> {
                System.out.println("Delete This : " + pn.getId());
                pan.supprimerPanier(pn.getId());
                loadView("/GUI/produits_2.fxml");
            });
            pane2.getChildren().addAll(nomt, prixt, nom, prix, delete, panequantitet);
            myVox.getChildren().add(pane2);
        }
        totalprix.setText("Montant Total : " + total);
        pane.getChildren().add(myVox);
        return pane;
    }

    public Pane AddPane(List<Produit> ThreeProduct) {
        Pane pane = new Pane();
        pane.setStyle(" -fx-background-color: white");
        int k = 1;
        for (Produit p3 : ThreeProduct) {
            if (k == 1) {
                Pane pane2 = new Pane();
                pane2.setLayoutX(25);
                pane2.setLayoutY(50);
                pane2.setPrefWidth(pane2.getWidth() + 200);
                pane2.setPrefHeight(pane2.getHeight() + 200);

                //pane2.setStyle("-fx-background-radius: 50;");
                pane2.setStyle(" -fx-border-radius: 10;-fx-border-color: #383d3b ;-fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.5), 8, 0, 0, 0); ");

                Button t1 = new Button("acheter");

                t1.setStyle("-fx-font-weight: bold;");
                t1.setStyle("-fx-background-color: #99c0ff");

                HBox hb2 = new HBox(t1);

                hb2.setLayoutX(70);
                hb2.setLayoutY(170);

                hb2.setStyle("-fx-background-color: #ea7066; ; -fx-background-radius: 0 0 10 0;");

                JFXButton t2 = new JFXButton("Modifier");
                t2.setStyle("-fx-font-weight: bold;");

                HBox hb3 = new HBox(t2);

                hb3.setLayoutX(20);
                hb3.setLayoutY(170);

                hb3.setStyle("-fx-background-color: #ea7066; ; -fx-background-radius: 0 0 10 0;");

                JFXButton t3 = new JFXButton("supprimer");

                t3.setStyle("-fx-font-weight: bold;");

                HBox hb4 = new HBox(t3);

                hb4.setLayoutX(100);
                hb4.setLayoutY(170);

                hb4.setStyle("-fx-background-color: #ea7066; ; -fx-background-radius: 0 0 10 0;");

                if (p3.getOwner() != getUserId()) {
                    pane2.getChildren().addAll(hb2);
                    System.out.println("moch taah");
                } else {
                    pane2.getChildren().addAll(hb4);
                    pane2.getChildren().addAll(hb3);
                }

                Text nomt = new Text("Nom : ");
                Label nom = new Label(p3.getNom());
                Text prixt = new Text("prix : ");
                Label prix = new Label(String.valueOf(p3.getPrix()) + " DT");

                nomt.setLayoutX(50);
                nomt.setLayoutY(160);
                nom.setLayoutX(100);
                nom.setLayoutY(145);
                prixt.setLayoutX(50);
                prixt.setLayoutY(180);
                prix.setLayoutX(100);
                prix.setLayoutY(165);
                nomt.setStyle("-fx-font-weight: bold;-fx-fill : #ce3b67");
                prixt.setStyle("-fx-font-weight: bold;-fx-fill : #ce3b67");
                System.out.println("houni" + Produit.getPanier());

                t3.setOnMouseClicked((MouseEvent event) -> {

                    service_pr.supprimerProduit(p3.getId());
                    loadView("/GUI/produits.fxml");

                });

                t1.setOnMouseClicked((MouseEvent event) -> {
                    
                    
                    
                    
                    if (Produit.getPanier().contains(p3)) {

                        for (int i = 0; i < Produit.getPanier().size(); i++) {
                            if (Produit.getPanier().get(i).getId() == p3.getId()) {
                                if (Produit.getPanier().get(i).getQuantity() < p3.getQuantity()) {
                                    Verif = Produit.setPanier(p3);
                                    System.out.println("aaaaaaaaaaa");

                                    Notifications n = Notifications.create().title("Notification")
                                            .text("Votre produit \"" + p3.getNom() + "\" a été ajouter au panier")
                                            .graphic(null)
                                            .position(Pos.BASELINE_LEFT)
                                            .onAction(new EventHandler<ActionEvent>() {

                                                public void handle(ActionEvent event) {
                                                    System.out.println("notifocation");
                                                }
                                            });
                                    n.showInformation();

                                    ListeCommande();
                                    getShowPane();

                                } else {
                                    Notifications n = Notifications.create().title("Notification")
                                            .text("Vous avez depasser le Stock du fournisseur on va lui notifier")
                                            .graphic(null)
                                            .position(Pos.BASELINE_LEFT)
                                            .onAction(new EventHandler<ActionEvent>() {

                                                public void handle(ActionEvent event) {
                                                    System.out.println("notifocation");
                                                }
                                            });
                                    n.showError();

                                }
                            }
                        }
                    } else {
                        if (p3.getQuantity() > 0) {
                            Verif = Produit.setPanier(p3);
                            System.out.println(Verif);
                            System.out.println(p3.getQuantity());
                            System.out.println(p3.getPanier());
                            ListeCommande();
                            getShowPane();
                            Notifications n = Notifications.create().title("Notification")
                                    .text("Votre produit \"" + p3.getNom() + "\" a été ajouter au panier")
                                    .graphic(null)
                                    .position(Pos.BASELINE_LEFT)
                                    .onAction(new EventHandler<ActionEvent>() {

                                        public void handle(ActionEvent event) {
                                            System.out.println("notifocation");
                                        }
                                    });
                            n.showInformation();

                        } else {
                            Notifications n = Notifications.create().title("Notification")
                                    .text("Vous avez depasser le Stock du fournisseur on va lui notifier")
                                    .graphic(null)
                                    .position(Pos.BASELINE_LEFT)
                                    .onAction(new EventHandler<ActionEvent>() {

                                        public void handle(ActionEvent event) {
                                            System.out.println("notifocation");
                                        }
                                    });
                            n.showError();

                        }
                    }
                });

                String A = p3.getImage_id();
                A = "C:\\xampp\\htdocs\\Pi-Dev-Web\\web\\shopImg\\" + A;
                File F1 = new File(A);
                Image image2 = new Image(F1.toURI().toString());

                ImageView image = new ImageView();
                image.setFitWidth(140);
                image.setFitHeight(130);
                image.setStyle("-fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.5), 8, 0, 0, 0);");

                image.setImage(image2);
                image.setLayoutX(30);
                image.setLayoutY(-45);
                pane2.getChildren().add(image);

                pane.getChildren().addAll(pane2, nomt, prixt, nom, prix);
            }
            ///////////////////////////////////              
            ///////////////////////////////////              
            ///////////////////////////////////              
            ///////////////////////////////////              
            ///////////////////////////////////              
            if (k == 2) {
                Pane pane2 = new Pane();
                pane2.setLayoutX(250);
                pane2.setLayoutY(50);
                pane2.setPrefWidth(pane2.getWidth() + 200);
                pane2.setPrefHeight(pane2.getHeight() + 200);
                //pane2.setStyle("-fx-background-radius: 50;");
                pane2.setStyle(" -fx-border-radius: 10 ;-fx-border-color: #383d3b ;-fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.5), 8, 0, 0, 0); ");

                JFXButton t1 = new JFXButton("acheter");

                t1.setStyle("-fx-font-weight: bold;");

                HBox hb2 = new HBox(t1);

                hb2.setLayoutX(80);
                hb2.setLayoutY(170);

                hb2.setStyle("-fx-background-color: #ea7066; ; -fx-background-radius: 0 0 10 0;");

                JFXButton t2 = new JFXButton("Modifier");
                t2.setStyle("-fx-font-weight: bold;");
                HBox hb3 = new HBox(t2);

                hb3.setLayoutX(20);
                hb3.setLayoutY(170);

                hb3.setStyle("-fx-background-color: #ea7066; ; -fx-background-radius: 0 0 10 0;");

                JFXButton t3 = new JFXButton("supprimer");

                t3.setStyle("-fx-font-weight: bold;");

                HBox hb4 = new HBox(t3);

                hb4.setLayoutX(100);
                hb4.setLayoutY(170);

                hb4.setStyle("-fx-background-color: #ea7066; ; -fx-background-radius: 0 0 10 0;");

                if (p3.getOwner() != getUserId()) {
                    pane2.getChildren().addAll(hb2);
                    System.out.println("moch taah");
                } else {
                    pane2.getChildren().addAll(hb4);
                    pane2.getChildren().addAll(hb3);
                }

                Text nomt = new Text("Nom : ");
                Label nom = new Label(p3.getNom());
                Text prixt = new Text("prix : ");
                Label prix = new Label(String.valueOf(p3.getPrix()) + " DT");

                nomt.setLayoutX(275);
                nomt.setLayoutY(160);
                nom.setLayoutX(325);
                nom.setLayoutY(145);
                prixt.setLayoutX(275);
                prixt.setLayoutY(180);
                prix.setLayoutX(325);
                prix.setLayoutY(165);
                nomt.setStyle("-fx-font-weight: bold;-fx-fill : #ce3b67");
                prixt.setStyle("-fx-font-weight: bold;-fx-fill : #ce3b67");
                t3.setOnMouseClicked((MouseEvent event) -> {

                    service_pr.supprimerProduit(p3.getId());
                    loadView("/GUI/produits.fxml");

                });
                t1.setOnMouseClicked((MouseEvent event) -> {
                    if (Produit.getPanier().contains(p3)) {
                        for (int i = 0; i < Produit.getPanier().size(); i++) {
                            if (Produit.getPanier().get(i).getId() == p3.getId()) {
                                if (Produit.getPanier().get(i).getQuantity() < p3.getQuantity()) {
                                    Verif = Produit.setPanier(p3);
                                    System.out.println("aaaaaaaaaaa");

                                    Notifications n = Notifications.create().title("Notification")
                                            .text("Votre produit \"" + p3.getNom() + "\" a été ajouter au panier")
                                            .graphic(null)
                                            .position(Pos.BASELINE_LEFT)
                                            .onAction(new EventHandler<ActionEvent>() {

                                                public void handle(ActionEvent event) {
                                                    System.out.println("notifocation");
                                                }
                                            });
                                    n.showInformation();

                                    ListeCommande();
                                    getShowPane();

                                } else {
                                    Notifications n = Notifications.create().title("Notification")
                                            .text("Vous avez depasser le Stock du fournisseur on va lui notifier")
                                            .graphic(null)
                                            .position(Pos.BASELINE_LEFT)
                                            .onAction(new EventHandler<ActionEvent>() {

                                                public void handle(ActionEvent event) {
                                                    System.out.println("notification");
                                                }
                                            });
                                    n.showError();

                                }
                            }
                        }
                    } else {
                        if (p3.getQuantity() > 0) {
                            Verif = Produit.setPanier(p3);
                            System.out.println(p3.getQuantity());
                            ListeCommande();
                            getShowPane();
                            Notifications n = Notifications.create().title("Notification")
                                    .text("Votre produit \"" + p3.getNom() + "\" a été ajouter au panier")
                                    .graphic(null)
                                    .position(Pos.BASELINE_LEFT)
                                    .onAction(new EventHandler<ActionEvent>() {

                                        public void handle(ActionEvent event) {
                                            System.out.println("notifocation");
                                        }
                                    });
                            n.showInformation();

                        } else {
                            Notifications n = Notifications.create().title("Notification")
                                    .text("Vous avez depasser le Stock du fournisseur on va lui notifier")
                                    .graphic(null)
                                    .position(Pos.BASELINE_LEFT)
                                    .onAction(new EventHandler<ActionEvent>() {

                                        public void handle(ActionEvent event) {
                                            System.out.println("notifocation");
                                        }
                                    });
                            n.showError();

                        }

                    }
                });

                String A = p3.getImage_id();
                A = "C:\\xampp\\htdocs\\Pi-Dev-Web\\web\\shopImg\\" + A;
                File F1 = new File(A);
                Image image2 = new Image(F1.toURI().toString());

                ImageView image = new ImageView();
                image.setFitWidth(140);
                image.setFitHeight(130);
                image.setStyle("-fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.5), 8, 0, 0, 0);");

                image.setImage(image2);
                image.setLayoutX(30);
                image.setLayoutY(-45);
                pane2.getChildren().add(image);

                pane.getChildren().addAll(pane2, nomt, prixt, nom, prix);
            }

            if (k == 3) {
                Pane pane2 = new Pane();
                pane2.setLayoutX(475);
                pane2.setLayoutY(50);
                pane2.setPrefWidth(pane2.getWidth() + 200);
                pane2.setPrefHeight(pane2.getHeight() + 200);
                //pane2.setStyle("-fx-background-radius: 50;");
                pane2.setStyle(" -fx-border-radius: 10;-fx-border-color: #383d3b ;-fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.5), 8, 0, 0, 0); ");

                JFXButton t1 = new JFXButton("acheter");

                t1.setStyle("-fx-font-weight: bold;");

                HBox hb2 = new HBox(t1);

                hb2.setLayoutX(80);
                hb2.setLayoutY(170);

                hb2.setStyle("-fx-background-color: #ea7066; ; -fx-background-radius: 0 0 10 0;");

                JFXButton t2 = new JFXButton("details");
                t2.setStyle("-fx-font-weight: bold;");
                HBox hb3 = new HBox(t2);

                hb3.setLayoutX(20);
                hb3.setLayoutY(170);

                hb3.setStyle("-fx-background-color: #ea7066; ; -fx-background-radius: 0 0 10 0;");

                JFXButton t3 = new JFXButton("supprimer");

                t3.setStyle("-fx-font-weight: bold;");

                HBox hb4 = new HBox(t3);

                hb4.setLayoutX(100);
                hb4.setLayoutY(170);

                hb4.setStyle("-fx-background-color: #ea7066; ; -fx-background-radius: 0 0 10 0;");

                if (p3.getOwner() != getUserId()) {
                    pane2.getChildren().addAll(hb2);
                    System.out.println("moch taah");
                } else {
                    pane2.getChildren().addAll(hb4);
                    pane2.getChildren().addAll(hb3);

                }

                Text nomt = new Text("Nom : ");
                Label nom = new Label(p3.getNom());
                Text prixt = new Text("prix : ");
                Label prix = new Label(String.valueOf(p3.getPrix()) + " DT");

                nomt.setLayoutX(500);
                nomt.setLayoutY(160);
                nom.setLayoutX(550);
                nom.setLayoutY(145);
                prixt.setLayoutX(500);
                prixt.setLayoutY(180);
                prix.setLayoutX(550);
                prix.setLayoutY(165);
                nomt.setStyle("-fx-font-weight: bold;-fx-fill : #ce3b67");
                prixt.setStyle("-fx-font-weight: bold;-fx-fill : #ce3b67");
                t3.setOnMouseClicked((MouseEvent event) -> {

                    service_pr.supprimerProduit(p3.getId());
                    loadView("/GUI/produits.fxml");

                });
                t1.setOnMouseClicked((MouseEvent event) -> {
                    if (Produit.getPanier().contains(p3)) {
                        for (int i = 0; i < Produit.getPanier().size(); i++) {
                            if (Produit.getPanier().get(i).getId() == p3.getId()) {
                                if (Produit.getPanier().get(i).getQuantity() < p3.getStock()) {
                                    Verif = Produit.setPanier(p3);
                                    System.out.println("aaaaaaaaaaa");

                                    Notifications n = Notifications.create().title("Notification")
                                            .text("Votre produit \"" + p3.getNom() + "\" a été ajouter au panier")
                                            .graphic(null)
                                            .position(Pos.BASELINE_LEFT)
                                            .onAction(new EventHandler<ActionEvent>() {

                                                public void handle(ActionEvent event) {
                                                    System.out.println("notifocation");
                                                }
                                            });
                                    n.showInformation();

                                    ListeCommande();
                                    getShowPane();

                                } else {
                                    Notifications n = Notifications.create().title("Notification")
                                            .text("Vous avez depasser le Stock du fournisseur on va lui notifier")
                                            .graphic(null)
                                            .position(Pos.BASELINE_LEFT)
                                            .onAction(new EventHandler<ActionEvent>() {

                                                public void handle(ActionEvent event) {
                                                    System.out.println("notifocation");
                                                }
                                            });
                                    n.showError();

                                }
                            }
                        }
                    } else {
                        if (p3.getStock() > 0) {
                            Verif = Produit.setPanier(p3);
                            System.out.println(p3.getStock());
                            ListeCommande();
                            getShowPane();
                            Notifications n = Notifications.create().title("Notification")
                                    .text("Votre produit \"" + p3.getNom() + "\" a été ajouter au panier")
                                    .graphic(null)
                                    .position(Pos.BASELINE_LEFT)
                                    .onAction(new EventHandler<ActionEvent>() {

                                        public void handle(ActionEvent event) {
                                            System.out.println("notifocation");
                                        }
                                    });
                            n.showInformation();

                        } else {
                            Notifications n = Notifications.create().title("Notification")
                                    .text("Vous avez depasser le Stock du fournisseur on va lui notifier")
                                    .graphic(null)
                                    .position(Pos.BASELINE_LEFT)
                                    .onAction(new EventHandler<ActionEvent>() {

                                        public void handle(ActionEvent event) {
                                            System.out.println("notifocation");
                                        }
                                    });
                            n.showError();

                        }

                    }
                });

                String A = p3.getImage_id();
                A = "C:\\xampp\\htdocs\\Pi-Dev-Web\\web\\shopImg\\" + A;
                File F1 = new File(A);
                Image image2 = new Image(F1.toURI().toString());

                ImageView image = new ImageView();
                image.setFitWidth(140);
                image.setFitHeight(130);
                image.setStyle("-fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.5), 8, 0, 0, 0);");

                image.setImage(image2);
                image.setLayoutX(30);
                image.setLayoutY(-45);
                pane2.getChildren().add(image);

                pane.getChildren().addAll(pane2, nomt, prixt, nom, prix);

            }

            k++;

        }
        return pane;
    }

    private Object loadView(String path) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource((path)));
        AnchorPane parentContent = null;
        try {
            parentContent = fxmlLoader.load();
        } catch (IOException ex) {
            Logger.getLogger(LayoutFrontController.class.getName()).log(Level.SEVERE, null, ex);
        }
        setNode(parentContent);
        System.gc();
        return fxmlLoader.getController();
    }

    private void setNode(Node node) {
        holderPane.getChildren().clear();
        holderPane.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.millis(1500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
    }

    public void ListeCommande() {
        ObservableList<Pane> refresh = FXCollections.observableArrayList();
        ListView_Produits1.setItems(refresh);
        // nombreproduits = nombreproduits + 1;
        nbrnotif.setText(String.valueOf(nombreproduits));

/////////////////panier ///////////////////////      
        ObservableList<Pane> Panes = FXCollections.observableArrayList();
        for (panier p3 : pan.ListPanierUser(getUserId())) {

            Pane pane = new Pane();
            pane.setStyle(" -fx-background-color: white");
            Pane pane2 = new Pane();
            pane2.setLayoutX(10);
            pane2.setLayoutY(10);
            pane2.setPrefWidth(pane2.getWidth() + 200);
            pane2.setPrefHeight(pane2.getHeight() + 80);

            //pane2.setStyle("-fx-background-radius: 50;");
            pane2.setStyle(" -fx-border-radius: 10 10 10 10;-fx-border-color: #383d3b ;");

            Pane panequantitet = new Pane();
            panequantitet.setLayoutX(100);
            panequantitet.setLayoutY(40);
            panequantitet.setPrefWidth(panequantitet.getWidth() + 160);
            panequantitet.setPrefHeight(panequantitet.getHeight() + 30);

            Text quan1 = new Text("Quantite : ");
            Label quant2 = new Label(String.valueOf(p3.getQuantite()));
            quan1.setLayoutX(10);
            quan1.setLayoutY(20);
            quant2.setLayoutX(90);
            quant2.setLayoutY(5);
            quan1.setStyle("-fx-font-weight: bold;-fx-fill : #d82819;-fx-font-size:15px;");
            quant2.setStyle("-fx-font-weight: bold;-fx-fill : #d82819;-fx-font-size:15px;");
            panequantitet.getChildren().addAll(quan1, quant2);

            Text nomt = new Text("Nom : ");
            Produit pname = new Produit();
            pname = service_pr.getAcualiteById(p3.getProduitid());
            Label nom = new Label(pname.getNom() + "");
            Text prixt = new Text("prix : ");
            Label prix = new Label(String.valueOf(p3.getPrix()) + " DT");
            nomt.setLayoutX(100);
            nomt.setLayoutY(20);
            nom.setLayoutX(150);
            nom.setLayoutY(10);
            prixt.setLayoutX(100);
            prixt.setLayoutY(35);
            prix.setLayoutX(150);
            prix.setLayoutY(25);
            nomt.setStyle("-fx-font-weight: bold;-fx-fill : #ce3b67");
            prixt.setStyle("-fx-font-weight: bold;-fx-fill : #ce3b67");
            pane2.getChildren().addAll(nomt, prixt, nom, prix, panequantitet);
            Panes.add(pane2);

        }

        if (Verif || Produit.getPanier().size() == 1) {

            ListView_Produits1.setPrefHeight(ListView_Produits1.getHeight() + 25);
        }
        ListView_Produits1.setItems(Panes);
    }

    private void tous(ActionEvent event) {
        System.out.println("Enter Load produitsf xml");
        loadView("/GUI/produits.fxml");
    }


    @FXML
    private void add(ActionEvent event) {
    }

    @FXML
    private void upload(ActionEvent event) {
    }

    @FXML
    private void panierhide(MouseEvent event) {
    }

    @FXML
    private void paniershow(MouseEvent event) {
    }

    @FXML
    private void paniershowall(MouseEvent event) {
    }


    @FXML
    private void hidepanier2(MouseEvent event) {
    }

    @FXML
    private void showpanier2(MouseEvent event) {
    }

    @FXML
    private void tous(MouseEvent event) {
    }

    @FXML
    private void paypal(ActionEvent event) {
                
        
    }

}
