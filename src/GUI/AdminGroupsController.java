/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Hsine
 */
public class AdminGroupsController implements Initializable {

    @FXML
    private AnchorPane holderPane;
    @FXML
    private JFXButton btnWork1;
    @FXML
    private JFXButton btnWork2;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void switchWork1(ActionEvent event) throws IOException {
         AnchorPane parentContent = FXMLLoader.load(getClass().getResource(("../GUI/affichageGroupe.fxml")));
        holderPane.getChildren().setAll(parentContent);
    }

    @FXML
    private void switchWork2(ActionEvent event) throws IOException {
         AnchorPane parentContent = FXMLLoader.load(getClass().getResource(("../GUI/SignalGroup.fxml")));
        holderPane.getChildren().setAll(parentContent);
    }
    
}
