/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import static Core.Controller.holderPane;
import Core.LayoutFrontController;
import Entities.Bonplan;
import Entities.Categorie;
import Entities.User;
import Service.BonPlanService;
import Service.CategorieService;
import Service.UserService;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.animation.FadeTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.controlsfx.control.Rating;
import org.controlsfx.*;
import utils.DataSource;

/**
 * FXML Controller class
 *
 * @author marie
 */
public class AjoutBonPlanController implements Initializable {
   
    @FXML
    private TextField name;
    @FXML
    private TextField adresse;
    @FXML
    private ComboBox<String> categorie;
    @FXML
    private TextField phone;
     @FXML
    private ImageView image;
    @FXML
    private ImageView image1;
    @FXML
    private TextField description;
    @FXML
    private Button importer;
    @FXML
    private TextField prix;
    @FXML
    private TextField note;
    @FXML
    private Rating etoile;
    @FXML
    private Button annulerBonPlan;
    @FXML
    private Button ajouterBonPlan;
       @FXML
    private Button backbtn;
     String chaine3="";
      @FXML
    private Label msg;


    /**
     * Initializes the controller class.
     */
                 Connection c= DataSource.getInstance().getConnection();
  

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        
        
        ////////////
                final FileChooser fileChooser = new FileChooser();
       importer.setOnAction(new EventHandler<ActionEvent>() {
                          @Override
                public void handle(final ActionEvent e) {
                    setExtFilters(fileChooser);
                    File file = fileChooser.showOpenDialog(null);
                    
                    if (file != null) {
                        openNewImageWindow(file);
                    }
                    
            }

            
        
        });
       try {
             String req = "SELECT * FROM categorie";
            Statement st;
            st = c.createStatement();
            ResultSet rs = st.executeQuery(req);
             while (rs.next()) {
                 Categorie cat = new Categorie();
                 cat.setId(rs.getInt(1));
                 cat.setType(rs.getString(2));

                 categorie.getItems().add(cat.getType());
             }
             
            

        } catch (SQLException ex) {
            Logger.getLogger(AjoutBonPlanController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        ////////////////
           /* CategorieService Categorieservice = new CategorieService();

        ArrayList<Categorie> listeCatgeorie = Categorieservice.AfficherCategorie();
        for (Categorie cat:listeCatgeorie){
            categorie.getItems().add(cat.getType());
        }*/
        //////////////
       
         /**
         * ********** Animation sur les bouttons ***********************
         */
        DropShadow shadow = new DropShadow();
        importer.addEventHandler(MouseEvent.MOUSE_ENTERED, (MouseEvent e) -> {
            importer.setEffect(shadow);
        });
        importer.addEventHandler(MouseEvent.MOUSE_EXITED, (MouseEvent e) -> {
            importer.setEffect(null);
        });
        annulerBonPlan.addEventHandler(MouseEvent.MOUSE_ENTERED, (MouseEvent e) -> {
            annulerBonPlan.setEffect(shadow);
        });
        annulerBonPlan.addEventHandler(MouseEvent.MOUSE_EXITED, (MouseEvent e) -> {
            annulerBonPlan.setEffect(null);
        });
        ajouterBonPlan.addEventHandler(MouseEvent.MOUSE_ENTERED, (MouseEvent e) -> {
            ajouterBonPlan.setEffect(shadow);
        });
       ajouterBonPlan.addEventHandler(MouseEvent.MOUSE_EXITED, (MouseEvent e) -> {
           ajouterBonPlan.setEffect(null);
        });
       

     //*********** rating 
      etoile.ratingProperty().addListener(new ChangeListener<Number>() {
            @Override public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
              msg.setText(t1.toString() );
            }
        });                   
     ///***************
	
    }    

    @FXML
    private void AnnulerBonPlan(ActionEvent event) {
               // loadView("../GUI/ListBonPlan.fxml");
                name.setText("");
                adresse.setText("");
                prix.setText("");
                note.setText("");
                phone.setText("");
                

    }
    
    @FXML
    private void backList(ActionEvent event) {
        loadView("../GUI/ListBonPlan.fxml");

    }
    @FXML
    private void AjouterBonPlan(ActionEvent event) {
         if (name.getText().isEmpty() || phone.getText().isEmpty() 
                || adresse.getText().isEmpty() ) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Erreur");
            alert.setHeaderText(null);
            alert.setContentText("Il faut remplir les champs obligatoires ");
            alert.showAndWait();
        }
        else if (validatePrix()&validateMobileNo())
        {
          
             
            Bonplan  bonPlan = new Bonplan (name.getText(), adresse.getText(), phone.getText()
                    , Integer.parseInt(note.getText()), description.getText(), (int) etoile.getRating(),
                    Double.parseDouble(prix.getText()),chaine3) ;
            
            bonPlan.setDatePublication(new Date());
            bonPlan.setCategorie(categorie.getValue());
            //bonPlan.setEtoile(Integer.parseInt(msg.getText()));
            BonPlanService bonplanService= new BonPlanService();
           
            bonplanService.ajouterBonPlan(bonPlan);
                        System.out.println("vbbbbbbbbbbbbbbbbbbb");

             Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Bon plan  ");
            alert.setHeaderText(null);
            alert.setContentText("Bon plan  ajouté !!  ");
            alert.showAndWait();
        }
    }
     private boolean validateMobileNo(){
        Pattern p = Pattern.compile("216[1-9][0-9]{7}");
        Matcher m = p.matcher(phone.getText());
        if(m.find() && m.group().equals(phone.getText())){
            return true;
        }else{
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Syntaxe Numero de télephone");
                alert.setHeaderText(null);
                alert.setContentText("S'il vous plait saisir un numéro de télephone valide");
                alert.showAndWait();
            
            return false;            
        }
}
      ////////////////////////////////////////////////////////
     private void setExtFilters(FileChooser chooser){
        chooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Images", "*.*"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );
    }
         ///////////////////////////////////////////////////////
     private void openNewImageWindow(File file){
        Stage secondStage = new Stage();
        MenuBar menuBar = new MenuBar();
        Menu menuFile = new Menu("Fichier");
        
        MenuItem menuItem_Save1 = new MenuItem("Enregistrer");

        menuFile.getItems().addAll(menuItem_Save1);
        menuBar.getMenus().addAll(menuFile);
        
        Label name = new Label(file.getName());
        Image imageu = new Image(file.toURI().toString());
        ImageView image2 = new ImageView();
        image.setImage(imageu);
        
        menuItem_Save1.setOnAction(new EventHandler<ActionEvent>(){
         @Override
            public void handle(ActionEvent event) {

               secondStage.close();
            }
        
        });  
          final VBox vbox = new VBox();
        vbox.setAlignment(Pos.CENTER);
        vbox.setSpacing(10);
        vbox.setPadding(new Insets(0, 10, 0, 10));
        vbox.getChildren().addAll(name, image2);
         
        image2.setFitHeight(400);
        image2.setPreserveRatio(true);
        image2.setImage(imageu);
        image2.setSmooth(true);
        image2.setCache(true);
         
        Scene scene = new Scene(new VBox(),500, 500);
        ((VBox)scene.getRoot()).getChildren().addAll(menuBar, vbox);
         
        secondStage.setTitle(file.getName());
        secondStage.setScene(scene);
        secondStage.show();
         File source = new File(file.toString());
        
        chaine3=""+source;

}
     private boolean validatePrix(){
        Pattern p = Pattern.compile("[0-9]+");
        Matcher m = p.matcher(prix.getText());
        if(m.find() && m.group().equals(prix.getText())){
            return true;
        }else{
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle(" Syntaxe Prix");
                alert.setHeaderText(null);
                alert.setContentText("S'il vous plait saisir un prix valide");
                alert.showAndWait();
            
            return false;            
        }
        }
     
     /////////////////////
     private Object loadView(String path)
    {
        FXMLLoader fxmlLoader= new FXMLLoader(getClass().getResource((path)));
        AnchorPane parentContent = null;
        try {
            parentContent = fxmlLoader.load();
        } catch (IOException ex) {
            Logger.getLogger(LayoutFrontController.class.getName()).log(Level.SEVERE, null, ex);
        }
        setNode(parentContent);
        System.gc();
        return fxmlLoader.getController();
    }
     
      private void setNode(Node node) {
        holderPane.getChildren().clear();
        holderPane.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.millis(1500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
    }
      /////////////////////////
}
