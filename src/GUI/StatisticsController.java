/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import static Core.Controller.holderPane;
import Core.LayoutFrontController;
import Entities.Bonplan;
import Service.BonPlanService;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author marie
 */
public class StatisticsController implements Initializable {

    @FXML
    private BarChart<String, Integer> barchart;
    @FXML
    private Label titreBon;
    @FXML
    private Label titreCat;
    @FXML
    private Button btnRetour;

        BonPlanService ps = new BonPlanService();
    @FXML
    private AnchorPane holderPane;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
            XYChart.Series<String, Integer> series11 = new XYChart.Series<>();
         // List<Programme> prog = new ArrayList<Programme>();
          
       //   ps.getAll();
          
                          // System.out.println("ana houniii "+ps.getAll());

       
       for (Bonplan t1 : ps.list()) {
         
                XYChart.Data<String, Integer> data11;
          
                    try {
                       //System.out.println(ps.calculernbruserparprog(t1.getId_prog()));
                        data11 = new XYChart.Data<String, Integer>(t1.getCategorie(), ps.calculerNbreBonPlanParCategorie(t1.getCategorie()));
                        System.out.println("******* nb"+ps.calculerNbreBonPlanParCategorie(t1.getCategorie()));
                     series11.getData().add(data11);
                    } catch (SQLException ex) {
                        Logger.getLogger(StatisticsController.class.getName()).log(Level.SEVERE, null, ex);
                   
             }}
           
          barchart.getData().addAll(series11);
          
         /* ObservableList<PieChart.Data> pieChartData= FXCollections.observableArrayList
                  (new PieChart.Data("cars", 13),
                  new PieChart.Data("Bikes",14));
          pichart.setData(pieChartData);*/
    }    

    @FXML
    private void retourAccueil (ActionEvent event) throws IOException{
          FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../GUI/AdminChoixBP.fxml"));
        AnchorPane parentContent = fxmlLoader.load();
        holderPane.getChildren().clear();
        holderPane.getChildren().setAll(parentContent);

    }

}
