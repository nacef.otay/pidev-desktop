/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import static Core.Controller.holderPane;
import static Core.Controller.getUserId;

import Core.LayoutFrontController;
import Service.UserService;
import com.jfoenix.controls.JFXButton;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author Hsine
 */
public class DrawerController implements Initializable {

    @FXML
    private AnchorPane box;
    @FXML
    private ImageView imgUser;
    @FXML
    private Label labelUser;
    @FXML
    private JFXButton notification;
    @FXML
    private JFXButton listeDesAnnonce;
    @FXML
    private JFXButton AjoutAnnonce;
    @FXML
    private JFXButton statRec;
    @FXML
    private JFXButton deconnexion;
    private int userId;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        UserService u=new UserService();
        System.out.println("55555555");
        String A =u.getUserById(getUserId()).getImage();
        
        A = "C:\\xampp\\htdocs\\Pidev Desktop\\src\\Images\\" + A;
        File F1 = new File(A);
        Image image2 = new Image(F1.toURI().toString());
        imgUser.setImage(image2);

    }    

    @FXML
    private void notification(ActionEvent event) {
                loadView("/GUI/stat.fxml");

    }

    @FXML
    private void listeDesAnnonce(ActionEvent event) throws IOException {
/*Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/FrontGroupe.fxml")));

                        Stage stage = new Stage();
                        stage.setScene(scene);
                        stage.show();*/
        loadView("/GUI/FrontGroupe.fxml");
    }

    @FXML
    private void AjoutAnnonce(ActionEvent event) throws IOException {
/*Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/ajouterGroupe.fxml")));

                        Stage stage = new Stage();
                        stage.setScene(scene);
                        stage.show();*/loadView("/GUI/ajouterGroupe.fxml");
    }

    private void chatRoom(ActionEvent event) {
         loadView("/GUI/SignalGroup.fxml");
    }

    @FXML
    private void makeStat(ActionEvent event) {
                 loadView("/GUI/affichafeGroupe.fxml");

    }

    @FXML
    private void makeDisconnect(ActionEvent event) {
    }
     private void setNode(Node node) {
        holderPane.getChildren().clear();
        holderPane.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.millis(1500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
    }
    
    private Object loadView(String path)
    {
        FXMLLoader fxmlLoader= new FXMLLoader(getClass().getResource((path)));
        AnchorPane parentContent = null;
        try {
            parentContent = fxmlLoader.load();
        } catch (IOException ex) {
            Logger.getLogger(LayoutFrontController.class.getName()).log(Level.SEVERE, null, ex);
        }
        setNode(parentContent);
        System.gc();
        return fxmlLoader.getController();
    }
    
}
