/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import static Core.Controller.holderPane;
import Core.LayoutFrontController;
import Entities.category;
import Service.ServiceCategory;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;

/**
 * FXML Controller class
 *
 * @author ZerOo
 */
public class CategorieBackController implements Initializable {

    @FXML
    private AnchorPane holderPane;
    @FXML
    private TableView<?> CategoryList;
    @FXML
    private TableColumn<?, ?> nom;
    @FXML
    private JFXTextField nomCt;
    
    
public void load()
{
    ServiceCategory cs=new ServiceCategory();
        List<category> l=cs.ListCategory();
        ObservableList obs=FXCollections.observableList(l);
        CategoryList.setItems(obs);
        nom.setCellValueFactory(new PropertyValueFactory<>("category"));
    
}
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       load();
    }    

    @FXML
    private void supprimer(ActionEvent event) {
        category c=(category) CategoryList.getSelectionModel().getSelectedItem();
        if(c==null)
        {
              Notifications n = Notifications.create().title("Notification")
                                    .text("Aucune categorie selectionnée")
                                    .graphic(null)
                                    .position(Pos.BASELINE_LEFT)
                                    .onAction(new EventHandler<ActionEvent>() {
                                        
                                        public void handle(ActionEvent event) {
                                            System.out.println("notifocation");
                                        }
                                    });
                            n.showError();
        }
        else
        {
             ServiceCategory cs=new ServiceCategory();
            
               Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                     alert.setTitle(" ");
                     alert.setHeaderText("Etes-vous sur de supprimer cette categorie ?");
                     Optional<ButtonType> result = alert.showAndWait();
                    if (result.get() == ButtonType.OK) {
                    cs.supprimerCategorie(c.getId());
                    CategoryList.getItems().remove(c);
                      Notifications notificationBuilder= Notifications.create()
               
                 .text("Club supprimé  avec succés")
                 .graphic(null)
                 .hideAfter(Duration.seconds(5))
                 .position(Pos.BOTTOM_RIGHT)
                 .onAction(new EventHandler<ActionEvent>() {
             @Override
             public void handle(ActionEvent event) {
                 System.out.println("Clicked on notification");
             }
         });
         }
    }
        }
    
        private Object loadView(String path) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource((path)));
        AnchorPane parentContent = null;
        try {
            parentContent = fxmlLoader.load();
        } catch (IOException ex) {
            Logger.getLogger(LayoutFrontController.class.getName()).log(Level.SEVERE, null, ex);
        }
        setNode(parentContent);
        System.gc();
        return fxmlLoader.getController();
    }
        
            private void setNode(Node node) {
        holderPane.getChildren().clear();
        holderPane.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.millis(1500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
    }
        
    

    @FXML
    private void ajout(ActionEvent event) {
        ServiceCategory cs=new ServiceCategory();
       
        if(cs.findCategoryByName(nomCt.getText()))
        {
             Notifications n = Notifications.create().title("Notification")
                                    .text("Votre produit \"" + nomCt.getText() + "\" déja existant")
                                    .graphic(null)
                                    .position(Pos.BASELINE_LEFT)
                                    .onAction(new EventHandler<ActionEvent>() {
                                        
                                        public void handle(ActionEvent event) {
                                            System.out.println("notifocation");
                                        }
                                    });
                            n.showError();
        }
        else{
       category cat= new category(nomCt.getText());
      
       cs.ajouterCategory(cat);
load();        Notifications n = Notifications.create().title("Notification")
                                    .text("Votre produit \"" + nomCt.getText() + "\" a été ajouter avec succés")
                                    .graphic(null)
                                    .position(Pos.BASELINE_LEFT)
                                    .onAction(new EventHandler<ActionEvent>() {
                                        
                                        public void handle(ActionEvent event) {
                                            System.out.println("notifocation");
                                        }
                                    });
                            n.showInformation();
    }
    
}

    @FXML
    private void stat(ActionEvent event) {
        
        loadView("/GUI/StatistiqueProduit.fxml");
        
    }
}