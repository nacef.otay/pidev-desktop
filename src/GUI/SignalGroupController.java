/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import static Core.Controller.getUserId;
import static Core.Controller.holderPane;
import Core.LayoutFrontController;
import Entities.SignalGroup;
import Service.GroupeService;
import Service.SignalGroupService;
import Service.UserService;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author Hsine
 */
public class SignalGroupController implements Initializable {

    SignalGroupService service = new SignalGroupService();
    GroupeService service1 = new GroupeService();
    UserService service2 = new UserService();

    @FXML
    private ListView<Pane> notificationFavoris;
    @FXML
    private Pane notification;
    @FXML
    private Label nbrnotif;
    @FXML
    private JFXListView<Pane> allFvoris;
    int i = 0;
    @FXML
    private AnchorPane hsan;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ObservableList<SignalGroup> listNotif = FXCollections.observableArrayList();
        for (SignalGroup p : service.selectReclamationNotChecked()) {

            listNotif.add(p);
        }

        System.out.println("Reclamations   " + listNotif);
        ObservableList<SignalGroup> listCheked = FXCollections.observableArrayList();
        for (SignalGroup p : service.selectReclamationChecked()) {
            listCheked.add(p);

        };

        nbrnotif.setText(String.valueOf(listNotif.size()));

        ObservableList<Pane> panes = FXCollections.observableArrayList();
        System.out.println("++++++++++   " + getUserId());

        for (int j = 0; j < listCheked.size(); j++) {

            SignalGroup get = listCheked.get(j);

            Pane pane2 = new Pane();
            pane2.setLayoutX(10);
            pane2.setLayoutY(10);
            pane2.setPrefWidth(pane2.getWidth() + 350);
            pane2.setPrefHeight(pane2.getHeight() + 105);
            pane2.setStyle("-fx-background-radius: 10 10 10 10;");

            ImageView image_p = new ImageView();
            String A = service1.getGroupeById(get.getIdGroup()).getImage();
            A = "C:\\xampp\\htdocs\\Pidev Desktop\\src\\Images\\" + A;
            File F1 = new File(A);
            Image image2 = new Image(F1.toURI().toString());
            image_p.setImage(image2);
            image_p.setLayoutX(20);
            image_p.setLayoutY(20);
            image_p.setFitHeight(image_p.getFitHeight() + 40);
            image_p.setFitWidth(image_p.getFitWidth() + 40);

            pane2.setStyle("-fx-border-color: #383d3b ;");
            Text t1 = new Text("L'utilisateur ");
            t1.setStyle("-fx-font-weight: bold;-fx-fill : #2a2af5");
            Text t2 = new Text("     a signalé le groupe ");
            t2.setStyle("-fx-font-weight: bold;-fx-fill : #2a2af5");
            Text t3 = new Text(" dont la cause est : ");
            t3.setStyle("-fx-font-weight: bold;-fx-fill : #2a2af5");
            System.out.println("test  " + get.getCause());
            Text tt1 = new Text();
            tt1.setText(service2.getUserById(get.getIdUser()).getNom());
            Text tt2 = new Text();
            tt2.setText(service1.getGroupeById(get.getIdGroup()).getNom());
            Text tt3 = new Text();
            tt3.setText(get.getCause());

            t1.setLayoutX(85);
            tt1.setLayoutX(160);
            t2.setLayoutX(180);
            tt2.setLayoutX(355);
            t3.setLayoutX(85);
            tt3.setLayoutX(250);
//        t4.setLayoutX(215);tt4.setLayoutX(215);
//        t5.setLayoutX(255);tt5.setLayoutX(255);
            ////
            t1.setLayoutY(35);
            tt1.setLayoutY(35);
            t2.setLayoutY(35);
            tt2.setLayoutY(35);
            t3.setLayoutY(60);
            tt3.setLayoutY(60);

            Button mailing = new Button("Traiter");
            mailing.setStyle("-fx-background-color: #12c536;");
            mailing.setLayoutX(225);
            mailing.setLayoutY(70);
            mailing.setOnMouseClicked((MouseEvent event) -> {
                service.sendMailToUser(get);
                try {
                    service.UpdateValiderRec(get);
                } catch (SQLException ex) {
                    Logger.getLogger(SignalGroupController.class.getName()).log(Level.SEVERE, null, ex);
                }
                loadView("/GUI/SignalGroup.fxml");

            });
            pane2.getChildren().addAll(image_p, t1, t2, t3, tt1, tt2, tt3, mailing);
            panes.add(pane2);

        }
        allFvoris.setItems(panes);
        notificationFavoris.setVisible(false);

        // TODO
    }

    @FXML
    private void notificationAffiche(MouseEvent event) throws SQLException {

        /*   for (SignalGroup s : service.selectAllReclamation()) {
            System.out.println("signal    " + service2.getUserById(service1.getAcualiteById(s.getIdGroup()).getIdUser()).getId());
            System.out.println("sig   " + getUserId());
            if (service2.getUserById(service1.getAcualiteById(s.getIdGroup()).getIdUser()).getId() == getUserId()) {*/
        if (i == 0) {
            ObservableList<SignalGroup> list = FXCollections.observableArrayList();
            for (SignalGroup p : service.selectReclamationNotChecked()) {
                System.out.println("signal    " + service2.getUserById(service1.getGroupeById(p.getIdGroup()).getIdUser()).getRoles());

                list.add(p);

            };
            ObservableList<Pane> panesnotif = FXCollections.observableArrayList();

            for (int j = 0; j < list.size(); j++) {
                SignalGroup get = list.get(j);

                Pane pane3 = new Pane();
                pane3.setLayoutX(10);
                pane3.setLayoutY(10);
                pane3.setPrefWidth(pane3.getWidth() + 50);
                pane3.setPrefHeight(pane3.getHeight() + 20);
                pane3.setStyle("-fx-border-color: #383d3b ; -fx-background-radius: 10; -fx-border-radius: 5 ");
                Text tnom = new Text("Signal sur :");
                Text tnom1 = new Text();
                tnom1.setText(service1.getGroupeById(get.getIdGroup()).getNom());
                tnom.setLayoutX(5);
                tnom.setLayoutY(15);
                tnom1.setLayoutX(95);
                tnom1.setLayoutY(15);
                pane3.getChildren().addAll(tnom, tnom1);
                panesnotif.add(pane3);

            }

            notificationFavoris.setItems(panesnotif);

            notificationFavoris.setVisible(true);
            for (SignalGroup p : service.selectAllReclamation()) {

                service.UpdateReclamation(p);
            };

            i++;

        } else {
            nbrnotif.setText(String.valueOf(0));
            notificationFavoris.setVisible(false);
            ObservableList<SignalGroup> list2 = FXCollections.observableArrayList();
            for (SignalGroup p : service.selectAllReclamation()) {
                list2.add(p);

            }
            ObservableList<Pane> panes = FXCollections.observableArrayList();
            for (int j = 0; j < list2.size(); j++) {
                SignalGroup get = list2.get(j);

                Pane pane2 = new Pane();
                pane2.setLayoutX(10);
                pane2.setLayoutY(10);
                pane2.setPrefWidth(pane2.getWidth() + 350);
                pane2.setPrefHeight(pane2.getHeight() + 105);
                pane2.setStyle("-fx-background-radius: 10 10 10 10;");

                ImageView image_p = new ImageView();
                String A = service1.getGroupeById(get.getIdGroup()).getImage();
                A = "C:\\wamp64\\www\\Pidev Desktop\\src\\Images\\" + A;
                File F1 = new File(A);
                Image image2 = new Image(F1.toURI().toString());
                image_p.setImage(image2);
                image_p.setLayoutX(20);
                image_p.setLayoutY(20);
                image_p.setFitHeight(image_p.getFitHeight() + 40);
                image_p.setFitWidth(image_p.getFitWidth() + 40);

                pane2.setStyle("-fx-border-color: #383d3b ;");
                Text t1 = new Text("L'utilisateur ");
                t1.setStyle("-fx-font-weight: bold;-fx-fill : #2a2af5");
                Text t2 = new Text("     a signalé le groupe ");
                t2.setStyle("-fx-font-weight: bold;-fx-fill : #2a2af5");
                Text t3 = new Text(" dont la cause est : ");
                t3.setStyle("-fx-font-weight: bold;-fx-fill : #2a2af5");
                Text tt1 = new Text();
                tt1.setText(service2.getUserById(get.getIdUser()).getNom());
                Text tt2 = new Text();
                tt2.setText(service1.getGroupeById(get.getIdGroup()).getNom());
                Text tt3 = new Text();
                tt3.setText(get.getCause());

                t1.setLayoutX(85);
                tt1.setLayoutX(160);
                t2.setLayoutX(180);
                tt2.setLayoutX(355);
                t3.setLayoutX(85);
                tt3.setLayoutX(250);
                t1.setLayoutY(35);
                tt1.setLayoutY(35);
                t2.setLayoutY(35);
                tt2.setLayoutY(35);
                t3.setLayoutY(60);
                tt3.setLayoutY(60);
                Button mailing1 = new Button("Traiter");
                mailing1.setStyle("-fx-background-color: #12c536;");
                mailing1.setLayoutX(225);
                mailing1.setLayoutY(70);
                mailing1.setOnMouseClicked((MouseEvent eventt) -> {
                    service.sendMailToUser(get);
                    try {
                        service.UpdateValiderRec(get);
                    } catch (SQLException ex) {
                        Logger.getLogger(SignalGroupController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    loadView("/GUI/SignalGroup.fxml");

                });

                pane2.getChildren().addAll(image_p, t1, t2, t3, tt1, tt2, tt3, mailing1);
                panes.add(pane2);
            }
            allFvoris.setItems(panes);
            i = 0;
        }
        /*   }
        }*/
    }

    private void setNode(Node node) {
        holderPane.getChildren().clear();
        holderPane.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.millis(1500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
    }

    private Object loadView(String path) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource((path)));
        AnchorPane parentContent = null;
        try {
            parentContent = fxmlLoader.load();
        } catch (IOException ex) {
            Logger.getLogger(LayoutFrontController.class.getName()).log(Level.SEVERE, null, ex);
        }
        setNode(parentContent);
        System.gc();
        return fxmlLoader.getController();
    }

    @FXML
    private void hsine(ActionEvent event) throws IOException {
        AnchorPane parentContent = FXMLLoader.load(getClass().getResource(("/GUI/GroupAdmin.fxml")));
        holderPane = hsan;
        holderPane.getChildren().setAll(parentContent);
    }

}
