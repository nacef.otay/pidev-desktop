/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Entities.Maisons_hotes;
import Entities.Reservation_hotes;

import Service.ReservationService;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javax.mail.internet.ParseException;
import utils.ReservationPdf;
import Entities.User;
import Service.MaisonHoteService;
import java.sql.SQLException;
import java.util.function.Predicate;
import javafx.collections.transformation.FilteredList;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Bhs Nada
 */
public class AfficherReservationController implements Initializable {

     private static User connectedUser;
   

    public static User getConnectedUser() {
        return connectedUser;
    }

  

  

    @FXML
    private Label resNum;
    @FXML
    private Label nomHote;
    @FXML
    private Label dateDeb;
    @FXML
    private Label dateFin;
    @FXML
    private Label nbPers;
    @FXML
    private Label prix;
    @FXML
    private TableView<Reservation_hotes> table_reserv;
   
    @FXML
    private TableColumn<Reservation_hotes, String> dateDebRes;
    @FXML
    private TableColumn<Reservation_hotes, String> DateFinRese;

    @FXML
    private TableColumn<Reservation_hotes, Double> prixRes;
    @FXML
    private TableColumn<Reservation_hotes, String> nbPersRsr;

    ReservationService reservationService = new ReservationService();
    Reservation_hotes reservation = new Reservation_hotes();
    @FXML
    private Button afficherRese;
    @FXML
    private Button telechargerPDF;
   Reservation_hotes reservation_hotes = new Reservation_hotes();
    @FXML
    private TextField tf_recherche;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        loadReservation();
        /**
         * * reservation Affichage **
         */
        resNum.setVisible(false);
        nomHote.setVisible(false);
        dateDeb.setVisible(false);
        dateFin.setVisible(false);
        nbPers.setVisible(false);
        prix.setVisible(false);
        /**
         * * ******* ********  **
         */
       // numeroRes.setCellValueFactory(new PropertyValueFactory<>("numero_reservation"));
        dateDebRes.setCellValueFactory(new PropertyValueFactory<>("date_debut"));
        DateFinRese.setCellValueFactory(new PropertyValueFactory<>("nb_jours"));
       // maisonHote.setCellValueFactory(new PropertyValueFactory<>("maisons_hotes_id"));
        prixRes.setCellValueFactory(new PropertyValueFactory<>("prix"));
        nbPersRsr.setCellValueFactory(new PropertyValueFactory<>("nb_personne"));

    }

    public void loadReservation() {
        ReservationService reservationService = new ReservationService();
        ArrayList<Reservation_hotes> listeHote = reservationService.AfficherReservation();

        ObservableList observableList = FXCollections.observableArrayList(listeHote);
        table_reserv.setItems(observableList);

    }

    /*       
 private void setcellValue() {
        table_reserv.setOnMousePressed(new EventHandler<javafx.scene.input.MouseEvent>() {
             
            @Override
            public void handle(javafx.scene.input.MouseEvent event) {
               Reservation_hotes p = table_reserv.getItems().get(table_reserv.getSelectionModel().getSelectedIndex());             
                resNum.setText(String.valueOf(p.getNumero_reservation()));  
                nomHote.setText(String.valueOf(p.getMaisons_hotes_id().getNom()));
                dateDeb.setText(String.valueOf(p.getDate_debut())); 
                dateFin.setText(String.valueOf(p.getDate_fin()));
                nbPers.setText(String.valueOf(p.getNb_personne()));
                prix.setText(String.valueOf(p.getPrix()));
        
        
        
           }
        });
}
     */

    @FXML
    private void AfficherReservation(ActionEvent event) {

        reservation_hotes = table_reserv.getSelectionModel().getSelectedItem();
        resNum.setText(String.valueOf(reservation_hotes.getNumero_reservation()));
        //    nomHote.setText(String.valueOf(reservation_hotes.getMaisons_hotes_id().getNom()));
        dateDeb.setText(String.valueOf(reservation_hotes.getDate_debut()));
        dateFin.setText(String.valueOf(reservation_hotes.getNb_jours()));
        nbPers.setText(String.valueOf(reservation_hotes.getNb_personne()));
        prix.setText(String.valueOf(reservation_hotes.getPrix()));

        /**
         * * reservation Affichage **
         */
        resNum.setVisible(true);
        nomHote.setVisible(true);
        dateDeb.setVisible(true);
        dateFin.setVisible(true);
        nbPers.setVisible(true);
        prix.setVisible(true);
        /********************/

        
       

    }

    @FXML
    private void telecharger(ActionEvent event) throws SQLException {
        Reservation_hotes res = new Reservation_hotes();
        
       res = table_reserv.getSelectionModel().getSelectedItem();
                ReservationPdf pdf = new ReservationPdf();
                pdf.Reservation(
                       res.getNumero_reservation(),
                       res.getDate_debut(),
                       res.getNb_jours(),
                       res.getNb_personne(),
                       res.getPrix()
                );
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Information");
                alert.setHeaderText("Pdf Reservation ");
                alert.setContentText("votre pdf est telecharger avec succès");
                alert.showAndWait();
                if (Desktop.isDesktopSupported()) {
                  
                        File myFile = new File("C:\\xampp\\htdocs\\Pidev Desktop/Reservation" + res.getNumero_reservation() + ".pdf");
                    try {
                        Desktop.getDesktop().open(myFile);
                    } catch (IOException ex) {
                        Logger.getLogger(AfficherReservationController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                  
                }
            }
      
    
 private void FilterMaison() throws SQLException {
      ReservationService rs = new ReservationService();
        ArrayList AL = (ArrayList) rs.AfficherReservation();
        ObservableList OReservation = FXCollections.observableArrayList(AL);
        FilteredList<Reservation_hotes> filtred_c = new FilteredList<>(OReservation, e -> true);
        tf_recherche.setOnKeyReleased(e -> {
            tf_recherche.textProperty().addListener((observableValue, oldValue, newValue) -> {
                filtred_c.setPredicate((Predicate<? super Reservation_hotes>) cat -> {
                    if (newValue == null || newValue.isEmpty()) {
                        return true;
                    }
                    String toLowerCaseNewValue = newValue.toLowerCase();
                    if ((Integer.toString(cat.getNb_personne()).toLowerCase().contains(toLowerCaseNewValue)) || (Double.toString(cat.getPrix()).toLowerCase().contains(toLowerCaseNewValue))  ) {
                        return true;

                    }

                    return false;
                });
            });
        });
        table_reserv.setItems(filtred_c);
    }

}
