/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Entities.Maisons_hotes;
import Service.MaisonHoteService;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import utils.DataSource;

/**
 * FXML Controller class
 *
 * @author Bhs Nada
 */
public class ModifierMHoteController implements Initializable {

    @FXML
    private TextArea description;
    private Button importerBtn;

    @FXML
    private Button annulerBtn;
    @FXML
    private Button ModifierBtn;
    @FXML
    private TextField nom;
 
    @FXML
    private TextField mail;
    @FXML
    private TextField site;
    @FXML
    private TextField tel;
    @FXML
    private TextField gouvernorat;
    @FXML
    private TextField capacites;
    @FXML
    private TextField prix;
    @FXML
    private TextField Adresse;
    @FXML
    private TextField id;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        id.setVisible(false);
        
    }    

    @FXML
    private void AnnulerHôte(ActionEvent event) {
    }

    @FXML
    private void ModifierHôte(ActionEvent event) {
 
     String requete="SELECT * FROM maisons_hotes WHERE id='"
                +id.getText()+"'";
      Connection c= DataSource.getInstance().getConnection();
        Statement st;
        try {
            st = c.createStatement();
             ResultSet rs = st.executeQuery(requete);
             Maisons_hotes maison = new Maisons_hotes();
             
             
             maison.setId(Integer.parseInt(id.getText()));
             maison.setTel(Integer.parseInt(tel.getText()));
             maison.setCapacites(Integer.parseInt(capacites.getText()));
             maison.setPrix(Double.parseDouble(prix.getText()));
             maison.setNom(nom.getText());
            // maison.setPays(pays.getSelectionModel().getSelectedItem().toString());
             maison.setGouvernorat(gouvernorat.getText());
             maison.setMail(mail.getText());
             maison.setSite_web(site.getText());
             maison.setAdresse(Adresse.getText());
             maison.setDescription(description.getText());
             
             
             MaisonHoteService maisonHoteService = new MaisonHoteService();
             maisonHoteService.ModifierMaison(maison, maison.getId());
             System.out.println("   */*/*/* "+maison.getId());
              //***********************************
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Info.");
            alert.setHeaderText("Info.");
            alert.setContentText("la maison d'hôte "+maison.getNom()+" a été Modifiée avec succés");
            alert.show();
            //***********************************
             
           
             
             
             
             
             
        } catch (SQLException ex) {
            Logger.getLogger(ModifierMHoteController.class.getName()).log(Level.SEVERE, null, ex);
        }
           
    
    }
    //////////////////////////////////
    /*
     Getter && Setter
    */
    
    
    /////////////////////

    public TextArea getDescription() {
        return description;
    }

    public void setDescription(TextArea description) {
        this.description = description;
    }

    public Button getImporterBtn() {
        return importerBtn;
    }

    public void setImporterBtn(Button importerBtn) {
        this.importerBtn = importerBtn;
    }

   
    public Button getAnnulerBtn() {
        return annulerBtn;
    }

    public void setAnnulerBtn(Button annulerBtn) {
        this.annulerBtn = annulerBtn;
    }

    public Button getModifierBtn() {
        return ModifierBtn;
    }

    public void setModifierBtn(Button ModifierBtn) {
        this.ModifierBtn = ModifierBtn;
    }

    public TextField getNom() {
        return nom;
    }

    public void setNom(TextField nom) {
        this.nom = nom;
    }

  

    public TextField getMail() {
        return mail;
    }

    public void setMail(TextField mail) {
        this.mail = mail;
    }

    public TextField getSite() {
        return site;
    }

    public void setSite(TextField site) {
        this.site = site;
    }

    public TextField getTel() {
        return tel;
    }

    public void setTel(TextField tel) {
        this.tel = tel;
    }

    public TextField getGouvernorat() {
        return gouvernorat;
    }

    public void setGouvernorat(TextField gouvernorat) {
        this.gouvernorat = gouvernorat;
    }

    public TextField getCapacites() {
        return capacites;
    }

    public void setCapacites(TextField capacites) {
        this.capacites = capacites;
    }

    public TextField getPrix() {
        return prix;
    }

    public void setPrix(TextField prix) {
        this.prix = prix;
    }

    public TextField getAdresse() {
        return Adresse;
    }

    public void setAdresse(TextField Adresse) {
        this.Adresse = Adresse;
    }

    public TextField getId() {
        return id;
    }

    public void setId(TextField id) {
        this.id = id;
    }
    
}
