/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import static Core.Controller.holderPane;
import Entities.Groups;
import Entities.SignalGroup;
import Service.GroupeService;
import Service.SignalGroupService;
import Service.UserService;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import com.sun.prism.impl.Disposer;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.animation.FadeTransition;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.util.Callback;
import javafx.util.Duration;
import javax.naming.Reference;

/**
 * FXML Controller class
 *
 * @author Hsine
 */
public class AfficheGroupeController implements Initializable {

    SignalGroupService service = new SignalGroupService();
    GroupeService service1 = new GroupeService();
    UserService service2 = new UserService();
    @FXML
    private TableView<Groups> table_view;
    @FXML
    private TableColumn<Groups, String> titre_col;

    @FXML
    private TableColumn<Groups, String> description_col;

    @FXML
    private TableColumn<Groups, Date> date_fin_col;
    @FXML
    private TableColumn<Groups, String> nbr_sig;
    @FXML
    private ListView<?> notificationFavoris;
    @FXML
    private Pane notification;
    @FXML
    private Label nbrnotif;
    @FXML
    private AnchorPane hsine;
    @FXML
    private TextField recherche;

    ObservableList<Groups> list;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        list = FXCollections.observableArrayList();
        ObservableList<Groups> list1 = FXCollections.observableArrayList();

        list = service1.AfficherAllGroups();
        System.out.println("ok");
        titre_col.setCellValueFactory(new PropertyValueFactory<>("nom"));
        description_col.setCellValueFactory(new PropertyValueFactory<>("description"));
        date_fin_col.setCellValueFactory(new PropertyValueFactory<>("date_de_creation"));
        nbr_sig.setCellValueFactory(new PropertyValueFactory<>("nbr_signal"));
        /* TableColumn col_action = new TableColumn<>("Supprimer");

        table_view.getColumns().add(col_action);
        TableColumn col_modifier = new TableColumn<>("Modifier");
        table_view.getColumns().add(col_modifier);*/

        recherche.setOnKeyReleased(e -> {
            recherche.textProperty().addListener((observableValue, oldValue, newValue) -> {
                System.out.println(newValue);
                if (newValue.isEmpty()) {
                    table_view.setItems(list);

                } else {
                    List<Groups> obsRr = list.stream().filter((o) -> (o.getNom().toLowerCase().contains(newValue.toLowerCase()))).collect((Collectors.toList()));
                    obsRr.forEach(o -> {
                        System.out.println("processed list, only even numbers: " + o);
                    });
                    ObservableList<Groups> sortedList = FXCollections.observableArrayList(obsRr);
                    table_view.setItems(sortedList);
                }

//                 
            });

        });
        table_view.setItems(list);

        /*  col_action.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<Disposer.Record, Boolean>, ObservableValue<Boolean>>() {

            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Disposer.Record, Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });

        //Adding the Button to the cell
        col_action.setCellFactory(
                new Callback<TableColumn<Disposer.Record, Boolean>, TableCell<Disposer.Record, Boolean>>() {

            @Override
            public TableCell<Disposer.Record, Boolean> call(TableColumn<Disposer.Record, Boolean> p) {
                return new Controller.ButtonCell();
            }

        });
        

        col_modifier.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<Disposer.Record, Boolean>, ObservableValue<Boolean>>() {

            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Disposer.Record, Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });

        //Adding the Button to the cell
        col_modifier.setCellFactory(
                new Callback<TableColumn<Disposer.Record, Boolean>, TableCell<Disposer.Record, Boolean>>() {

            @Override
            public TableCell<Disposer.Record, Boolean> call(TableColumn<Disposer.Record, Boolean> p) {
                return new Controller.ButtonCell2();
            }

        });*/
        ObservableList<SignalGroup> listNotif = FXCollections.observableArrayList();
        for (SignalGroup p : service.selectReclamationNotChecked()) {

            listNotif.add(p);
        }

        System.out.println("Reclamations   " + listNotif);
        ObservableList<SignalGroup> listCheked = FXCollections.observableArrayList();
        for (SignalGroup p : service.selectReclamationChecked()) {
            listCheked.add(p);

        };

        nbrnotif.setText(String.valueOf(listNotif.size()));
        notificationFavoris.setVisible(false);

    }

    private void setNode(Node node) {
        holderPane.getChildren().clear();
        holderPane.getChildren().add((Node) node);

        FadeTransition ft = new FadeTransition(Duration.millis(1500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
    }

    @FXML
    private void notificationAffiche(MouseEvent event) throws IOException {
        AnchorPane parentContent = FXMLLoader.load(getClass().getResource(("/GUI/SignalGroup.fxml")));
        holderPane = hsine;
        holderPane.getChildren().setAll(parentContent);
    }

    @FXML
    private void hsane(ActionEvent event) throws IOException {
        AnchorPane parentContent = FXMLLoader.load(getClass().getResource(("/GUI/GroupAdmin.fxml")));
        holderPane = hsine;
        holderPane.getChildren().setAll(parentContent);
    }

   

    @FXML
    private void supp(ActionEvent event) {
        Groups g=table_view.getSelectionModel().getSelectedItem();
        GroupeService ser=new GroupeService();
        int idg=g.getId();
        ser.supprimerGroupe(g);
        table_view.getItems().removeAll(table_view.getSelectionModel().getSelectedItem());
        table_view.getSelectionModel().select(null);
    }

}
