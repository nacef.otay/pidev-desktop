/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Core.Controller;
import static Core.Controller.holderPane;
import Entities.AvisEvenement;
import Entities.Evenement;
import Service.AvisEvenementService;
import Service.ParticipationService;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import jdk.nashorn.internal.codegen.CompilerConstants;
import org.controlsfx.control.Notifications;
import utils.DataSource;

/**
 * FXML Controller class
 *
 * @author DJAZIA
 */
public class EvenementController extends Controller implements Initializable {

    @FXML
    private ImageView imageEve;
    @FXML
    private Label titre;
    @FXML
    private Label description;
    @FXML
    private Label date;
  
    private Label nbplaces = new Label();
    private AnchorPane holderPane;
    public static Evenement evenement;
    @FXML
    private TextArea contenuu;
    private int idEvenement ;
    @FXML
    private WebView webView;
    @FXML
    private Label lieu;
    @FXML
    private Label type;
    @FXML
    private Label datefin;
    @FXML
    private Label prix;
private Connection con = DataSource.getInstance().getConnection();
private final ParticipationService es = new ParticipationService();
private final AvisEvenementService as =  new AvisEvenementService();
    @FXML
    private AnchorPane holderPaneVue;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Calendar c = Calendar.getInstance();
        c.setTime(evenement.getDateEvenement());
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        int second = c.get(Calendar.SECOND);
        WebEngine webEngine = webView.getEngine();
        String lien = getClass().getResource("../GUI/CountDown.html").toExternalForm();
        webEngine.load(lien);
        webEngine.reload();
        webView.getEngine().getLoadWorker().stateProperty().addListener((ObservableValue<? extends Worker.State> observable, Worker.State oldValue, Worker.State newValue) -> {
            if( newValue != Worker.State.SUCCEEDED ) {
                return;  
            }
            webEngine.executeScript("document.init("+year+","+(month+1)+","+day+","+hour+","+minute+","+second+")");
        });
    }    

    public ImageView getImageEve() {
        return imageEve;
    }

    public void setImageEve(String imageEve) {
        //File f = new File( );
        Image img = new Image(getClass().getResource("/Images/").toExternalForm() + imageEve);

        this.imageEve.setImage(img);
    }

    public int getIdEvenement() {
        return idEvenement;
    }

    public void setIdEvenement(int idEvenement) {
        this.idEvenement = idEvenement;
    }
    

    public Label getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre.setText(titre);
    }

    public Label getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description.setText(description);
    }

    public Label getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date.setText(date);
    }
    public Label getDatefin() {
        return datefin;
    }

    public void setDatefin(String datefin) {
        this.datefin.setText(datefin);
    }

    public Label getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu.setText(lieu);
    }

    public Label getNbplaces() {
        return nbplaces;
    }

    public void setNbplaces(String nbplaces) {
        this.nbplaces.setText(nbplaces);
    }
     public Label getPrix() {
        return prix;
    }

    public void setPrix(String prixx) {
        this.prix.setText(prixx);
    }
      public Label getType() {
        return type;
    }

    public void setType(String type) {
        this.type.setText(type);
    }

    public void setNbplaces() {
        this.nbplaces.setText(String.valueOf(Integer.parseInt(this.nbplaces.getText())));
    }

    @FXML
    private void ajouteravis(ActionEvent event) {
        
            as.insertAvis(contenuu.getText());//tekhedh el id event mel label el hidden
             Notifications n = Notifications.create().title("Notification")
                        .text("Avis ajouté avec succées")
                        .graphic(null)
                        .position(Pos.BASELINE_LEFT)
                        .onAction((ActionEvent event1) -> {
                            System.out.println("notification");
                        });
                n.showConfirm();

            contenuu.setText(null);        
    }

    @FXML
    private void retour(ActionEvent event) throws IOException, SQLException {
         AnchorPane parentContent = FXMLLoader.load(getClass().getResource(("/GUI/ListeEvent.fxml")));
        holderPane = holderPaneVue ;
        holderPane.getChildren().setAll(parentContent);
       
    }

    }