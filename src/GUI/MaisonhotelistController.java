/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;


import static Core.Controller.getUserId;
import static Core.Controller.holderPane;
import Core.LayoutFrontController;
import Entities.Maisons_hotes;
import Entities.Reservation_hotes;
import Entities.User;
import Service.MaisonHoteService;
import Service.ReservationService;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Duration;
import tray.animations.AnimationType;
import static tray.notification.NotificationType.SUCCESS;
import tray.notification.TrayNotification;
import GUI.FXMLDocumentController;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import org.apache.http.pool.ConnPool;
import static tray.notification.NotificationType.SUCCESS;
import utils.EnvoyerMail;

/**
 * FXML Controller class
 *
 * @author Bhs Nada
 */
public class MaisonhotelistController implements Initializable {

    @FXML
    private ListView<Maisons_hotes> listView;

    MaisonHoteService maisonserv = new MaisonHoteService();

    ObservableList<Maisons_hotes> mmhote = FXCollections.observableArrayList();
    @FXML
    private DialogPane popup;
    @FXML
    private ImageView imagehote;

    @FXML
    private Label telhote;
    @FXML
    private AnchorPane AnchorPane;
    @FXML
    private Label nomhote;
    @FXML
    private Label sitehote;
    @FXML
    private Label payshote;
    @FXML
    private Label gouvernorathote;
    @FXML
    private Label prixhote;
    @FXML
    private Label capaciteshote;
    @FXML
    private Label descriptionhote;
    @FXML
    private Label adressehote;
    @FXML
    private Label mailhote;
    @FXML
    private Hyperlink close_popup;
    private MaisonHoteService maisonHoteService = new MaisonHoteService();
    @FXML
    private Menu Contacter;

    @FXML
    private MenuBar Menu;
    @FXML
    private Label reservationTitle;
    @FXML
    private TextField id_hote;
    @FXML
    private Label d_deb;
    @FXML
    private DatePicker date_debut;
    @FXML
    private Label d_fin;

    @FXML
    private Label nb_p;
    @FXML
    private TextField nb_personnes;
    @FXML
    private Button reserverBtn;
    @FXML
    private Button annulerBtn;
    @FXML
    private TextField RechercheTxt;
    @FXML
    private Button RechercheBtn;
    @FXML
    private ComboBox<String> paysRechercher;
    ReservationService reserv = new ReservationService();
    @FXML
    private Button afficherReser;
    @FXML
    private Button btnRetour;
    @FXML
    private TextField nb_jours;
    @FXML
    private Slider slider;
    private static final int value = 10;
    @FXML
    private Label telhote1;
    @FXML
    private Label sitehote1;
    @FXML
    private Label capaciteshote1;
    @FXML
    private Label capaciteshote2;
    @FXML
    private Label payshote1;
    @FXML
    private Label prixhote1;
    @FXML
    private Label descriptionhote1;
    @FXML
    private Label gouvernorathote1;
    @FXML
    private Label adressehote1;
    @FXML
    private Label mailhote1;
    @FXML
    private AnchorPane Contact;
    @FXML
    private Label title;
    @FXML
    private TextField mailre;
    @FXML
    private Label a;
    @FXML
    private TextField objetMail;
    @FXML
    private Label objet;
    @FXML
    private Label msg;
    @FXML
    private TextArea contenuMsg;
    @FXML
    private Button envoyerBtn;
    @FXML
    private Button annulerMail;
    @FXML
    private Label name2;
    @FXML
    private Label name1;
    @FXML
    private Button DemandeBtn;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        /// slider 

        slider.setValue(value);

        slider.setMin(1);
        slider.setShowTickMarks(true);
        slider.setShowTickLabels(true);
        slider.setBlockIncrement(1);

        nb_personnes.textProperty().bindBidirectional(slider.valueProperty(), NumberFormat.getNumberInstance());

        ////
        paysRechercher.getItems().add("Algérie");
        paysRechercher.getItems().add("Allemagne");
        paysRechercher.getItems().add("France");
        paysRechercher.getItems().add("Turque");
        paysRechercher.getItems().add("Tunis");

        RechercheTxt.setVisible(true);
        paysRechercher.setVisible(true);
        RechercheBtn.setVisible(true);
        ////******* menu
        Menu.setVisible(false);

        Contacter.setVisible(false);
        ///////
        afficherReser.setVisible(false);

        /**
         * ******** Resevation form ********
         */
        reservationTitle.setVisible(false);
        id_hote.setVisible(false);
        d_deb.setVisible(false);
        date_debut.setVisible(false);
        d_fin.setVisible(false);
        nb_p.setVisible(false);
        nb_personnes.setVisible(false);
        reserverBtn.setVisible(false);
        annulerBtn.setVisible(false);
        slider.setVisible(false);
        nb_jours.setVisible(false);
        name1.setVisible(false);
        /**
         **********************************
         */

        nomhote.setVisible(false);
        payshote.setVisible(false);
        gouvernorathote.setVisible(false);
        sitehote.setVisible(false);
        prixhote.setVisible(false);
        capaciteshote.setVisible(false);
        adressehote.setVisible(false);
        descriptionhote.setVisible(false);
        mailhote.setVisible(false);
        close_popup.setVisible(false);
        popup.setVisible(false);
        imagehote.setVisible(false);
        telhote.setVisible(false);
////
        payshote1.setVisible(false);
        gouvernorathote1.setVisible(false);
        sitehote1.setVisible(false);
        prixhote1.setVisible(false);
        capaciteshote1.setVisible(false);
        adressehote1.setVisible(false);
        descriptionhote1.setVisible(false);
        mailhote1.setVisible(false);
        telhote1.setVisible(false);
////

        /**
         * **************** Anchorpne Mail *****************
         */
        Contact.setVisible(false);
        a.setVisible(false);
        mailre.setVisible(false);
        objet.setVisible(false);
        objetMail.setVisible(false);
        msg.setVisible(false);
        contenuMsg.setVisible(false);
        envoyerBtn.setVisible(false);
        annulerMail.setVisible(false);
        name2.setVisible(false);
        /**
         * *****************************************************
         */
        mmhote = (ObservableList) maisonserv.list();
        listView.setItems(mmhote);
        listView.getSelectionModel().clearSelection();
        Contacter.setVisible(false);

        /////////
        int nb;
        try {
            nb = maisonHoteService.UserResponsable();
            if (nb != 0) {
                btnRetour.setVisible(true);
                DemandeBtn.setVisible(false);
            } else {
                btnRetour.setVisible(false);
                DemandeBtn.setVisible(true);

            }
        } catch (SQLException ex) {
            Logger.getLogger(MaisonhotelistController.class.getName()).log(Level.SEVERE, null, ex);
        }

        ////////////
        ////////////
        listView.setCellFactory(new Callback<ListView<Maisons_hotes>, ListCell<Maisons_hotes>>() {
            @Override
            public ListCell<Maisons_hotes> call(ListView<Maisons_hotes> param) {
                ListCell<Maisons_hotes> cell;
                cell = new ListCell<Maisons_hotes>() {
                    protected void updateItem(Maisons_hotes item, boolean empty) {
                        if (item != null) {

                            HBox hbox = new HBox();
                            hbox.setSpacing(100);
                            hbox.setAlignment(Pos.CENTER);

                            VBox vbox = new VBox();
                            vbox.setSpacing(2.5);
                            vbox.setAlignment(Pos.CENTER);

                            VBox Buttons = new VBox();
                            Buttons.setSpacing(2.5);
                            Buttons.setAlignment(Pos.CENTER);

                            Image img;
                            img = new Image("file:" + item.getImage());
                            ImageView imgView = new ImageView(img);

                            //**************************ImgView Resize*****************
                            imgView.setFitWidth(150);
                            imgView.setFitHeight(150);
                            imgView.setPreserveRatio(false);
                            imgView.setSmooth(true);
                            imgView.setCache(true);
                            //***********************************************************

                            Button detail = new Button("Détail");
                            detail.setStyle("  -fx-background-color: linear-gradient(#ff5400,#be1d00);\n"
                                    + "    -fx-background-radius: 30;\n"
                                    + "    -fx-background-insets: 0;\n"
                                    + "    -fx-text-fill: white;"
                                    + "-fx-pref-height: 25px;"
                                    + "-fx-pref-width:25px;");

                            Button contacterBtn = new Button("Contacter");
                            contacterBtn.setStyle("  -fx-background-color: linear-gradient(#7ebcea, #2f4b8f);\n"
                                    + "    -fx-background-radius: 30;\n"
                                    + "    -fx-background-insets: 0;\n"
                                    + "    -fx-text-fill: white;");

                            Button Reserver = new Button("Reserver");
                            Reserver.setStyle(" -fx-background-color: linear-gradient(#d7e244,#32CD32);\n"
                                    + "    -fx-background-radius: 30;\n"
                                    + "    -fx-background-insets: 0;\n"
                                    + "    -fx-text-fill: white;");
                            //*****************************Details_button_OnClick******************************
                            detail.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<Event>() {
                                @Override
                                public void handle(Event event) {

                                    ////******* menu
                                    Menu.setVisible(false);

                                    Contacter.setVisible(false);
                                    ///////

                                    popup.setVisible(true);
                                    close_popup.setVisible(true);
                                    imagehote.setVisible(true);
                                    telhote.setVisible(true);
                                    nomhote.setVisible(true);

                                    imagehote.setFitWidth(200);
                                    imagehote.setFitHeight(200);
                                    imagehote.setPreserveRatio(false);
                                    imagehote.setSmooth(true);
                                    imagehote.setCache(true);

                                    nomhote.setVisible(true);
                                    payshote.setVisible(true);
                                    gouvernorathote.setVisible(true);
                                    sitehote.setVisible(true);
                                    prixhote.setVisible(true);
                                    capaciteshote.setVisible(true);
                                    adressehote.setVisible(true);
                                    descriptionhote.setVisible(true);
                                    mailhote.setVisible(true);

                                    Contacter.setVisible(true);
                                    ////
                                    payshote1.setVisible(true);
                                    gouvernorathote1.setVisible(true);
                                    sitehote1.setVisible(true);
                                    prixhote1.setVisible(true);
                                    capaciteshote1.setVisible(true);
                                    adressehote1.setVisible(true);
                                    descriptionhote1.setVisible(true);
                                    mailhote1.setVisible(true);
                                    telhote1.setVisible(true);
////

                                    imagehote.setImage(img);
                                    System.out.println("++++++++++++++++++++++++");
                                    System.out.println(item.getId());
                                    System.out.println("++++++++++++++++++++++++");
                                    telhote.setText(Integer.toString(item.getTel()));
                                    nomhote.setText(item.getNom());
                                    sitehote.setText(item.getSite_web());
                                    adressehote.setText(item.getAdresse());
                                    mailhote.setText(item.getMail());
                                    payshote.setText(item.getPays());
                                    descriptionhote.setText(item.getDescription());
                                    gouvernorathote.setText(item.getGouvernorat());
                                    capaciteshote.setText(Integer.toString(item.getCapacites()));
                                    prixhote.setText(Double.toString(item.getPrix()));

                                }

                            });
                            detail.setMaxWidth(120);

                            ///////////////////////////////////////////////////////////////////////////// 
                            //*****************************Details_button_Reserver******************************
                            Reserver.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<Event>() {
                                @Override
                                public void handle(Event event) {
                                    /**
                                     * ******** Resevation form ********
                                     */
                                    reservationTitle.setVisible(true);
                                    id_hote.setVisible(false);
                                    d_deb.setVisible(true);
                                    date_debut.setVisible(true);
                                    d_fin.setVisible(true);
                                    nb_p.setVisible(true);
                                    nb_personnes.setVisible(true);
                                    reserverBtn.setVisible(true);
                                    annulerBtn.setVisible(true);
                                    nb_jours.setVisible(true);
                                    slider.setVisible(true);
                                    name1.setVisible(true);
                                    /**
                                     * *********************************
                                     */

                                    slider.setMax(item.getCapacites());
                                    id_hote.setText(Integer.toString(item.getId()));
                                    name1.setText(item.getNom());
                                    double prixTotReser = reserv.PrixReservation(item.getId());

                                    afficherReser.setVisible(true);
                                }
                            });

                            //*****************************Details_button_Contacter******************************
                            contacterBtn.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
                                @Override
                                public void handle(MouseEvent event) {
                                    /**
                                     * **************** Anchorpne Mail
                                     * *****************
                                     */
                                    Contact.setVisible(true);
                                    a.setVisible(true);
                                    mailre.setVisible(true);
                                    objet.setVisible(true);
                                    objetMail.setVisible(true);
                                    msg.setVisible(true);
                                    contenuMsg.setVisible(true);
                                    envoyerBtn.setVisible(true);
                                    annulerMail.setVisible(true);
                                    name2.setVisible(true);
                                    mailre.setText(item.getMail());
                                    name2.setText(item.getNom());
                                    /**
                                     * *****************************************************
                                     */
                                }
                            });

                            /**
                             * ****************************************************************
                             */
                            /////////////////////////////////////////////////////Add interface
                            Label nomhote = new Label(item.getNom());
                            Label pays = new Label(item.getPays());
                            vbox.getChildren().add(nomhote);
                            vbox.getChildren().add(pays);
                            Buttons.getChildren().add(detail);
                            Buttons.getChildren().add(Reserver);
                            Buttons.getChildren().add(contacterBtn);
                            hbox.getChildren().add(imgView);
                            hbox.getChildren().add(vbox);
                            hbox.getChildren().add(Buttons);

                            setGraphic(hbox);

                        }

                    }

                };
                return cell;
            }

        });

    }

    @FXML
    private void closePopup(ActionEvent event) {
        nomhote.setVisible(false);

        payshote.setVisible(false);
        gouvernorathote.setVisible(false);
        sitehote.setVisible(false);
        prixhote.setVisible(false);
        capaciteshote.setVisible(false);
        adressehote.setVisible(false);
        descriptionhote.setVisible(false);
        mailhote.setVisible(false);
        close_popup.setVisible(false);
        popup.setVisible(false);
        imagehote.setVisible(false);

        telhote.setVisible(false);
////
        payshote1.setVisible(false);
        gouvernorathote1.setVisible(false);
        sitehote1.setVisible(false);
        prixhote1.setVisible(false);
        capaciteshote1.setVisible(false);
        adressehote1.setVisible(false);
        descriptionhote1.setVisible(false);
        mailhote1.setVisible(false);
        telhote1.setVisible(false);
////
        ////******* menu
        Menu.setVisible(false);
        Contacter.setVisible(false);
        ///////
        Contacter.setVisible(false);
    }

    @FXML
    private void Reserver(ActionEvent event) {
        if (date_debut.getValue() == null && nb_jours.getText() == null && nb_personnes.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Erreur");
            alert.setHeaderText(null);
            alert.setContentText("Il faut remplir tout les champs");
            alert.showAndWait();
        } else if (validateNumberPerso()) {
            java.sql.Date DDebut = java.sql.Date.valueOf(date_debut.getValue());
            //java.sql.Date DFin = java.sql.Date.valueOf(date_fin.getValue());
            Maisons_hotes maison = new Maisons_hotes();

            maison.setId(Integer.parseInt(id_hote.getText()));

            double prixTotReser = reserv.PrixReservation(maison.getId());
            //reservation.setPrix(prixTotReser);

            Reservation_hotes reservation = new Reservation_hotes(DDebut, Integer.parseInt(nb_personnes.getText()), prixTotReser, maison, Integer.parseInt(nb_jours.getText()));

            /*  System.out.println("******************");
            System.out.println(maison.getPrix());
            System.out.println("******************");
             */
            ReservationService reservationService = new ReservationService();

            reservationService.ajouterReservation(reservation);

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Reservation hôte ");
            alert.setHeaderText(null);
            alert.setContentText("Reservation Hote Reussi !!  ");
            alert.showAndWait();

        }
    }

    @FXML
    private void annulerReservation(ActionEvent event) {
        /**
         * ******** Resevation form ********
         */
        reservationTitle.setVisible(false);
        id_hote.setVisible(false);
        d_deb.setVisible(false);
        date_debut.setVisible(false);

        d_fin.setVisible(false);
        nb_p.setVisible(false);
        nb_personnes.setVisible(false);
        reserverBtn.setVisible(false);
        annulerBtn.setVisible(false);
        slider.setVisible(false);
        nb_jours.setVisible(false);
        name1.setVisible(false);
        /**
         * *********************************
         */
        afficherReser.setVisible(false);
    }

    private boolean validateNumberPerso() {
        Pattern p = Pattern.compile("[1-9]+");
        Matcher m = p.matcher(nb_personnes.getText());
        if (m.find() && m.group().equals(nb_personnes.getText())) {
            return true;
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Nombre Personnes");
            alert.setHeaderText(null);
            alert.setContentText("S'il vous plait saisir un nombre valide");
            alert.showAndWait();

            return false;
        }
    }

    @FXML
    private void RechercheParNom(ActionEvent event) {

        mmhote.clear();
        mmhote.addAll((ObservableList) maisonserv.FindByName(RechercheTxt.getText()));
        listView.setItems(mmhote);

        /**
         * appel ll liste view pour l affichage *
         */
        listView.setCellFactory(new Callback<ListView<Maisons_hotes>, ListCell<Maisons_hotes>>() {
            @Override
            public ListCell<Maisons_hotes> call(ListView<Maisons_hotes> param) {
                ListCell<Maisons_hotes> cell;
                cell = new ListCell<Maisons_hotes>() {
                    protected void updateItem(Maisons_hotes item, boolean empty) {
                        if (item != null) {

                            HBox hbox = new HBox();
                            hbox.setSpacing(100);
                            hbox.setAlignment(Pos.CENTER);

                            VBox vbox = new VBox();
                            vbox.setSpacing(2.5);
                            vbox.setAlignment(Pos.CENTER);

                            VBox Buttons = new VBox();
                            Buttons.setSpacing(2.5);
                            Buttons.setAlignment(Pos.CENTER);

                            Image img;
                            img = new Image("file:" + item.getImage());
                            ImageView imgView = new ImageView(img);

                            //**************************ImgView Resize*****************
                            imgView.setFitWidth(150);
                            imgView.setFitHeight(150);
                            imgView.setPreserveRatio(false);
                            imgView.setSmooth(true);
                            imgView.setCache(true);
                            //***********************************************************

                            Button detail = new Button("Détail");
                            detail.setStyle("  -fx-background-color: linear-gradient(#ff5400,#be1d00);\n"
                                    + "    -fx-background-radius: 30;\n"
                                    + "    -fx-background-insets: 0;\n"
                                    + "    -fx-text-fill: white;");

                            Button contacterBtn = new Button("Contacter");
                            contacterBtn.setStyle("  -fx-background-color: linear-gradient(#7ebcea, #2f4b8f);\n"
                                    + "    -fx-background-radius: 30;\n"
                                    + "    -fx-background-insets: 0;\n"
                                    + "    -fx-text-fill: white;");

                            Button Reserver = new Button("Reserver");
                            Reserver.setStyle(" -fx-background-color: linear-gradient(#d7e244,#32CD32);\n"
                                    + "    -fx-background-radius: 30;\n"
                                    + "    -fx-background-insets: 0;\n"
                                    + "    -fx-text-fill: white;");

                            //*****************************Details_button_OnClick******************************
                            detail.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<Event>() {
                                @Override
                                public void handle(Event event) {

                                    ////******* menu
                                    Menu.setVisible(true);

                                    Contacter.setVisible(true);
                                    ///////

                                    popup.setVisible(true);
                                    close_popup.setVisible(true);
                                    imagehote.setVisible(true);
                                    telhote.setVisible(true);
                                    nomhote.setVisible(true);

                                    imagehote.setFitWidth(200);
                                    imagehote.setFitHeight(200);
                                    imagehote.setPreserveRatio(false);
                                    imagehote.setSmooth(true);
                                    imagehote.setCache(true);

                                    nomhote.setVisible(true);
                                    payshote.setVisible(true);
                                    gouvernorathote.setVisible(true);
                                    sitehote.setVisible(true);
                                    prixhote.setVisible(true);
                                    capaciteshote.setVisible(true);
                                    adressehote.setVisible(true);
                                    descriptionhote.setVisible(true);
                                    mailhote.setVisible(true);

                                    ////
                                    payshote1.setVisible(true);
                                    gouvernorathote1.setVisible(true);
                                    sitehote1.setVisible(true);
                                    prixhote1.setVisible(true);
                                    capaciteshote1.setVisible(true);
                                    adressehote1.setVisible(true);
                                    descriptionhote1.setVisible(true);
                                    mailhote1.setVisible(true);
                                    telhote1.setVisible(true);
                                    ////

                                    imagehote.setImage(img);
                                    System.out.println("++++++++++++++++++++++++");
                                    System.out.println(item.getId());
                                    System.out.println("++++++++++++++++++++++++");
                                    telhote.setText(Integer.toString(item.getTel()));
                                    nomhote.setText(item.getNom());
                                    sitehote.setText(item.getSite_web());
                                    adressehote.setText(item.getAdresse());
                                    mailhote.setText(item.getMail());
                                    payshote.setText(item.getPays());
                                    descriptionhote.setText(item.getDescription());
                                    gouvernorathote.setText(item.getGouvernorat());
                                    capaciteshote.setText(Integer.toString(item.getCapacites()));
                                    prixhote.setText(Double.toString(item.getPrix()));

                                }

                            });
                            detail.setMaxWidth(120);

                            ///////////////////////////////////////////////////////////////////////////// 
                            //*****************************Details_button_Reserver******************************
                            Reserver.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<Event>() {
                                @Override
                                public void handle(Event event) {
                                    /**
                                     * ******** Resevation form ********
                                     */
                                    reservationTitle.setVisible(true);
                                    id_hote.setVisible(false);
                                    d_deb.setVisible(true);
                                    date_debut.setVisible(true);
                                    d_fin.setVisible(true);
                                    nb_p.setVisible(true);
                                    nb_personnes.setVisible(true);
                                    reserverBtn.setVisible(true);
                                    annulerBtn.setVisible(true);
                                    nb_jours.setVisible(true);
                                    slider.setVisible(true);
                                    name1.setVisible(true);
                                    /**
                                     * *********************************
                                     */

                                    slider.setMax(item.getCapacites());
                                    id_hote.setText(Integer.toString(item.getId()));
                                    name1.setText(item.getNom());
                                    afficherReser.setVisible(true);
                                }
                            });
                            //*****************************Details_button_Contacter******************************
                            contacterBtn.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
                                @Override
                                public void handle(MouseEvent event) {
                                    /**
                                     * **************** Anchorpne Mail
                                     * *****************
                                     */
                                    Contact.setVisible(true);
                                    a.setVisible(true);
                                    mailre.setVisible(true);
                                    objet.setVisible(true);
                                    objetMail.setVisible(true);
                                    msg.setVisible(true);
                                    contenuMsg.setVisible(true);
                                    envoyerBtn.setVisible(true);
                                    annulerMail.setVisible(true);
                                    name2.setVisible(true);
                                    mailre.setText(item.getMail());
                                    name2.setText(item.getNom());
                                    /**
                                     * *****************************************************
                                     */
                                }
                            });

                            /**
                             * ****************************************************************
                             */
                            /////////////////////////////////////////////////////
                            Label nomhote = new Label(item.getNom());
                            Label pays = new Label(item.getPays());
                            vbox.getChildren().add(nomhote);
                            vbox.getChildren().add(pays);
                            Buttons.getChildren().add(detail);
                            Buttons.getChildren().add(Reserver);
                            Buttons.getChildren().add(contacterBtn);

                            hbox.getChildren().add(imgView);
                            hbox.getChildren().add(vbox);
                            hbox.getChildren().add(Buttons);

                            setGraphic(hbox);

                        }

                    }

                };
                return cell;
            }

        });

    }

    @FXML
    private void RechercheParPays(ActionEvent event) {
        mmhote.clear();
        mmhote.addAll((ObservableList) maisonserv.FindByPays(paysRechercher.getSelectionModel().getSelectedItem()));
        listView.setItems(mmhote);

        /**
         * appel ll liste view pour l affichage *
         */
        listView.setCellFactory(new Callback<ListView<Maisons_hotes>, ListCell<Maisons_hotes>>() {
            @Override
            public ListCell<Maisons_hotes> call(ListView<Maisons_hotes> param) {
                ListCell<Maisons_hotes> cell;
                cell = new ListCell<Maisons_hotes>() {
                    protected void updateItem(Maisons_hotes item, boolean empty) {
                        if (item != null) {

                            HBox hbox = new HBox();
                            hbox.setSpacing(100);
                            hbox.setAlignment(Pos.CENTER);

                            VBox vbox = new VBox();
                            vbox.setSpacing(2.5);
                            vbox.setAlignment(Pos.CENTER);

                            VBox Buttons = new VBox();
                            Buttons.setSpacing(2.5);
                            Buttons.setAlignment(Pos.CENTER);

                            Image img;
                            img = new Image("file:" + item.getImage());
                            ImageView imgView = new ImageView(img);

                            //**************************ImgView Resize*****************
                            imgView.setFitWidth(150);
                            imgView.setFitHeight(150);
                            imgView.setPreserveRatio(false);
                            imgView.setSmooth(true);
                            imgView.setCache(true);
                            //***********************************************************
                            Button detail = new Button("Détail");
                            detail.setStyle("  -fx-background-color: linear-gradient(#ff5400,#be1d00);\n"
                                    + "    -fx-background-radius: 30;\n"
                                    + "    -fx-background-insets: 0;\n"
                                    + "    -fx-text-fill: white;");

                            Button contacterBtn = new Button("Contacter");
                            contacterBtn.setStyle("  -fx-background-color: linear-gradient(#7ebcea, #2f4b8f);\n"
                                    + "    -fx-background-radius: 30;\n"
                                    + "    -fx-background-insets: 0;\n"
                                    + "    -fx-text-fill: white;");

                            Button Reserver = new Button("Reserver");
                            Reserver.setStyle(" -fx-background-color: linear-gradient(#d7e244,#32CD32);\n"
                                    + "    -fx-background-radius: 30;\n"
                                    + "    -fx-background-insets: 0;\n"
                                    + "    -fx-text-fill: white;");
                            //*****************************Details_button_OnClick******************************
                            detail.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<Event>() {
                                @Override
                                public void handle(Event event) {

                                    ////******* menu
                                    Menu.setVisible(true);

                                    Contacter.setVisible(true);
                                    ///////

                                    popup.setVisible(true);
                                    close_popup.setVisible(true);
                                    imagehote.setVisible(true);
                                    telhote.setVisible(true);
                                    nomhote.setVisible(true);

                                    imagehote.setFitWidth(200);
                                    imagehote.setFitHeight(200);
                                    imagehote.setPreserveRatio(false);
                                    imagehote.setSmooth(true);
                                    imagehote.setCache(true);

                                    nomhote.setVisible(true);
                                    payshote.setVisible(true);
                                    gouvernorathote.setVisible(true);
                                    sitehote.setVisible(true);
                                    prixhote.setVisible(true);
                                    capaciteshote.setVisible(true);
                                    adressehote.setVisible(true);
                                    descriptionhote.setVisible(true);
                                    mailhote.setVisible(true);

                                    ////
                                    payshote1.setVisible(true);
                                    gouvernorathote1.setVisible(true);
                                    sitehote1.setVisible(true);
                                    prixhote1.setVisible(true);
                                    capaciteshote1.setVisible(true);
                                    adressehote1.setVisible(true);
                                    descriptionhote1.setVisible(true);
                                    mailhote1.setVisible(true);
                                    telhote1.setVisible(true);
////

                                    imagehote.setImage(img);
                                    System.out.println("++++++++++++++++++++++++");
                                    System.out.println(item.getId());
                                    System.out.println("++++++++++++++++++++++++");
                                    telhote.setText(Integer.toString(item.getTel()));
                                    nomhote.setText(item.getNom());
                                    sitehote.setText(item.getSite_web());
                                    adressehote.setText(item.getAdresse());
                                    mailhote.setText(item.getMail());
                                    payshote.setText(item.getPays());
                                    descriptionhote.setText(item.getDescription());
                                    gouvernorathote.setText(item.getGouvernorat());
                                    capaciteshote.setText(Integer.toString(item.getCapacites()));
                                    prixhote.setText(Double.toString(item.getPrix()));

                                }

                            });
                            detail.setMaxWidth(120);

                            ///////////////////////////////////////////////////////////////////////////// 
                            //*****************************Details_button_Reserver******************************
                            Reserver.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<Event>() {
                                @Override
                                public void handle(Event event) {
                                    /**
                                     * ******** Resevation form ********
                                     */
                                    reservationTitle.setVisible(true);
                                    id_hote.setVisible(false);
                                    d_deb.setVisible(true);
                                    date_debut.setVisible(true);
                                    d_fin.setVisible(true);
                                    nb_p.setVisible(true);
                                    nb_personnes.setVisible(true);
                                    reserverBtn.setVisible(true);
                                    annulerBtn.setVisible(true);
                                    nb_jours.setVisible(true);
                                    slider.setVisible(true);
                                    name1.setVisible(true);
                                    /**
                                     * *********************************
                                     */

                                    slider.setMax(item.getCapacites());
                                    id_hote.setText(Integer.toString(item.getId()));
                                    name1.setText(item.getNom());
                                    afficherReser.setVisible(true);
                                }
                            });
                            //*****************************Details_button_Contacter******************************
                            contacterBtn.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
                                @Override
                                public void handle(MouseEvent event) {
                                    /**
                                     * **************** Anchorpne Mail
                                     * *****************
                                     */
                                    Contact.setVisible(true);
                                    a.setVisible(true);
                                    mailre.setVisible(true);
                                    objet.setVisible(true);
                                    objetMail.setVisible(true);
                                    msg.setVisible(true);
                                    contenuMsg.setVisible(true);
                                    envoyerBtn.setVisible(true);
                                    annulerMail.setVisible(true);
                                    name2.setVisible(true);
                                    mailre.setText(item.getMail());
                                    name2.setText(item.getNom());
                                    /**
                                     * *****************************************************
                                     */
                                }
                            });

                            /**
                             * ****************************************************************
                             */
                            /////////////////////////////////////////////////////
                            Label nomhote = new Label(item.getNom());
                            Label pays = new Label(item.getPays());
                            vbox.getChildren().add(nomhote);
                            vbox.getChildren().add(pays);
                            Buttons.getChildren().add(detail);
                            Buttons.getChildren().add(Reserver);
                            Buttons.getChildren().add(contacterBtn);
                            hbox.getChildren().add(imgView);
                            hbox.getChildren().add(vbox);
                            hbox.getChildren().add(Buttons);

                            setGraphic(hbox);

                        }

                    }

                };
                return cell;
            }

        });

    }

    @FXML
    private void afficheReservation(ActionEvent event) throws IOException {

        loadView("/GUI/AfficherReservation.fxml");
    }

    private void setNode(Node node) {
        holderPane.getChildren().clear();
        holderPane.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.millis(1500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
    }

    private Object loadView(String path) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource((path)));
        AnchorPane parentContent = null;
        try {
            parentContent = fxmlLoader.load();
        } catch (IOException ex) {
            Logger.getLogger(LayoutFrontController.class.getName()).log(Level.SEVERE, null, ex);
        }
        setNode(parentContent);
        System.gc();
        return fxmlLoader.getController();
    }

    @FXML
    private void btnRetour(ActionEvent event) throws IOException {
        /*     Parent home_page_parent = FXMLLoader.load(getClass().getResource("/GUI/ListesHotes.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

        app_stage.hide(); //optional
        app_stage.setScene(home_page_scene);
        app_stage.show();*/
        loadView("/GUI/ListesHotes.fxml");
    }

    private void contactAction(ActionEvent event) {

    }

    @FXML
    private void ContacterHoteMail(ActionEvent event) {
        loadView("../GUI/MailContact.fxml");
    }

    @FXML
    private void EnvoyerMail(ActionEvent event) {
        if (contenuMsg.getText() != null && !mailre.getText().equals("")) {
            //SendMail email= new SendMail();

            EnvoyerMail email = new EnvoyerMail();

            email.Send(mailre.getText(), objetMail.getText(), contenuMsg.getText());

            /**
             * ******* notif ******
             */
            TrayNotification tray = new TrayNotification("Email Envoyee", " avec Succés !", SUCCESS);
            tray.setAnimationType(AnimationType.POPUP);
            tray.showAndDismiss(Duration.seconds(10));
            /**
             * **********************
             */
        } else {

            /**
             * ******* notif ******
             */
            TrayNotification tray = new TrayNotification("??? Attention ???", " Email n'est pas Envoyee !", SUCCESS);
            tray.setAnimationType(AnimationType.POPUP);
            tray.showAndDismiss(Duration.seconds(10));
            /**
             * **********************
             */
        }

    }

    @FXML
    private void annulerMail(ActionEvent event) {
        /**
         * **************** Anchorpne Mail *****************
         */
        Contact.setVisible(false);
        a.setVisible(false);
        mailre.setVisible(false);
        objet.setVisible(false);
        objetMail.setVisible(false);
        msg.setVisible(false);
        contenuMsg.setVisible(false);
        envoyerBtn.setVisible(false);
        annulerMail.setVisible(false);
        name2.setVisible(false);
        /**
         * *****************************************************
         */
    }

    @FXML
    private void DemandeResponsable(ActionEvent event) {
        loadView("../GUI/DemandeRespHote.fxml");
    }

}
