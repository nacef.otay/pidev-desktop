/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import static Core.Controller.holderPane;
import Entities.Evenement;
import Entities.Guide;
import Service.EvenementService;
import Service.GuideService;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import com.sun.javafx.collections.ElementObservableListDecorator;
import com.sun.prism.impl.Disposer;
import java.io.IOException;
import java.net.URL;
import static java.nio.file.Files.list;
import java.time.LocalDate;
import static java.util.Collections.list;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.animation.FadeTransition;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author soumaya
 */
public class partie_adminController implements Initializable {

   @FXML
    private AnchorPane AdminEvent;
    @FXML
    private TableView<Evenement> display;
    @FXML
    private TableColumn<?, ?> titre;
    @FXML
    private TableColumn<?, ?> date;
    @FXML
    private TableColumn<?, ?> datefin;
    @FXML
    private TableColumn<?, ?> typeevent;
    @FXML
    private TableColumn<?, ?> nbplaces;
    @FXML
    private TableColumn<?, ?> lieu;
    @FXML
    private TableColumn<?, ?> prix;
    @FXML
    private TableColumn<?, ?> description;
    @FXML
    private TableColumn<?, ?> ColumnIdEvenement;
    
    @FXML
    private JFXTextField recherche;
private final EvenementService es = new EvenementService();
    @FXML
    private JFXButton btn_supprimer;

    /**
     * Initializes the controller class.
     */
     EvenementService service1 = new EvenementService();
   
        ObservableList<Evenement> list;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        list = FXCollections.observableArrayList();
 list = service1.getAll();
        // TODO
        afficherevent();
    recherche.setOnKeyReleased(e -> {
            recherche.textProperty().addListener((observableValue, oldValue, newValue) -> {
                System.out.println(newValue);
                if (newValue.isEmpty()) {
                    display.setItems(list);

                } else {
                    List<Evenement> obsRr = list.stream().filter((o) -> (o.getTitre().toLowerCase().contains(newValue.toLowerCase()))).collect((Collectors.toList()));
                    obsRr.forEach(o -> {
                        System.out.println("processed list, only even numbers: " + o);
                    });
                    ObservableList<Evenement> sortedList = FXCollections.observableArrayList(obsRr);
                    display.setItems(sortedList);
                }

//                 
            });

        });
    }
     
public void afficherevent() {

        display.getItems().clear();
        nbplaces.setCellValueFactory(new PropertyValueFactory<>("nbplaces"));
        titre.setCellValueFactory(new PropertyValueFactory<>("titre"));
        description.setCellValueFactory(new PropertyValueFactory<>("description"));
        typeevent.setCellValueFactory(new PropertyValueFactory<>("titreCordination"));
        date.setCellValueFactory(new PropertyValueFactory<>("dateEvenement"));
        datefin.setCellValueFactory(new PropertyValueFactory<>("datefin"));
        lieu.setCellValueFactory(new PropertyValueFactory<>("adr"));
        prix.setCellValueFactory(new PropertyValueFactory<>("prix"));
        
        
        display.setItems(es.getEv());
       

    }
    
   

   
     public void setNode(Node node) {
        AdminEvent.getChildren().clear();
        AdminEvent.getChildren().add((Node) node);

        FadeTransition ft = new FadeTransition(Duration.millis(1500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
  }

    @FXML
    private void retourclicked(ActionEvent event) throws IOException {
        AnchorPane parentContent = FXMLLoader.load(getClass().getResource(("/GUI/EventAdmin.fxml")));
        holderPane = AdminEvent;
        holderPane.getChildren().setAll(parentContent);
    }

    @FXML
    private void supprimerEvenements(ActionEvent event) {
         Evenement e = (Evenement) display.getSelectionModel().getSelectedItem();
      Alert alert = new Alert(Alert.AlertType.CONFIRMATION,"Etes vous sure de vouloir supprimer cet événement "+e.getTitre()+"?",ButtonType.YES,ButtonType.NO,ButtonType.CANCEL);
      
      alert.showAndWait();
      if (alert.getResult() == ButtonType.YES)
      {
          EvenementService ev = new EvenementService();
          int ide = e.getId();
          ev.deleteEvenement(ide);
        
        display.getItems().removeAll(display.getSelectionModel().getSelectedItem());
        display.getSelectionModel().select(null);
      }
    }
   

}